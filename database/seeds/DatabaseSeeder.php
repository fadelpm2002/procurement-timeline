<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   
        $this->call(UserSeeder::class);
        $this->call(FungsiPemintaPengadaanSeeder::class);
        $this->call(KategoriSeeder::class);
        $this->call(MetodePengadaanSeeder::class);
        $this->call(SubBidangSeeder::class);
        // $this->call(PengadaanSeeder::class);
        // $this->call(RiwayatPengadaanSeeder::class);
        $this->call(TahapanPengadaanSeeder::class);
        // $this->call(RoleSeeder::class);
        // $this->call(ProcurementStageSeeder::class);
        // $this->call(ProcurementStatusSeeder::class);
        // $this->call(ProcurementSeeder::class);
        // $this->call(LastUpdateSeeder::class);
    }
}