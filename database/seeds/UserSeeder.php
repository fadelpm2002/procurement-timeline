<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nama' => 'super user',
            'email' => 'superuser@gmail.com',
            'password' => Hash::make('password'),
            'id_role' => 1,
        ]);
        DB::table('users')->insert([
            'nama' => 'buyer',
            'email' => 'buyer@gmail.com',
            'password' => Hash::make('password'),
            'id_role' => 2,
        ]);
        // DB::table('users')->insert([
        //     'nama' => 'fadel',
        //     'email' => ' fadeltrisatriaborneo@gmail.com',
        //     'password' => Hash::make('fadel123'),
        //     'id_role' => 2,
        // ]);
        DB::table('users')->insert([
            'nama' => 'sekretaris',
            'email' => 'sekretaris@gmail.com',
            'password' => Hash::make('password'),
            'id_role' => 3,
        ]);
    }
}