<?php

use Illuminate\Database\Seeder;

class FungsiPemintaPengadaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\FungsiPemintaPengadaan::create([ 'nama' => 'RPD' ]);
        App\FungsiPemintaPengadaan::create([ 'nama' => 'HSSE' ]);
        App\FungsiPemintaPengadaan::create([ 'nama' => 'S&D' ]);
        App\FungsiPemintaPengadaan::create([ 'nama' => 'Asset' ]);
        App\FungsiPemintaPengadaan::create([ 'nama' => 'COS' ]);
        App\FungsiPemintaPengadaan::create([ 'nama' => 'CS' ]);
        App\FungsiPemintaPengadaan::create([ 'nama' => 'Retail Sales' ]);
        App\FungsiPemintaPengadaan::create([ 'nama' => 'Medical' ]);
        App\FungsiPemintaPengadaan::create([ 'nama' => 'Lainnya' ]);
    }
}