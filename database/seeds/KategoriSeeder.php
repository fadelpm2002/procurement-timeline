<?php

use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Kategori::create(['nama' => 'Pengadaan Baru']);
        App\Kategori::create(['nama' => 'Retender']);
    }
}