<?php

use Illuminate\Database\Seeder;

class SubBidangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\SubBidang::create([
            'code' => 'P.01',
            'description' => 'Eksplorasi, Produksi dan Pengolah lanjutan',
        ]);
        App\SubBidang::create([
            'code' => 'P.01.01',
            'description' => 'Peralatan/suku cadang pemboran, eksplorasi dan produksi',
        ]);
        App\SubBidang::create([
            'code' => 'P.01.02',
            'description' => 'Selubung sumur, pipa produksi dan kelengkapannya',
        ]);
        App\SubBidang::create([
            'code' => 'P.01.03',
            'description' => 'Peralatan/bahan lumpur/kimia pemboran dan penyemenan',
        ]);
        App\SubBidang::create([
            'code' => 'P.01.04',
            'description' => 'Peralatan/suku cadang boiler, mesin, turbin, pembangkit listrik, pompa dan kompresor',
        ]);
        App\SubBidang::create([
            'code' => 'P.01.05',
            'description' => 'Peralatan/suku cadang pengolah dan  pemurni minyak/gas /kimia',
        ]);
        App\SubBidang::create([
            'code' => 'P.02',
            'description' => 'Konstruksi, Mekanikal dan Elektrikal',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.01',
            'description' => 'Peralatan/suku cadang pengemas, pengangkat dan pengangkut',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.02',
            'description' => 'Peralatan/suku cadang bangunan, jalan dan konstruksi',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.03',
            'description' => 'Peralatan/suku cadang instrumentasi  dan kelengkapan mesin',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.04',
            'description' => 'Peralatan/suku cadang mekanikal serta elektrikal',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.05',
            'description' => 'Pipa, selang, katup, dan penyambung',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.06',
            'description' => 'Peralatan/suku cadang telekomunikasi, navigasi, dan komputer',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.07',
            'description' => 'Peralatan/suku cadang alat ukur, survai  dan laboratorium',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.08',
            'description' => 'Alat-alat kerja dan peralatan bengkel',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.09',
            'description' => 'Peralatan/suku cadang  keselamatan kerja, pemadam kebakaran dan lindungan lingkungan',
        ]);
        App\SubBidang::create([
            'code' => 'P.02.10',
            'description' => 'Peralatan/bahan bangunan/tangki, bahan metal/bukan metal, tali baja, rantai, bahan kemasan,
            bahan pengikat dan kelengkapannya',
        ]);
        App\SubBidang::create([
            'code' => 'P.03',
            'description' => 'Bahan Kimia dan Bahan Peledak',
        ]);
        App\SubBidang::create([
            'code' => 'P.03.01',
            'description' => 'Bahan kimia, bahan bakar, pelumas dan cat',
        ]);
        App\SubBidang::create([
            'code' => 'P.03.02',
            'description' => 'Peralatan/suku cadang/bahan peledak, senjata api dan amunisi',
        ]);
        App\SubBidang::create([
            'code' => 'P.04',
            'description' => 'Kantor, Pergudangan, Kesehatan dan Rumah Tangga',
        ]);
        App\SubBidang::create([
            'code' => 'P.04.01',
            'description' => 'Peralatan/perlengkapan tulis, barang cetakan, kantor, pendidikan, peragaan/visualisasi, olah
            raga, kesenian, pergudangan dan perlengkapan pegawai',
        ]);
        App\SubBidang::create([
            'code' => 'P.04.02',
            'description' => 'Peralatan/suku cadang/bahan pertanian, perkebunan, peternakan, perikanan, dan kehutanan',
        ]);
        App\SubBidang::create([
            'code' => 'P.04.03',
            'description' => 'Peralatan/suku cadang kesehatan, farmasi dan obat-obatan',
        ]);
        App\SubBidang::create([
            'code' => 'P.04.04',
            'description' => 'Peralatan, perlengkapan, perabotan dan bahan-bahan kebutuhan rumah tangga',
        ]);
        App\SubBidang::create([
            'code' => 'Q.01',
            'description' => 'Arsitektur',
        ]);
        App\SubBidang::create([
            'code' => 'Q.01.01',
            'description' => 'Jasa Nasihat/Pradisain, disain dan administrasi Kontrak Arsitektural',
        ]);
        App\SubBidang::create([
            'code' => 'Q.01.02',
            'description' => 'Jasa Arsitektural Lansekap',
        ]);
        App\SubBidang::create([
            'code' => 'Q.01.03',
            'description' => 'Jasa Disain Interior',
        ]);
        App\SubBidang::create([
            'code' => 'Q.01.04',
            'description' => 'Jasa Penilaian Perawatan Bangunan Gedung',
        ]);
        App\SubBidang::create([
            'code' => 'Q.01.05',
            'description' => 'Jasa Arsitektural Lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.02',
            'description' => 'Sipil',
        ]);
        App\SubBidang::create([
            'code' => 'Q.02.01',
            'description' => 'Jasa Nasehat/Pra-Disain dan Disain Enjinering Bangunan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.02.02',
            'description' => 'Jasa Nasihat/Pra-disain, dan Disain Enjinering Pekerjaan Teknik Sipil Keairan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.02.03',
            'description' => 'Jasa Nasihat/Pra-disain, dan Disain Enjinering Pekerjaan Teknik Sipil Transportasi',
        ]);
        App\SubBidang::create([
            'code' => 'Q.02.04',
            'description' => 'Jasa Nasihat/Pra-disain, dan Disain Enjinering Pekerjaan Teknik Sipil Lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.03',
            'description' => 'Mekanikal',
        ]);
        App\SubBidang::create([
            'code' => 'Q.03.01',
            'description' => 'Jasa Disain Enjinering Mekanikal',
        ]);
        App\SubBidang::create([
            'code' => 'Q.03.02',
            'description' => 'Jasa Nasehat/Pra-Disain dan Disain Enjinering Industrial Plant & Proses',
        ]);
        App\SubBidang::create([
            'code' => 'Q.03.03',
            'description' => 'Jasa Nasehat/Pra-Disain dan Disain Enjinering Pekerjaan Mekanikal Lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.04',
            'description' => 'Elektrikal',
        ]);
        App\SubBidang::create([
            'code' => 'Q.04.01',
            'description' => 'Jasa Disain Enjinering Elektrikal',
        ]);
        App\SubBidang::create([
            'code' => 'Q.04.02',
            'description' => 'Jasa Nasehat/Pra-disain & Disain Enjinering Sistem Kontrol Lalu Lintas',
        ]);
        App\SubBidang::create([
            'code' => 'Q.04.03',
            'description' => 'Jasa Nasehat/Pra-disain & Disain Enjinering Pekerjaan Elektrikal Lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.05',
            'description' => 'Tata Lingkungan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.05.01',
            'description' => 'Jasa Konsultasi Lingkungan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.05.02',
            'description' => 'Jasa Perencanaan Urban',
        ]);
        App\SubBidang::create([
            'code' => 'Q.05.03',
            'description' => 'Jasa Nasehat/Pra-disain dan Disain Enjinering Pekerjaan Tata Lingkungan lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.06',
            'description' => 'Jasa Survey',
        ]);
        App\SubBidang::create([
            'code' => 'Q.06.01',
            'description' => 'Jasa Survey Permukaan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.06.02',
            'description' => 'Jasa Pembuatan Peta',
        ]);
        App\SubBidang::create([
            'code' => 'Q.06.03',
            'description' => 'Jasa Survey Bawah Tanah',
        ]);
        App\SubBidang::create([
            'code' => 'Q.06.04',
            'description' => 'Jasa Geologi, Geofisik dan Prospek Lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.07',
            'description' => 'Jasa Analisis & Enginering Lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.07.01',
            'description' => 'Jasa Komposisi, Kemurnian dan Analisis',
        ]);
        App\SubBidang::create([
            'code' => 'Q.07.02',
            'description' => 'Jasa Enginering Lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11',
            'description' => 'Arsitektur',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.01',
            'description' => 'Perumahan tunggal dan kopel, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.02',
            'description' => 'Perumahan multi hunian, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.03',
            'description' => 'Bangunan pergudangan dan industri, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.04',
            'description' => 'Bangunan komersial, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.05',
            'description' => 'Bangunan-bangunan non perumahan lainnya termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.06',
            'description' => 'Fasilitas pelatihan sport diluar gedung, fasilitas rekreasi, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.07',
            'description' => 'Pertamanan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.10',
            'description' => 'Finishing Bangunan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.10.01',
            'description' => 'Pekerjaan pemasangan instalasi asesori bangunan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.10.02',
            'description' => 'Pekerjaan dinding dan jendela kaca, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.10.03',
            'description' => 'Pekerjaan interior, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.20',
            'description' => 'Pekerjaan Berketerampilan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.20.01',
            'description' => 'Pekerjaan kayu',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.20.02',
            'description' => 'Pekerjaan logam',
        ]);
        App\SubBidang::create([
            'code' => 'Q.11.30.01',
            'description' => 'Perawatan Gedung / Bangunan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12',
            'description' => 'Sipil',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.01',
            'description' => 'Jalan raya, jalan lingkungan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.02',
            'description' => 'Jalan kereta api, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.03',
            'description' => 'Lapangan terbang dan runway, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.04',
            'description' => 'Jembatan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.05',
            'description' => 'Jalan layang, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.06',
            'description' => 'Terowongan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.07',
            'description' => 'Jalan bawah tanah, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.08',
            'description' => 'Pelabuhan atau dermaga termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.09',
            'description' => 'Drainase Kota, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.10',
            'description' => 'Bendung, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.11',
            'description' => 'Irigasi & Drainase termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.12',
            'description' => 'Persungaian Rawa dan Pantai termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.13',
            'description' => 'Bendungan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.14',
            'description' => 'Pengerukan dan Pengurukan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.20',
            'description' => 'Pekerjaan Persiapan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.20.01',
            'description' => 'Pekerjaan penghancuran',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.20.02',
            'description' => 'Pekerjaan penyiapan dan pengupasan lahan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.20.03',
            'description' => 'Pekerjaan penggalian dan pemindahan tanah',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.30',
            'description' => 'Pekerjaan Struktur',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.30.01',
            'description' => 'Pekerjaan pemancangan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.30.02',
            'description' => 'Pekerjaan pelaksanaan pondasi, termasuk untuk perbaikannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.30.03',
            'description' => 'Pekerjaan kerangka konstruksi atap, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.30.04',
            'description' => 'Pekerjaan atap dan kedap air, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.30.05',
            'description' => 'Pekerjaan pembetonan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.30.06',
            'description' => 'Pekerjaan Konstruksi baja, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.30.07',
            'description' => 'Pekerjaan pemasangan perancah beton',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.30.08',
            'description' => 'Pekerjaan pelaksanaan khusus lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.40',
            'description' => 'Pekerjaan Finishing Struktur',
        ]);
        App\SubBidang::create([
            'code' => 'Q.12.40.01',
            'description' => 'Pekerjaan pengaspalan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13',
            'description' => 'Mekanikal',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.01',
            'description' => 'Instalasi pemanasan, ventilasi udara & AC dalam bangunan termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.02',
            'description' => 'Perpipaan air dalam bangunan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.03',
            'description' => 'Instalasi pipa gas dalam bangunan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.04',
            'description' => 'Insulasi dalam bangunan, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.05',
            'description' => 'Instalasi lift dan eskalator, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.06',
            'description' => 'Pertambangan dan manufaktur, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.07',
            'description' => 'Instalasi thermal, bertekanan, minyak, gas, geothermal, termasuk perawatannya (pekerjaan
            rekayasa)',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.08',
            'description' => 'Konstruksi alat angkut dan alat angkat, termasuk perawatannya (pekerjaan rekayasa)',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.09',
            'description' => 'Konstruksi perpipaan minyak, gas dan energi, termasuk perawatannya (pekerjaan rekayasa)',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.10',
            'description' => 'Fasilitas produksi, penyimpanan minyak dan gas, termasuk perawatannya (pekerjaan
            rekayasa)',
        ]);
        App\SubBidang::create([
            'code' => 'Q.13.11',
            'description' => 'Jasa penyedia peralatan kerja konstruksi',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14',
            'description' => 'Elektrikal',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.01',
            'description' => 'Pembangkit tenaga listrik semua daya, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.02',
            'description' => 'Pembangkit tenaga listrik dengan daya maksimal 10 MW/unit, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.03',
            'description' => 'Pembangkit tenaga listrik energi baru dan terbarukan termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.04',
            'description' => 'Jaringan transmisi tenaga listrik tegangan tinggi dan ekstra tegangan tinggi, termasuk
            perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.05',
            'description' => 'Jaringan transmisi telekomunikasi dan atau telepon termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.06',
            'description' => 'Jaringan distribusi tenaga lisrik tegangan menengah, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.07',
            'description' => 'Jaringan distribusi tenaga lisrik tegangan rendah, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.08',
            'description' => 'Jaringan distribusi telekomunikasi dan atau telepon termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.09',
            'description' => 'Instalasi kontrol & instrumentasi, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.10',
            'description' => 'Instalasi listrik gedung dan pabrik, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.14.11',
            'description' => 'Instalasi listrik lainnya, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.15',
            'description' => 'Tata Lingkungan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.15.01',
            'description' => 'Perpipaan minyak termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.15.02',
            'description' => 'Perpipaan gas termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.15.03',
            'description' => 'Perpipaan air bersih/limbah termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.15.04',
            'description' => 'Pengolahan air bersih, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.15.05',
            'description' => 'Instalasi pengolahan limbah, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.15.06',
            'description' => 'Pekerjaan pengeboran air tanah, termasuk perawatannya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.15.07',
            'description' => 'Reboisasi / penghijauan, termasuk perawatannya
            ',
        ]);
        App\SubBidang::create([
            'code' => 'Q.21',
            'description' => 'Layanan Jasa Inspeksi Teknis',
        ]);
        App\SubBidang::create([
            'code' => 'Q.21.01',
            'description' => 'Jasa Enjinering Fase Konstruksi dan Instalasi Bangunan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.21.02',
            'description' => 'Jasa Enjinering Fase Konstruksi dan Instalasi Pekerjaan Teknik Sipil Transportasi',
        ]);
        App\SubBidang::create([
            'code' => 'Q.21.03',
            'description' => 'Jasa Enjinering Fase Konstruksi dan Instalasi Pekerjaan Teknik Sipil Keairan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.21.04',
            'description' => 'Jasa Enjinering Fase Konstruksi dan Instalasi Pekerjaan Teknik Sipil Lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.21.05',
            'description' => 'Jasa Enjinering Fase Konstruksi dan Instalasi Industrial Plant dan Proses',
        ]);
        App\SubBidang::create([
            'code' => 'Q.21.06',
            'description' => 'Jasa Enjinering Fase Konstruksi dan Instalasi Sistem Kontrol Lalulintas',
        ]);
        App\SubBidang::create([
            'code' => 'Q.21.07',
            'description' => 'Jasa Enjinering Fase Konstruksi dan Instalasi Sistem Kontrol Lalulintas',
        ]);
        App\SubBidang::create([
            'code' => 'Q.22.01',
            'description' => 'Jasa Manajemen Proyek Terkait Konstruksi Bangunan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.22.02',
            'description' => 'Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Teknik Sipil Transportasi',
        ]);
        App\SubBidang::create([
            'code' => 'Q.22.03',
            'description' => 'Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Teknik Keairan',
        ]);
        App\SubBidang::create([
            'code' => 'Q.22.04',
            'description' => 'Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Teknik Sipil Lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'Q.22.05',
            'description' => 'Jasa Manajemen Proyek Terkait Konstruksi Industrial Plant & Proses',
        ]);
        App\SubBidang::create([
            'code' => 'Q.22.06',
            'description' => 'Jasa Manajemen Proyek Terkait Konstruksi Sistem Kontrol Lalu-lintas',
        ]);
        App\SubBidang::create([
            'code' => 'Q.23.01',
            'description' => 'Layanan Jasa Enginering Terpadu',
        ]);
        App\SubBidang::create([
            'code' => 'R.01',
            'description' => 'Logam, Kayu dan Plastik',
        ]);
        App\SubBidang::create([
            'code' => 'R.01.01',
            'description' => 'Pembangunan kapal dan alat apung lainnya serta sarana Iepas pantai',
        ]);
        App\SubBidang::create([
            'code' => 'R.01.02',
            'description' => 'Pengangkatan kerangka Rapal dan alat apung lainnya serta sarana lepas pantai',
        ]);
        App\SubBidang::create([
            'code' => 'R.01.03',
            'description' => 'Pemotongan kapal dan alat apung lainnya serta sarana lepas pantai',
        ]);
        App\SubBidang::create([
            'code' => 'R.01.04',
            'description' => 'Karoseri, peti kemas, dll',
        ]);
        App\SubBidang::create([
            'code' => 'R.01.05',
            'description' => 'Pengecoran logam dan pembentukan logam',
        ]);
        App\SubBidang::create([
            'code' => 'R.01.06',
            'description' => 'Produk kayu dan rotan, serta pengawetannya',
        ]);
        App\SubBidang::create([
            'code' => 'R.01.07',
            'description' => 'Bahan baku dan produk plastik, serta kompositnya',
        ]);
        App\SubBidang::create([
            'code' => 'R.01.08',
            'description' => 'Pembuatan mesln dan peralatan Industri, rnekanikal dan elektrikal',
        ]);
        App\SubBidang::create([
            'code' => 'R.02',
            'description' => 'Pertanian, Tanaman Pangan, Perkebunan, Peternakan, Perikanan & Kehutanan',
        ]);
        App\SubBidang::create([
            'code' => 'R.02.01',
            'description' => 'Proses pemblbitan pembenihan tanaman pangan, peternakan, perlkanan, perkebunan dan
            kehutanan',
        ]);
        App\SubBidang::create([
            'code' => 'R.02.02',
            'description' => 'Reboisasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.03',
            'description' => 'Pertambangan Umum',
        ]);
        App\SubBidang::create([
            'code' => 'R.03.01',
            'description' => 'Eksplorasi Pertambangan',
        ]);
        App\SubBidang::create([
            'code' => 'R.03.02',
            'description' => 'Pengupasan',
        ]);
        App\SubBidang::create([
            'code' => 'R.03.03',
            'description' => 'Penggalian Penambangan',
        ]);
        App\SubBidang::create([
            'code' => 'R.03.04',
            'description' => 'Pengolahan pemurnian',
        ]);
        App\SubBidang::create([
            'code' => 'R.04',
            'description' => 'Pertambangan Minyak dan Gas Bumi',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.01',
            'description' => 'Pemboran',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.02',
            'description' => 'Pemboran berarah',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.03',
            'description' => 'Pengukuran kemiringan sumur',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.04',
            'description' => 'Pemboran Inti',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.05',
            'description' => 'Pekerjaan Pancing',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.06',
            'description' => 'Mud logging',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.07',
            'description' => 'Well logging dan perforating',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.08',
            'description' => 'Penyemenan sumur',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.09',
            'description' => 'Pengujlan lapisan bawah tanah',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.10',
            'description' => 'Pengujian Produksi Sumur',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.11',
            'description' => 'Stimulasl sumur dan penambangan sekunder',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.12',
            'description' => 'Perawatan sumur',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.13',
            'description' => 'Pemboran hidrolik',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.14',
            'description' => 'Pekerjaan ulang workover',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.15',
            'description' => 'Pemboran seismik',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.16',
            'description' => 'Pelayanan casing dan tubin',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.17',
            'description' => 'Mud engineering',
        ]);
        App\SubBidang::create([
            'code' => 'R.04.18',
            'description' => 'Perawatan fasllltas produksi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05',
            'description' => 'Telematika',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.01',
            'description' => 'Jasa teknologi lnformasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.02',
            'description' => 'Komunikasi multimedia',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.03',
            'description' => 'Telekomunikasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.04',
            'description' => 'Navigasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.05',
            'description' => 'Kontrol & instrumentasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.06',
            'description' => 'Penginderaan jauh',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.07',
            'description' => 'Jasa pemborongan telekomunikasi darat',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.07.01',
            'description' => 'Sentral',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.07.02',
            'description' => 'Transmisi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.07.03',
            'description' => 'Jaringan telekomunikasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.07.04',
            'description' => 'Teknologi dan sistem informasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.07.05',
            'description' => 'Networking',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.07.06',
            'description' => 'Sistem pemancar dan penerima radio dan televisi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.07.07',
            'description' => 'Kontrol dan instrumental',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.08',
            'description' => 'Jasa Pemborongan telekomunikasi satelit',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.08.01',
            'description' => 'Sentral',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.08.02',
            'description' => 'Transmisi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.08.03',
            'description' => 'Jaringan telekomunikasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.08.04',
            'description' => 'Teknologi dan sistem informasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.08.05',
            'description' => 'Networking',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.08.06',
            'description' => 'Sistem pemancar dan penerima radio dan televisi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.08.07',
            'description' => 'Kontrol dan instrumental',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.09',
            'description' => 'Jasa pemborongan perangkat keras',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.09.01',
            'description' => 'Komputer',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.09.02',
            'description' => 'Printer',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.09.03',
            'description' => 'Projector multimedia',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.09.04',
            'description' => 'Input device',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.09.05',
            'description' => 'Alat penyimpan data',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.09.06',
            'description' => 'Networking product',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.09.07',
            'description' => 'Accessories dan supplies',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.09.08',
            'description' => 'Perangkat sistem informasi khusus',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.10',
            'description' => 'Jasa pengembangan konten',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.10.01',
            'description' => 'Kontens design learing',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.10.02',
            'description' => 'Konten program TV Interactive',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.10.03',
            'description' => 'Konten program multimedia',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.10.04',
            'description' => 'Konten program portal',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.11',
            'description' => 'Jasa Pengembang aplikasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.11.01',
            'description' => 'Aplikasi komputer',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.11.02',
            'description' => 'Aplikasi komunikasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.11.03',
            'description' => 'Aplikasi telemetrik',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.11.04',
            'description' => 'Aplikasi GIS',
        ]);
        App\SubBidang::create([
            'code' => 'R.05.11.05',
            'description' => 'Aplikasi GPS',
        ]);
        App\SubBidang::create([
            'code' => 'R.06',
            'description' => 'Jasa Real Estate',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.01.01',
            'description' => 'Perumahan dan Pemukiman',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.01.02',
            'description' => 'Perumahan',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.01.03',
            'description' => 'Rumah Susun',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.01.03',
            'description' => 'Kawasan Perumahan',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.02',
            'description' => 'Pengembangan properti',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.02.01',
            'description' => 'Gedung/ruang perkantoran',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.02.02',
            'description' => 'Gedung/ruang perbelanjaan dan pertokoan',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.02.03',
            'description' => 'Kawasan rekreasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.02.03',
            'description' => 'Kawasan rekreasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.02.04',
            'description' => 'Kawasan agro estate',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.02.05',
            'description' => 'Kawasan gedung peristirahatan/villa/resort',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.03',
            'description' => 'Pengembangan kawasan industri',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.03.01',
            'description' => 'Gedung/ruang industri',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.03.02',
            'description' => 'Gedung Pergudangan',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.04',
            'description' => 'Manajemen properti',
        ]);
        App\SubBidang::create([
            'code' => 'R.06.05',
            'description' => 'Jasa perantara (brokerage) real estate dan properti',
        ]);
        App\SubBidang::create([
            'code' => 'R.07',
            'description' => 'Jasa Lain-lain',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.01',
            'description' => 'Percetakan dan penjilidan',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.02',
            'description' => 'Pemeliharaan/perbaikan alat/peralatan kantor',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.03',
            'description' => 'Pemeliharaan/perbaikan pustaka, barang-barang awetan, fauna dan lain-lain',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.04',
            'description' => 'Jasa pembersihan, pest control, termite control',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05',
            'description' => 'Pengepakan, pengangkutan, pengurusan dan penyampaian barang melalui darat/laut/udara',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.01',
            'description' => 'Pengepakan, pengangkutan, pengurusan dan  penyampaian barang melalui darat',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.02',
            'description' => 'Pengepakan, pengangkutan, pengurusan dan  penyampaian barang melalui laut',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.03',
            'description' => 'Pengepakan, pengangkutan, pengurusan dan  penyampaian barang melalui udara',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.04',
            'description' => 'Jasa Angkutan BBM/BBG',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.05',
            'description' => 'Jasa Angkutan multi moda',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.06',
            'description' => 'Jasa Angkutan hewan melalui darat, laut dan udara',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.07',
            'description' => 'Jasa peluncuran satelit',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.08',
            'description' => 'Jasa ekspedisi dan kepabeanan',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.09',
            'description' => 'Jasa bongkar muat barang',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.10',
            'description' => 'Jasa pengiriman ekspres',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.11',
            'description' => 'Pemeliharaan alat angkutan laut termasuk perbaikan kapal',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.12',
            'description' => 'Jasa distribusi dan pemasaran BBM dan Petro-Kimia',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.13',
            'description' => 'Pemeliharaan alat angkutan darat',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.05.14',
            'description' => 'Sub bidang lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.06',
            'description' => 'Penjahitan/konpeksi',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.07',
            'description' => 'Jasa boga',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.08',
            'description' => 'Jasa importir/eksportir',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.09',
            'description' => 'Jasa perawatan komputer, alat/peralatan elektronik & telekomunikasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.09.01',
            'description' => 'Perawatan alat/peralatan & aplikasi komputer',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.09.02',
            'description' => 'Perawatan Perangkat keras komputer',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.09.03',
            'description' => 'Perawatan Jaringan Komputer dan Jaringan Internet',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.09.04',
            'description' => 'Perawatan Perangkat Lunak Aplikasi dan Software Pendukung',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.09.05',
            'description' => 'Perawatan Konten',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.09.06',
            'description' => 'Jasa Pemelihara dan Perawatan alat/peralatan Telekomunikasi Darat',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.09.07',
            'description' => 'Jasa Pemelihara dan Perawatan alat/peralatan Telekomunikasi Satelit',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.10',
            'description' => 'Iklan/reklame, film, pemotretan',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.11',
            'description' => 'Jasa penulisan dan penerjemahan',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.12',
            'description' => 'Penyedia tenaga kerja',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.13',
            'description' => 'Penyewaan alat angkutan darat/laut/udara',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.13.01',
            'description' => 'Penyewaan alat angkutan darat',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.13.02',
            'description' => 'Penyewaan alat angkutan laut',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.13.03',
            'description' => 'Penyewaan alat angkutan Udara',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.14',
            'description' => 'Jasa penyelaman/pekerjaan bawah air',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.15',
            'description' => 'Jasa asuransi',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.16',
            'description' => 'Pengadaan/pembebasan tanah',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.17',
            'description' => 'Akomodasi, Jasa Perjalanan dan Penyelenggara Acara',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.17.01',
            'description' => 'Akomodasi, Jasa Perjalanan',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.17.02',
            'description' => 'Penyelenggara Acara',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.18',
            'description' => 'Jasa Inspeksi / Quality Control',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.19',
            'description' => 'Jasa  pengolahan  panas  dan  permukaan  logam,  pengujian  dan  kalibrasi,  pemeliharaan  dan
            reparasi mesin dan peralatan industri',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.19.01',
            'description' => 'Jasa pengolahan panas dan permukaan logam',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.19.02',
            'description' => 'Jasa pengujian dan kalibrasi',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.19.03',
            'description' => 'Jasa pemeliharaan dan reparasi mesin dan peralatan industri',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.19.04',
            'description' => 'Sub bidang lainnya',
        ]);
        App\SubBidang::create([
            'code' => 'R.07.20',
            'description' => 'Pekerjaan jasa lainnya',
        ]);
    }
}
