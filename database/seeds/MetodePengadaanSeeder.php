<?php

use Illuminate\Database\Seeder;

class MetodePengadaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\MetodePengadaan::create(['nama' => 'Sinergi Pertamina Incorporated']);
        App\MetodePengadaan::create(['nama' => 'Tender Terbuka']);
        App\MetodePengadaan::create(['nama' => 'Tender Terbatas']);
        App\MetodePengadaan::create(['nama' => 'Penunjukan Langsung']);
    }
}