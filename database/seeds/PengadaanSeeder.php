<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PengadaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $judul = array('Judul 1', 'Judul 2', 'Judul 3');
        $id_kategori = array(1, 2, 3);
        $tender = array(1, 2, 1);
        $jenis_anggaran = array('ABO', 'ABI' , 'ABO');
        $jenis_pengadaan = array('barang', 'jasa', 'barang & jasa');
        $tahun_anggaran = array('2019', '2020', '2021');
        $id_fpp = array(1, 2, 3);
        $no_pr = array(null, 50, 100);
        $id_metode_pengadaan = array(1, 2, 3);
        $kategori_csms = array('Non Risk', 'Low Risk', 'Non Risk');
        $id_pengadaan_pic = array(1, 2, 3);
        $status = array(1, 2, 1);
        $id_mengajukan = array(2, 1, 1);
        $id_mengetahui = array(2, 1, 3);
        $id_menyetujui = array(3, 2, 1);
        
        for ($i = 0; $i < 10; $i++){
            App\Pengadaan::create([
                'judul' => $judul[$i%3],
                'id_kategori' => $id_kategori[$i%3],
                'tender' => $tender[$i%3],
                'jenis_anggaran' => $jenis_anggaran[$i%3],
                'jenis_pengadaan' => $jenis_pengadaan[$i%3],
                'tahun_anggaran' => $tahun_anggaran[$i%3],
                'id_fpp' => $id_fpp[$i%3],
                'no_pr' => $no_pr[$i%3],
                'id_metode_pengadaan' => $id_metode_pengadaan[$i%3],
                'kategori_csms' => $kategori_csms[$i%3],
                'id_pengadaan_pic' => $id_pengadaan_pic[$i%3],
                'status' => 1,
                'id_mengajukan' => 3,
                'id_mengetahui' => 1,
                'id_menyetujui' => 2,
                'created_at' => Carbon::now()->subDays(rand(0, 365))->subMinutes(rand(0, 59))->format('Y-m-d H:i:s'),
            ]);
        }
        for ($i = 0; $i < 10; $i++){
            App\Pengadaan::create([
                'judul' => $judul[$i%3],
                'id_kategori' => $id_kategori[$i%3],
                'tender' => $tender[$i%3],
                'jenis_anggaran' => $jenis_anggaran[$i%3],
                'jenis_pengadaan' => $jenis_pengadaan[$i%3],
                'tahun_anggaran' => $tahun_anggaran[$i%3],
                'id_fpp' => $id_fpp[$i%3],
                'no_pr' => $no_pr[$i%3],
                'id_metode_pengadaan' => $id_metode_pengadaan[$i%3],
                'kategori_csms' => $kategori_csms[$i%3],
                'id_pengadaan_pic' => $id_pengadaan_pic[$i%3],
                'status' => 2,
                'id_mengajukan' => 3,
                'id_mengetahui' => 1,
                'id_menyetujui' => 2,
                'created_at' => Carbon::now()->subDays(rand(0, 365))->subMinutes(rand(0, 59))->format('Y-m-d H:i:s'),
            ]);
        }
        for ($i = 0; $i < 10; $i++){
            App\Pengadaan::create([
                'judul' => $judul[$i%3],
                'id_kategori' => $id_kategori[$i%3],
                'tender' => $tender[$i%3],
                'jenis_anggaran' => $jenis_anggaran[$i%3],
                'jenis_pengadaan' => $jenis_pengadaan[$i%3],
                'tahun_anggaran' => $tahun_anggaran[$i%3],
                'id_fpp' => $id_fpp[$i%3],
                'no_pr' => $no_pr[$i%3],
                'id_metode_pengadaan' => $id_metode_pengadaan[$i%3],
                'kategori_csms' => $kategori_csms[$i%3],
                'id_pengadaan_pic' => $id_pengadaan_pic[$i%3],
                'status' => 3,
                'id_mengajukan' => 3,
                'id_mengetahui' => 1,
                'id_menyetujui' => 2,
                'created_at' => Carbon::now()->subDays(rand(0, 365))->subMinutes(rand(0, 59))->format('Y-m-d H:i:s'),
            ]);
        }
    }
}