<?php

use Illuminate\Database\Seeder;

class RiwayatPengadaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 14; $i++){
            App\RiwayatPengadaan::create([
                'id_pengadaan' => $i+1,
                'tahapan_pengadaan' => $i+1,
                'keterangan' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates quam eius dolorum quos ea eum adipisci magnam earum quidem omnis, hic dignissimos molestias animi asperiores quaerat! Illum aut labore id.
                ',
            ]);
        }
    }
}