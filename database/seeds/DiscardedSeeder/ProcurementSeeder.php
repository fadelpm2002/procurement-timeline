<?php

use Illuminate\Database\Seeder;

class ProcurementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('procurements')->insert([
            'fpp' => 'Pendidikan',
            'judul_pekerjaan' => 'Memperbaiki pendidikan sekolah menengah',
            'tahun_anggaran' => '2020',
            'pic_procurement' => '1',
            'id_kategori' => '1',
            'id_jenis_pengadaan' => '1',
            'id_status' => '1',
            'id_tahapan' => '1',
        ]);
        DB::table('procurements')->insert([
            'fpp' => 'Pendidikan',
            'judul_pekerjaan' => 'Memperbaiki pendidikan sekolah dasar',
            'tahun_anggaran' => '2021',
            'pic_procurement' => '1',
            'id_kategori' => '1',
            'id_jenis_pengadaan' => '1',
            'id_status' => '1',
            'id_tahapan' => '1',
        ]);
        DB::table('procurements')->insert([
            'fpp' => 'Pendidikan',
            'judul_pekerjaan' => 'Merusak pendidikan sekolah dasar',
            'tahun_anggaran' => '2021',
            'pic_procurement' => '1',
            'id_kategori' => '1',
            'id_jenis_pengadaan' => '1',
            'id_status' => '1',
            'id_tahapan' => '1',
        ]);
        DB::table('procurements')->insert([
            'fpp' => 'Pendidikan',
            'judul_pekerjaan' => 'Mememe pendidikan sekolah dasar',
            'tahun_anggaran' => '2021',
            'pic_procurement' => '1',
            'id_kategori' => '1',
            'id_jenis_pengadaan' => '1',
            'id_status' => '1',
            'id_tahapan' => '1',
        ]);
        DB::table('procurements')->insert([
            'fpp' => 'Budaya',
            'judul_pekerjaan' => 'Pantai sebagai sarana pariwisata',
            'tahun_anggaran' => '2020',
            'pic_procurement' => '1',
            'id_kategori' => '2',
            'id_jenis_pengadaan' => '2',
            'id_status' => '2',
            'id_tahapan' => '2',
        ]);
    }
}