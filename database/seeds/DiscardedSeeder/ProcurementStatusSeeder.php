<?php

use Illuminate\Database\Seeder;

class ProcurementStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('procurement_statuses')->insert([
            'nama_status_pengadaan' => 'On Hold',
        ]);
        DB::table('procurement_statuses')->insert([
            'nama_status_pengadaan' => 'On Going',
        ]);
        DB::table('procurement_statuses')->insert([
            'nama_status_pengadaan' => 'Done',
        ]);
        DB::table('procurement_statuses')->insert([
            'nama_status_pengadaan' => 'Gagal',
        ]);
        DB::table('procurement_statuses')->insert([
            'nama_status_pengadaan' => 'Batal',
        ]);
    }
}