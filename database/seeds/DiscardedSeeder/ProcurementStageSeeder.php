<?php

use Illuminate\Database\Seeder;

class ProcurementStageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Done',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'DP3 Belum Lengkap',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Pengumuman Tender',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Prebid',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Pemasukan Penawaran',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Evaluasi Admin',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Evaluasi Teknis',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Negosiasi',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'LHP',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Pengumuman Pemenang',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'PO',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Kontrak',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Gagal',
        ]);
        DB::table('procurement_stages')->insert([
            'nama_tahapan_pengadaan' => 'Batal',
        ]);        
    }
}