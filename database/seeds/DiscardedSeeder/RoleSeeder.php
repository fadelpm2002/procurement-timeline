<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'nama_role' => 'super admin',
        ]);
        DB::table('roles')->insert([
            'nama_role' => 'buyer',
        ]);
        DB::table('roles')->insert([
            'nama_role' => 'sekretaris',
        ]);
    }
}