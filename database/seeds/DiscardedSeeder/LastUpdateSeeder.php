<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class LastUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('procurement_last_updates')->insert([
            'id_pengadaan' => '1',
            'deskripsi_update' => 'update 1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('procurement_last_updates')->insert([
            'id_pengadaan' => '1',
            'deskripsi_update' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat, tempore?
            ',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('procurement_last_updates')->insert([
            'id_pengadaan' => '2',
            'deskripsi_update' => 'update 1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('procurement_last_updates')->insert([
            'id_pengadaan' => '3',
            'deskripsi_update' => 'update 1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}