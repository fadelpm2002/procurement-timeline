<?php

use Illuminate\Database\Seeder;

class TahapanPengadaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Tahapan::create([
            'nama' => 'Pra Tender',
            'status_pengadaan' => '4',
        ]);
        // App\Tahapan::create([
        //     'nama' => 'DP3 Belum Lengkap',
        //     'status_pengadaan' => '5',
        // ]);
        App\Tahapan::create([
            'nama' => 'Pengumuman Tender',
            'status_pengadaan' => '4',
        ]);
        App\Tahapan::create([
            'nama' => 'Evaluasi Prakualifikasi',
            'status_pengadaan' => '5',
        ]);
        App\Tahapan::create([
            'nama' => 'Prebid Meeting',
            'status_pengadaan' => '4',
        ]);
        App\Tahapan::create([
            'nama' => 'Prebid Lapangan',
            'status_pengadaan' => '4',
        ]);
        App\Tahapan::create([
            'nama' => 'Pemasukan Penawaran',
            'status_pengadaan' => '4',
        ]);
        App\Tahapan::create([
            'nama' => 'Evaluasi Sampul',
            'status_pengadaan' => '4',
        ]);
        App\Tahapan::create([
            'nama' => 'Negosiasi',
            'status_pengadaan' => '4',
        ]);
        App\Tahapan::create([
            'nama' => 'LHP',
            'status_pengadaan' => '4',
        ]);
        // App\Tahapan::create([
        //     'nama' => 'Evaluasi oleh FPP',
        //     'status_pengadaan' => '5',
        // ]);
        App\Tahapan::create([
            'nama' => 'Pengumuman Pemenang',
            'status_pengadaan' => '4',
        ]);
        App\Tahapan::create([
            'nama' => 'PO & Kontrak',
            'status_pengadaan' => '6',
        ]);
        // App\Tahapan::create([
        //     'nama' => 'Kontrak',
        //     'status_pengadaan' => '6',
        // ]);
        App\Tahapan::create([
            'nama' => 'Done',
            'status_pengadaan' => '6',
        ]);
        App\Tahapan::create([
            'nama' => 'Batal',
            'status_pengadaan' => '7',
        ]);
        App\Tahapan::create([
            'nama' => 'Gagal',
            'status_pengadaan' => '8',
        ]);
    }
}