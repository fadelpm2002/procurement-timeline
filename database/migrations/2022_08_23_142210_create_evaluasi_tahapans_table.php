<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluasiTahapansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluasi_tahapans', function (Blueprint $table) {
            $table->id();
            $table->integer('riwayat_pengadaan_id')->nullable();
            // $table->string('plan_start')->nullable();
            // $table->string('plan_end')->nullable();
            $table->string('plan_hk')->nullable();
            $table->string('aktual_start')->nullable();
            $table->string('aktual_end')->nullable();
            $table->string('actual_hk')->nullable();
            $table->text('alasan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluasi_tahapans');
    }
}
