<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengadaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengadaans', function (Blueprint $table) {
            $table->id();
            $table->string('judul')->nullable();
            $table->integer('id_kategori')->nullable();
            $table->string('tender')->nullable();
            $table->enum('jenis_anggaran', ['ABO', 'ABI'])->nullable();
            $table->enum('jenis_pengadaan', ['barang', 'jasa', 'barang & jasa'])->nullable();
            $table->string('tahun_anggaran')->nullable();
            $table->string('dp3_diterima_lengkap')->nullable();
            $table->integer('id_fpp')->nullable();
            $table->string('plan_evaluasi_FPP')->nullable(); //
            $table->string('no_pr')->nullable();
            $table->integer('id_metode_pengadaan')->nullable();
            $table->enum('kategori_csms', ['non risk', 'low risk', 'medium risk', 'high risk'])->nullable();
            $table->string('id_pengadaan_pic')->nullable();
            $table->text('sub_bidang')->nullable();
            $table->string('plan_dp_3_belum_lengkap')->nullable();
            $table->string('plan_prebid_meeting')->nullable();
            //penawaran
            $table->string('plan_pemasukan_penawaran')->nullable(); //
            $table->string('plan_evaluasi_sampul')->nullable(); // 
            $table->string('plan_negosiasi')->nullable(); //
            $table->decimal('nilai_oe', 20, 2)->nullable();
            $table->decimal('harga_penawaran', 20, 2)->nullable();
            $table->decimal('harga_setelah_negosiasi', 20, 2)->nullable();
            $table->string('nama_penyedia')->nullable();
            $table->string('plan_pengumuman_tender')->nullable(); //
            $table->string('plan_penunjukan_pemenang')->nullable(); //
            $table->string('pengumuman_tender')->nullable(); //
            $table->string('penunjukan_pemenang')->nullable(); //
            $table->string('waktu_koreksi')->nullable();
            $table->string('penyelesain_proses_pengadaan')->nullable();
            //pre tender
            $table->string('plan_prebid_lapangan')->nullable();
            $table->string('pre_tender_no_nr')->nullable();
            $table->string('pre_tender_tanggal')->nullable();
            //lhp
            $table->string('plan_lhp_plan')->nullable(); //
            $table->string('lhp_no')->nullable();
            $table->string('lhp_tgl')->nullable();
            //po
            $table->string('plan_po_plan')->nullable(); //
            $table->string('po_no')->nullable();
            $table->string('po_tgl')->nullable();
            //kontrak
            $table->string('plan_kontrak_plan')->nullable(); //
            $table->string('kontrak_no')->nullable();
            $table->string('kontrak_tgl')->nullable();
            $table->string('kontrak_jenis')->nullable();
            $table->string('kontrak_masa')->nullable();
            //ba
            $table->string('ba_aanwijzing_no')->nullable();
            $table->string('ba_aanwijzing_tanggal')->nullable();
            $table->string('ba_aanwijzing_lapangan_no')->nullable();
            $table->string('ba_aanwijzing_lapangan_tanggal')->nullable();
            $table->string('ba_negosiasi_no')->nullable();
            $table->string('ba_negosiasi_tanggal')->nullable();
            //tkdn
            $table->string('form_a2_tkdn')->nullable();
            $table->string('form_a3_nilai_tkdn_barang')->nullable();
            $table->string('tkdn_barang')->nullable();
            $table->string('form_a4_nilai_tkdn_jasa')->nullable();
            $table->string('tkdn_jasa')->nullable();
            $table->string('form_a5_nilai_tkdn_jasa_barang')->nullable();
            $table->string('tkdn_jasa_barang')->nullable();
            //mandatori dokumen
            $table->integer('id_mengajukan')->nullable();
            $table->string('ttd_mengajukan')->nullable();
            $table->integer('id_mengetahui')->nullable();
            $table->string('ttd_mengetahui')->nullable();
            $table->integer('id_menyetujui')->nullable();
            $table->string('ttd_menyetujui')->nullable();
            $table->text('keterangan_reject')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengadaans');
    }
}