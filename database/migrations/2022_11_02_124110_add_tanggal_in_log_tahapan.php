<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTanggalInLogTahapan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_tahapans', function (Blueprint $table) {
            $table->string('tanggal_description')->nullable()->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('log_tahapans','tanggal_description'))
        {

          Schema::table('log_tahapans', function (Blueprint $table)
          {
            $table->dropColumn('tanggal_description');
          });
        }
    }
}
