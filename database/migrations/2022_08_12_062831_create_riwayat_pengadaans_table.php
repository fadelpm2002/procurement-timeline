<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiwayatPengadaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_pengadaans', function (Blueprint $table) {
            $table->id();
            $table->integer('id_pengadaan');
            $table->integer('status_pengadaan')->nullable();
            $table->integer('tahapan_pengadaan')->nullable();
            $table->text('keterangan')->nullable();
            $table->integer('pemeroses_id')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_pengadaans');
    }
}