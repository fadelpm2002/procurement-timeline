<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procurements', function (Blueprint $table) {
            $table->id();
            $table->string('fpp');
            $table->string('judul_pekerjaan');
            $table->string('tahun_anggaran');
            $table->integer('pic_procurement');
            $table->integer('id_kategori');
            $table->integer('id_jenis_pengadaan');
            $table->foreignId('id_status')->references('id')->on('procurement_statuses')->onDelete('cascade')->nullable();
            $table->foreignId('id_tahapan')->references('id')->on('procurement_stages')->onDelete('cascade')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procurements');
    }
}