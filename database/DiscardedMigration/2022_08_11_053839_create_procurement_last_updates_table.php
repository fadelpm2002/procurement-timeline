<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcurementLastUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procurement_last_updates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_pengadaan')->references('id')->on('procurements')->onDelete('cascade')->nullable();
            $table->string('deskripsi_update');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procurement_last_updates');
    }
}