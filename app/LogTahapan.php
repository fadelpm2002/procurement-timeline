<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogTahapan extends Model
{
    protected $fillable = [
        'riwayat_pengadaan_id',
        'title',
        'tanggal_description',
        'user_id',
        'keterangan',
    ];
}
