<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengadaan extends Model
{
    protected $fillable = [
       'judul',
        'id_kategori',
       'tender',
        'jenis_anggaran',
        'jenis_pengadaan',
       'tahun_anggaran',
       'dp3_diterima_lengkap',
        'id_fpp',
       'plan_evaluasi_FPP', 
       'no_pr',
        'id_metode_pengadaan',
        'kategori_csms',
        'id_pengadaan_pic',
       'sub_bidang',
       'plan_dp_3_belum_lengkap',
       'plan_prebid_meeting',
        //penawaran
       'plan_pemasukan_penawaran',
       'plan_evaluasi_sampul', 
       'plan_negosiasi',
       'nilai_oe',
       'harga_penawaran',
       'harga_setelah_negosiasi',
       'nama_penyedia',
       'plan_pengumuman_tender',
       'plan_penunjukan_pemenang',
       'pengumuman_tender',
        'penunjukan_pemenang',
       'waktu_koreksi',
       'penyelesain_proses_pengadaan',
        //pre tender
       'plan_prebid_lapangan',
       'pre_tender_no_nr',
       'pre_tender_tanggal',
        //lhp
       'plan_lhp_plan',
       'lhp_no',
       'lhp_tgl',
        //po
       'plan_po_plan',
       'po_no',
       'po_tgl',
        //kontrak
       'plan_kontrak_plan',
       'kontrak_no',
       'kontrak_tgl',
       'kontrak_jenis',
       'kontrak_masa',
        //ba
       'ba_aanwijzing_no',
       'ba_aanwijzing_tanggal',
       'ba_aanwijzing_lapangan_no',
       'ba_aanwijzing_lapangan_tanggal',
       'ba_negosiasi_no',
       'ba_negosiasi_tanggal',
        //tkdn
       'form_a2_tkdn',
       'form_a3_nilai_tkdn_barang',
       'tkdn_barang',
       'form_a4_nilai_tkdn_jasa',
       'tkdn_jasa',
       'form_a5_nilai_tkdn_jasa_barang',
       'tkdn_jasa_barang',
        //mandatori dokumen
        'id_mengajukan',
       'ttd_mengajukan',
        'id_mengetahui',
       'ttd_mengetahui',
        'id_menyetujui',
       'ttd_menyetujui',
        'keterangan_reject',
        'status',
    ];
}