<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetodePengadaan extends Model
{
    protected $fillable = [
        'nama',
    ];
}