<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcurementStage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_tahapan_pengadaan', 
    ];

    public function pengadaan()
    {
       return $this->hasMany('App\Procurement', 'id_tahapan');
    }
}