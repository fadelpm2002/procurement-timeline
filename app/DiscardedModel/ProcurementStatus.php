<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcurementStatus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_status_pengadaan', 
    ];

    public function pengadaan()
    {
       return $this->hasMany('App\Procurement', 'id_status');
    }
}