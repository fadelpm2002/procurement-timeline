<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcurementLastUpdate extends Model
{   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_pengadaan', 'deskripsi_update', 
    ];

    public function pengadaan()
    {
       return $this->belongsTo('App\Procurement', 'id_pengadaan');
    }
}