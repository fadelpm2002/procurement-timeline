<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_role', 
    ];

    // Relation
    public function users()
    {
        return $this->hasMany('App\User', 'id_role');
    }
}