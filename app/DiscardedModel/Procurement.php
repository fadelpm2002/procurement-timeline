<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procurement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fpp', 'judul_pekerjaan', 'tahun_anggaran', 'pic_procurement', 'id_kategori', 'id_jenis_pengadaan', 'id_status', 'id_tahapan',
    ];

    public function statusPengadaan()
    {
        return $this->belongsTo('App\ProcurementStatus', 'id_status');
    }

    public function tahapanPengadaan()
    {
        return $this->belongsTo('App\ProcurementStage', 'id_tahapan');
    }

    public function updateTerakhirPengadaan()
    {
        return $this->hasMany('App\ProcurementLastUpdate', 'id_pengadaan');
    }
}