<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth::check() && Auth::user()->id_role == 1){ // super user
            return $next($request);
        }else if (auth::check() && Auth::user()->id_role == 2){ // buyer
            return $next($request);
        }else if (auth::check() && Auth::user()->id_role == 3){ // sekretaris
            return $next($request);
        }else {
            return redirect()->route('login');
        }
    }
}