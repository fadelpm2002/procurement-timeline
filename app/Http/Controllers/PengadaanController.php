<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pengadaan;
use App\RiwayatPengadaan;
use App\FungsiPemintaPengadaan;
use App\MetodePengadaan;
use App\Kategori;
use App\User;
use App\EvaluasiTahapan;
use App\Tahapan;
use App\LogTahapan;
use App\SubBidang;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Traits\PengadaanTrait;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportPengadaan;
use App\Exports\ExportEvaluasiTahapan;
use Illuminate\Support\Facades\Validator;


class PengadaanController extends Controller
{   
    use PengadaanTrait;

    public function pengadaanIndexTimeline()
    {
        $role = Auth::user()->id_role;
        $data = [];
        if($role == 1 || $role == 3){
            $pengadaan = Pengadaan::get();
            $data = $this->ambilPengadaans($pengadaan);
        }else{
            $pengadaan = Pengadaan::where('id_pengadaan_pic','like', '%'.Auth::user()->id.'%')->get();
            $data = $this->ambilPengadaans($pengadaan);
        }
        return view('pengadaan.timeline', compact('data'));
    }
    public function pengadaanIndex()
    {
        $role = Auth::user()->id_role;
        $data = [];
        if($role == 1 || $role == 3){
            $pengadaan = Pengadaan::get();
            $data = $this->ambilPengadaans($pengadaan);
        }else{
            $pengadaan = Pengadaan::where('id_pengadaan_pic','like', '%'.Auth::user()->id.'%')->get();
            $data = $this->ambilPengadaans($pengadaan);
        }
        // return response()->json($data, 200);
        return view('pengadaan.index', compact('data'));
    }

    public function pengadaanUbahStatus(Request $request, $id){
        
        try {
            // dd($request->all());
            $pengadaan = Pengadaan::find($id);
            if ($request->status == 7) {
                $pengadaan->update([
                    'status'=> $request->status,
                ]);
                $riwayatLanjut = RiwayatPengadaan::Create([
                    'id_pengadaan' => $pengadaan->id, 
                    'status_pengadaan' => $request->status,
                    'tahapan_pengadaan' => 14,
                    'keterangan' => $request->keterangan,
                    'pemeroses_id'=> Auth::user()->id,
                    'status'=> 1,
                ]);
                $checkRiwayat = RiwayatPengadaan::where('id_pengadaan', $pengadaan->id)->orderBy('id', 'desc')->first();
                $checkRiwayat->update(['status' => 1]);

                $LogTahapan = LogTahapan::create([
                    'riwayat_pengadaan_id' => $riwayatLanjut->id,
                    'title' => 'Gagal Tahapan - '.$this->statusPengadaan($riwayatLanjut->status_pengadaan),
                    'user_id' => Auth::user()->id,
                    'keterangan' => $request->keterangan,
                ]);
            }elseif($request->status == 8){
                $pengadaan->update([
                    'status'=> $request->status,
                ]);
                $riwayatLanjut = RiwayatPengadaan::Create([
                    'id_pengadaan' => $pengadaan->id, 
                    'status_pengadaan' => $request->status,
                    'tahapan_pengadaan' => 13,
                    'keterangan' => $request->keterangan,
                    'pemeroses_id'=> Auth::user()->id,
                    'status'=> 1,
                ]);
                $checkRiwayat = RiwayatPengadaan::where('id_pengadaan', $pengadaan->id)->orderBy('id', 'desc')->first();
                $checkRiwayat->update(['status' => 1]);

                $LogTahapan = LogTahapan::create([
                    'riwayat_pengadaan_id' => $riwayatLanjut->id,
                    'title' => 'Batal Tahapan - '.$this->statusPengadaan($riwayatLanjut->status_pengadaan),
                    'user_id' => Auth::user()->id,
                    'keterangan' => $request->keterangan,
                ]);
            }
            return redirect()->back()->with('success', 'pengadaan berhasil dibatalkan');
        }catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        $data = [
            'id'=> $id, 
            'data' => $request->all(),
        ];
    }

    public function pengadaanExport(Request $request){
        $data = [
            'start' => $request->start,
            'end' => $request->end,
        ];
        return Excel::download(new ExportPengadaan($data), 'export-pengadaan.xlsx');
    }

    public function pengadaanTimelineFilter(Request $request){
        $id = $request->id_pengadaan;
        $riwayat = RiwayatPengadaan::where('id_pengadaan', $request->id_pengadaan);
        if ($request->filled('start') || $request->filled('end')){
            $validated = $request->validate([
                'start' => 'required',
                'end' => 'required',
            ]);
            $riwayat = $riwayat->whereDate('created_at', '>=', $request->start)
            ->whereDate('created_at', '<=', $request->end);
        }
        if ($request->filled('tahapan')){
            $riwayat = $riwayat->where('tahapan_pengadaan', $request->tahapan);
            
            
        }
        // return response()->json($riwayat->get());
        $riwayat = $riwayat->get();
        $data = [];
        foreach ($riwayat as $key => $item) {
            $data[$key] = [
                'id' => $item->id,
                'created_at' => $item->created_at->translatedFormat('d F Y'),
                'id_tahapan' => $item->tahapan_pengadaan,
                'tahapan_pengadaan' => $this->tahapanPengadaan($item->tahapan_pengadaan),
                'keterangan' => $item->keterangan,
            ];
        } 
        // return response()->json(['data' => $data, 'id' => $id]);
        return view('pengadaan.timeline', compact('data', 'id'));
    }

    public function pengadaanTimeline($id){
        $riwayat = RiwayatPengadaan::where('id_pengadaan', $id)->get();
        $data = [];
        $lastUpdateDesc = '';
        foreach ($riwayat as $key => $item) {
            $logs = [];
            $log = LogTahapan::where('riwayat_pengadaan_id', $item->id)->get();
            $lastUpdateLog = LogTahapan::where('riwayat_pengadaan_id', $item->id)
                            ->select('tanggal_description')
                            ->orderBy('id', 'desc')
                            ->first();

            if ($lastUpdateLog == null) {
                $lastUpdateDesc = '00-00-0000';
            }else{
                $lastUpdateDesc = Carbon::parse($lastUpdateLog->tanggal_description)->format('d F Y');
            }
            foreach ($log as $kuy => $logTahapan) {
                $logs[$kuy]= [
                    'id' => $logTahapan->id,
                    'riwayat_pengadaan_id' => $logTahapan->riwayat_pengadaan_id,
                    'title' => $logTahapan->title,
                    'user_id' => $this->userName($logTahapan->user_id),
                    'keterangan' => $logTahapan->keterangan,
                    'tanggal_description' => Carbon::parse($logTahapan->tanggal_description)->format('d F Y'),
                    'created_at' => Carbon::parse($logTahapan->created_at)->format('d F Y'),
                ];
            }
            $data[$key] = [
                'id' => $item->id,
                'tanggal_description' => $lastUpdateDesc,
                'created_at' => Carbon::parse($item->created_at)->format('d F Y'),
                'id_tahapan' => $item->tahapan_pengadaan,
                'id_tahapan_pengadaan' => $item->tahapan_pengadaan,
                'status' => $item->status,
                'tahapan_pengadaan' => $this->tahapanPengadaan($item->tahapan_pengadaan),
                'log' => $logs,
            ];
        }
        return response()->json($data, 200);
    }

    public function pengadaanUpdateStatus(Request $request, $id){
        $role = Auth::user()->id_role;
        // return response()->json(['data' => $request->all(), 'id' => $id]);
        if ($role >= 1 && $role <= 3){
            if ($request->status == 1){
                $pengadaan = Pengadaan::find($id)->update(['status' => 2]);
                RiwayatPengadaan::create([
                    'id_pengadaan' => $request->id,
                    'tahapan_pengadaan' => '20', // 'Persetujuan Pengadaan' hanya sebagai key di database 
                    'keterangan' => 'Persetujuan mengetahui oleh '.Auth::user()->nama,
                ]);
            }else if ($request->status == 2) {
                $pengadaan = Pengadaan::find($id)->update(['status' => 3]);
                RiwayatPengadaan::create([
                    'id_pengadaan' => $request->id,
                    'tahapan_pengadaan' => '2', 
                    'keterangan' => 'Persetujuan menyetujui oleh '.Auth::user()->nama,
                ]);
            }else if ($request->status == 7) {
                $pengadaan = Pengadaan::find($id)->update(['status' => 7]);
                RiwayatPengadaan::create([
                    'id_pengadaan' => $request->id,
                    'tahapan_pengadaan' => 13, 
                    'keterangan' => $request->keterangan,
                ]);
            }else if ($request->status == 8) {
                $pengadaan = Pengadaan::find($id)->update(['status' => 8]);
                RiwayatPengadaan::create([
                    'id_pengadaan' => $request->id,
                    'tahapan_pengadaan' => 14, 
                    'keterangan' => $request->keterangan,
                ]);
            }
        }
        // return response()->json(['data' => $pengadaan, 'id' => $id]);
        return redirect()->back();
    }

    public function pengadaanDetail($id){
        try {
            $detailPengadaan = Pengadaan::findOrFail($id);
            $data = $this->ambilPengadaan($detailPengadaan);
            // return response()->json($data);
            return view('pengadaan.detail', compact('data'));
        }catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function pengadaanDetailApi($id){
        try {
            $detailPengadaan = Pengadaan::findOrFail($id);
            $data = $this->ambilPengadaan($detailPengadaan);
            return response()->json($data, 200);
        }catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }

    public function pengadaanFilter(Request $request){
        $role = Auth::user()->id_role;
        $data = [];
        if($role >= 1 && $role <=3){
            $pengadaan = Pengadaan::whereDate('created_at', '>=', $request->start)
                                    ->whereDate('created_at', '<=', $request->end)
                                    ->get();
            $data = $this->ambilPengadaans($pengadaan);
        }
        return view('pengadaan.index', compact('data'));
    }
    
    public function pengadaanCreate(Request $request){
        $validator = Validator::make($request->all(),[
            'judul_pekerjaan' => 'required',
            'id_kategori' => 'required',
            'jenis_anggaran' => 'required',
            'jenis_pengadaan' => 'required',
            'tahun_anggaran' => 'required',
            'id_fpp' => 'required',
            'id_metode_pengadaan' => 'required',
            'kategori_csms' => 'required',
            'id_pic' => 'required',
            // 'id_mengajukan' => 'required',
            // 'id_mengetahui' => 'required',
            // 'id_menyetujui' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }else{
            $idKategori = '';
            $kategori = Kategori::where('nama', $request->id_kategori)->first();
            if ($kategori) {
                $idKategori = $kategori->id;
            }else{
                $createKategori = Kategori::create([
                    'nama' => $request->id_kategori
                ]);
                $idKategori = $createKategori->id;
            }
            $idfpp = '';
            $fpp = FungsiPemintaPengadaan::where('nama', $request->id_fpp)->first();
            if ($fpp) {
                $idfpp = $fpp->id;
            }else{
                $creatfpp = FungsiPemintaPengadaan::create([
                    'nama' => $request->id_fpp
                ]);
                $idfpp = $creatfpp->id;
            }
    
            $idPengadaan = '';
            $metPengadaan = MetodePengadaan::where('nama', $request->id_metode_pengadaan)->first();
            if ($metPengadaan) {
                $idPengadaan = $metPengadaan->id;
            }else{
                $creatMetPengadaan = MetodePengadaan::create([
                    'nama' => $request->id_metode_pengadaan
                ]);
                $idPengadaan = $creatMetPengadaan->id;
            }

            $picMultiple = [];
            $pic;
            for ($i=0; $i < count($request->id_pic) ; $i++) {
                $picMultiple[$i] = $request->id_pic[$i];
                $pic = implode("||",$picMultiple);
            }
            
            $subMultiple = [];
            $sub;
            for ($i=0; $i < count($request->sub_bidang) ; $i++) {
                $subMultiple[$i] = $request->sub_bidang[$i];
                $sub = implode("||",$subMultiple);
            }
    
            $pengadaan = Pengadaan::create([
                'judul' => $request->judul_pekerjaan,
                'id_kategori' =>$idKategori,
                'tender' => $request->tander,
                'jenis_anggaran' => $request->jenis_anggaran,
                'jenis_pengadaan' => $request->jenis_pengadaan,
                'tahun_anggaran' => $request->tahun_anggaran,
                'id_fpp' => $idfpp,
                'no_pr' => $request->no_pr,
                'sub_bidang' => $sub,
                'id_metode_pengadaan' => $idPengadaan,
                'kategori_csms' => $request->kategori_csms,
                'id_pengadaan_pic' => $pic,
                'status' => 1,
            ]);
            $riwayatPeng = RiwayatPengadaan::Create([
                'id_pengadaan' => $pengadaan->id, 
                'status_pengadaan' => 4,
                'tahapan_pengadaan' => 1,
                'pemeroses_id' => Auth::user()->id,
                'status' => 0,
            ]);
            // $evaluasi = EvaluasiTahapan::create([
            //     'riwayat_pengadaan_id' => $riwayatPeng->id,
            //     'plan_start' => Carbon::now(),
            // ]);
            return redirect()->back()->with('success', 'Pengadaan Berhasil Di Tambah');
        }

    }
    public function pengadaanEdit(Request $request, $id){
        $validator = Validator::make($request->all(),[
            'judul_pekerjaan' => 'required',
            'id_kategori' => 'required',
            'jenis_anggaran' => 'required',
            'jenis_pengadaan' => 'required',
            'tahun_anggaran' => 'required',
            'id_fpp' => 'required',
            'id_metode_pengadaan' => 'required',
            'kategori_csms' => 'required',
            'id_pic' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }else{
            $idKategori = '';
            $kategori = Kategori::where('nama', $request->id_kategori)->first();
            if ($kategori) {
                $idKategori = $kategori->id;
            }else{
                $createKategori = Kategori::create([
                    'nama' => $request->id_kategori
                ]);
                $idKategori = $createKategori->id;
            }
            $idfpp = '';
            $fpp = FungsiPemintaPengadaan::where('nama', $request->id_fpp)->first();
            if ($fpp) {
                $idfpp = $fpp->id;
            }else{
                $creatfpp = FungsiPemintaPengadaan::create([
                    'nama' => $request->id_fpp
                ]);
                $idfpp = $creatfpp->id;
            }
    
            $idPengadaan = '';
            $metPengadaan = MetodePengadaan::where('nama', $request->id_metode_pengadaan)->first();
            if ($metPengadaan) {
                $idPengadaan = $metPengadaan->id;
            }else{
                $creatMetPengadaan = MetodePengadaan::create([
                    'nama' => $request->id_metode_pengadaan
                ]);
                $idPengadaan = $creatMetPengadaan->id;
            }

            $picMultiple = [];
            $pic;
            for ($i=0; $i < count($request->id_pic) ; $i++) {
                $picMultiple[$i] = $request->id_pic[$i];
                $pic = implode("||",$picMultiple);
            }
            
            $subMultiple = [];
            $sub;
            for ($i=0; $i < count($request->sub_bidang) ; $i++) {
                $subMultiple[$i] = $request->sub_bidang[$i];
                $sub = implode("||",$subMultiple);
            }
    
            $pengadaan = Pengadaan::find($id);
            $pengadaan->update([
                'judul' => $request->judul_pekerjaan,
                'id_kategori' =>$idKategori,
                'tender' => $request->tander,
                'jenis_anggaran' => $request->jenis_anggaran,
                'jenis_pengadaan' => $request->jenis_pengadaan,
                'tahun_anggaran' => $request->tahun_anggaran,
                'id_fpp' => $idfpp,
                'no_pr' => $request->no_pr,
                'sub_bidang' => $sub,
                'id_metode_pengadaan' => $idPengadaan,
                'kategori_csms' => $request->kategori_csms,
                'id_pengadaan_pic' => $pic,
                'status' => 1,
            ]);
            return redirect()->back()->with('success', 'Pengadaan Berhasil Di Ubah');
        }

    }
    public function pengadaanMengetahui($id,Request $request)
    {
        try {
            $pengadaan = Pengadaan::find($id);
            $pengadaan->update([
                'status' => 2,
                'ttd_mengetahui' => 'approved.png',
            ]);
            return redirect()->back()->with('success', 'Pengadaan Berhasil Di Ketahui');
        }catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function pengadaanMenyetujui($id,Request $request)
    {
        try {
            $pengadaan = Pengadaan::find($id);
            $pengadaan->update([
                'status' => 3,
                'ttd_menyetujui' => 'approved.png',
            ]);
            return redirect()->back()->with('success', 'Pengadaan Berhasil Di Setujui');
        }catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function pengadaanBatalDokumen($id,Request $request)
    {
        try {
            $pengadaan = Pengadaan::find($id);
            $pengadaan->update([
                'status' => 10,
                'ttd_mengajukan' => 'cancelled.png',
                'ttd_mengetahui' => 'cancelled.png',
                'ttd_menyetujui' => 'cancelled.png',
                'keterangan_reject' => $request->keterangan,
            ]);
            return redirect()->back()->with('success', 'Pengadaan Berhasil Di Batal');
        }catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function pengadaanRejectDokumen($id,Request $request)
    {
        try {
            $pengadaan = Pengadaan::find($id);
            $pengadaan->update([
                'status' => 9,
                'ttd_mengajukan' => 'rejected.png',
                'ttd_mengetahui' => 'rejected.png',
                'ttd_menyetujui' => 'rejected.png',
                'keterangan_reject' => $request->keterangan,
            ]);
            return redirect()->back()->with('success', 'Pengadaan Berhasil Di Reject');
        }catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    
    public function pengadaanUpdateTahapan($id,Request $request)
    {
        // dd($request->all());
        try {
            $tahapanLanjutan = '';
            if ($request->tahapan >=1 && $request->tahapan <= 11){
                $tahapanLanjutan = $request->tahapan + 1;
            }
            
            $tombol = $request->tombol;
            $pengadaan = Pengadaan::find($id);
            if ($tombol == 0) {
                $planStart = 0;
                $tahapanCretae = Tahapan::find($request->tahapan);
                if ($request->tahapan == 2) {
                    $pengadaan->update([
                        'plan_pengumuman_tender'=> $request->pengumuman_tender,
                        'pengumuman_tender'=> Carbon::today(),
                    ]);
                    $planStart = $request->pengumuman_tender;
                }elseif ($request->tahapan == 3) {
                    $pengadaan->update([
                        'plan_prebid_meeting'=> $request->penyelesain_proses_pengadaan,
                    ]);
                    $planStart = $pengadaan->plan_prebid_meeting;
                }elseif ($request->tahapan == 4) {
                    $pengadaan->update([
                        'plan_prebid_lapangan' => $request->plan_prebid_lapangan,
                        'pre_tender_no_nr' => $request->pre_tender_no_nr,
                        'pre_tender_tanggal' => $request->pre_tender_tanggal,
                        'ba_aanwijzing_no' => $request->ba_aanwijzing_no,
                        'ba_aanwijzing_tanggal' => $request->ba_aanwijzing_tanggal,
                        'form_a2_tkdn' => $request->form_a2_tkdn,
                        'form_a3_nilai_tkdn_barang' => $request->form_a3_nilai_tkdn_barang,
                        'tkdn_barang' => $request->tkdn_barang,
                        'form_a4_nilai_tkdn_jasa' => $request->form_a4_nilai_tkdn_jasa,
                        'tkdn_jasa' => $request->tkdn_jasa,
                        'form_a5_nilai_tkdn_jasa_barang' => $request->form_a5_nilai_tkdn_jasa_barang,
                        'tkdn_jasa_barang' => $request->tkdn_jasa_barang,
                    ]);
                    $planStart = $request->plan_prebid_lapangan;
                }elseif ($request->tahapan == 5) {
                    $pengadaan->update([
                        'plan_pemasukan_penawaran' => $request->pemasukan_penawaran,
                        'ba_aanwijzing_lapangan_no' => $request->ba_aanwijzing_lapangan_no,
                        'ba_aanwijzing_lapangan_tanggal' => $request->ba_aanwijzing_lapangan_tanggal,
                    ]);
                    $planStart = $request->pemasukan_penawaran;
                }elseif ($request->tahapan == 6) {
                    $pengadaan->update([
                        'plan_evaluasi_sampul' => $request->evaluasi_sampul,
                        'nilai_oe' => $request->nilai_oe,
                        'harga_penawaran' => $request->harga_penawaran,
                    ]);
                    $planStart = $request->evaluasi_sampul;
                }elseif ($request->tahapan == 7) {
                    $pengadaan->update([
                        'plan_negosiasi' => $request->negosiasi,
                    ]);
                    $planStart = $request->negosiasi;
                }elseif ($request->tahapan == 8) {
                    $pengadaan->update([
                        'plan_lhp_plan' => $request->lhp_plan,
                        'ba_negosiasi_no' => $request->ba_negosiasi_no,
                        'ba_negosiasi_tanggal' => $request->ba_negosiasi_tanggal,
                        'harga_setelah_negosiasi' => $request->harga_setelah_negosiasi,
                    ]);
                    $planStart = $pengadaan->plan_lhp_plan;
                }elseif ($request->tahapan == 9) {
                    $pengadaan->update([
                        'plan_evaluasi_FPP' => $request->evaluasi_FPP,
                        'lhp_no' => $request->lhp_no,
                        'lhp_tgl' => $request->lhp_tgl,
                    ]);
                    $planStart = $request->evaluasi_FPP;
                }elseif ($request->tahapan == 10) {
                    $pengadaan->update([
                        'plan_penunjukan_pemenang' => $request->penunjukan_pemenang,
                        'nama_penyedia' => $request->nama_penyedia,
                        'penunjukan_pemenang'=> Carbon::today(),
                    ]);
                    $planStart = $request->penunjukan_pemenang;
                }elseif ($request->tahapan == 11) {
                    $pengadaan->update([
                        // 'plan_po_plan' => $request->po_plan,
                        // 'nama_penyedia' => $request->nama_penyedia,
                        // 'penunjukan_pemenang'=> Carbon::today(),
                    // ]);
                    // $planStart = $request->po_plan;
                // }elseif ($request->tahapan == 12) {
                    // $pengadaan->update([
                        // 'plan_kontrak_plan' => $request->kontrak_plan,
                        'po_no' => $request->po_no,
                        'po_tgl' => $request->po_tgl,
                    // ]);
                    // }elseif ($request->tahapan == 13) {
                        // $pengadaan->update([
                            'kontrak_no' => $request->kontrak_no,
                            'kontrak_tgl' => $request->kontrak_tgl,
                            'kontrak_jenis' => $request->kontrak_jenis,
                            'kontrak_masa' => $request->kontrak_masa,
                        ]);
                        $planStart = $request->kontrak_plan;
                }

                $pengadaan->update([
                    'status'=> $request->status_pengadaan,
                ]);

                $riwayatPeng = RiwayatPengadaan::find($request->id_tahapan);
                if ($riwayatPeng) {
                    $riwayatPeng->update([
                        'status_pengadaan' => $request->status_pengadaan,
                        'keterangan' => $request->keterangan,
                        'pemeroses_id'=> Auth::user()->id,
                        'status'=> 0,
                    ]);
                    $evaluasi = EvaluasiTahapan::where('riwayat_pengadaan_id', $riwayatPeng->id);
                    if ($evaluasi) {
                        $evaluasi->update([
                            'plan_hk' => ($planStart == '' || $planStart == null) ? 0 : $planStart,
                        ]);
                    }
                }

                $LogTahapan = LogTahapan::create([
                    'riwayat_pengadaan_id' => $request->id_tahapan,
                    'title' => 'Update Tahapan - '. $this->tahapanPengadaan($request->tahapan) .' - '.$this->statusPengadaan($request->status_pengadaan),
                    'user_id' => Auth::user()->id,
                    'tanggal_description' => $request->tanggal_description,
                    'keterangan' => $request->keterangan,
                ]);

                return redirect()->back()->with('success', 'Pengadaan Berhasil Disimpan Ke Tahap '.$this->tahapanPengadaan($request->tahapan));
            }elseif($tombol == 1){
                $planStart = 0;
                $tahapanCretae = Tahapan::find($request->tahapan);
                if ($request->tahapan == 2) {
                    $pengadaan->update([
                        'plan_pengumuman_tender'=> $request->pengumuman_tender,
                        'pengumuman_tender'=> Carbon::today(),
                    ]);
                    $planStart = $request->pengumuman_tender;
                }elseif ($request->tahapan == 3) {
                    $pengadaan->update([
                        'plan_prebid_meeting'=> $request->penyelesain_proses_pengadaan,
                        // 'dp3_diterima_lengkap'=> Carbon::today(),
                    ]);
                    $planStart = $pengadaan->plan_prebid_meeting;
                }elseif ($request->tahapan == 4) {
                    $pengadaan->update([
                        'plan_prebid_lapangan' => $request->plan_prebid_lapangan,
                        'pre_tender_no_nr' => $request->pre_tender_no_nr,
                        'pre_tender_tanggal' => $request->pre_tender_tanggal,
                        'ba_aanwijzing_no' => $request->ba_aanwijzing_no,
                        'ba_aanwijzing_tanggal' => $request->ba_aanwijzing_tanggal,
                        'form_a2_tkdn' => $request->form_a2_tkdn,
                        'form_a3_nilai_tkdn_barang' => $request->form_a3_nilai_tkdn_barang,
                        'tkdn_barang' => $request->tkdn_barang,
                        'form_a4_nilai_tkdn_jasa' => $request->form_a4_nilai_tkdn_jasa,
                        'tkdn_jasa' => $request->tkdn_jasa,
                        'form_a5_nilai_tkdn_jasa_barang' => $request->form_a5_nilai_tkdn_jasa_barang,
                        'tkdn_jasa_barang' => $request->tkdn_jasa_barang,
                    ]);
                    $planStart = $request->plan_prebid_lapangan;
                }elseif ($request->tahapan == 5) {
                    $pengadaan->update([
                        'plan_pemasukan_penawaran' => $request->pemasukan_penawaran,
                        'ba_aanwijzing_lapangan_no' => $request->ba_aanwijzing_lapangan_no,
                        'ba_aanwijzing_lapangan_tanggal' => $request->ba_aanwijzing_lapangan_tanggal,
                    ]);
                    $planStart = $request->pemasukan_penawaran;
                }elseif ($request->tahapan == 6) {
                    $pengadaan->update([
                        'plan_evaluasi_sampul' => $request->evaluasi_sampul,
                        'nilai_oe' => $request->nilai_oe,
                        'harga_penawaran' => $request->harga_penawaran,
                    ]);
                    $planStart = $request->evaluasi_sampul;
                }elseif ($request->tahapan == 7) {
                    $pengadaan->update([
                        'plan_negosiasi' => $request->negosiasi,
                    ]);
                    $planStart = $request->negosiasi;
                }elseif ($request->tahapan == 8) {
                    $pengadaan->update([
                        'plan_lhp_plan' => $request->lhp_plan,
                        'ba_negosiasi_no' => $request->ba_negosiasi_no,
                        'ba_negosiasi_tanggal' => $request->ba_negosiasi_tanggal,
                        'harga_setelah_negosiasi' => $request->harga_setelah_negosiasi,
                    ]);
                    $planStart = $pengadaan->plan_lhp_plan;
                }elseif ($request->tahapan == 9) {
                    $pengadaan->update([
                        'plan_evaluasi_FPP' => $request->evaluasi_FPP,
                        'lhp_no' => $request->lhp_no,
                        'lhp_tgl' => $request->lhp_tgl,
                    ]);
                    $planStart = $request->evaluasi_FPP;
                }elseif ($request->tahapan == 10) {
                    $pengadaan->update([
                        'plan_penunjukan_pemenang' => $request->penunjukan_pemenang,
                        'nama_penyedia' => $request->nama_penyedia,
                        'penunjukan_pemenang'=> Carbon::today(),
                    ]);
                    $planStart = $request->penunjukan_pemenang;
                }elseif ($request->tahapan == 11) {
                    $pengadaan->update([
                        // 'plan_po_plan' => $request->po_plan,
                        // 'nama_penyedia' => $request->nama_penyedia,
                        // 'penunjukan_pemenang'=> Carbon::today(),
                //     ]);
                //     $planStart = $request->po_plan;
                // }elseif ($request->tahapan == 12) {
                //     $pengadaan->update([
                        // 'plan_kontrak_plan' => $request->kontrak_plan,
                        'po_no' => $request->po_no,
                        'po_tgl' => $request->po_tgl,
                    // ]);
                    // $planStart = $request->kontrak_plan;
                // }elseif ($request->tahapan == 13) {
                //     $pengadaan->update([
                        'kontrak_no' => $request->kontrak_no,
                        'kontrak_tgl' => $request->kontrak_tgl,
                        'kontrak_jenis' => $request->kontrak_jenis,
                        'kontrak_masa' => $request->kontrak_masa,
                    ]);
                    $planStart = Carbon::today();
                }elseif ($request->tahapan == 1){
                    $planStart = 0;
                }

                $riwayatPeng = RiwayatPengadaan::find($request->id_tahapan);
                if ($riwayatPeng) {
                    $riwayatPeng->update([
                        'keterangan' => $request->keterangan,
                        'pemeroses_id'=> Auth::user()->id,
                        'status'=> 1,
                    ]);
                    //tahapan lama
                    $evaluasi = EvaluasiTahapan::where('riwayat_pengadaan_id', $riwayatPeng->id);
                    if ($evaluasi) {
                        $evaluasi->update([
                            // 'plan_end' => Carbon::today(),
                            'aktual_end' => Carbon::today(),
                        ]);
                    }
                }
                $tahapan = Tahapan::find($tahapanLanjutan);
                // dd($request->tahapan);
                $pengadaan->update([
                    'status'=> $tahapan->status_pengadaan,
                ]);

                $LogTahapan = LogTahapan::create([
                    'riwayat_pengadaan_id' => $request->id_tahapan,
                    'title' => 'Update Tahapan - '. $this->tahapanPengadaan($request->tahapan) .' - '.$this->statusPengadaan($request->status_pengadaan),
                    'user_id' => Auth::user()->id,
                    'tanggal_description' => $request->tanggal_description,
                    'keterangan' => $request->keterangan,
                ]);

                
                if ($tahapanLanjutan == 12) {
                    $riwayatLanjut = RiwayatPengadaan::Create([
                        'id_pengadaan' => $pengadaan->id, 
                        'status_pengadaan' => $tahapan->status_pengadaan,
                        'tahapan_pengadaan' => $tahapanLanjutan,
                        'pemeroses_id'=> Auth::user()->id,
                        'status'=> 1,
                    ]);
                }else{
                    $riwayatLanjut = RiwayatPengadaan::Create([
                        'id_pengadaan' => $pengadaan->id, 
                        'status_pengadaan' => $tahapan->status_pengadaan,
                        'tahapan_pengadaan' => $tahapanLanjutan,
                        // 'keterangan' => $request->keterangan,
                        'pemeroses_id'=> Auth::user()->id,
                        'status'=> 0,
                    ]);
                    //tahapan baru
                    $evaluasi = EvaluasiTahapan::create([
                        'riwayat_pengadaan_id' => $riwayatLanjut->id,
                        'plan_hk' => ($planStart == '' || $planStart == null) ? 0 : $planStart,
                        'aktual_start' => Carbon::today(),
                    ]);

                }
                
                return redirect()->back()->with('success', 'Pengadaan Lanjut Ke Tahap '.$this->tahapanPengadaan($tahapanLanjutan));
            }
        }catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function pengadaanEditTahapan($id,Request $request)
    {
        try {
            $pengadaan = Pengadaan::find($id);
            $planStart = '';
            if ($request->tahapan_id == 2) {
                $pengadaan->update([
                    'plan_pengumuman_tender'=> $request->pengumuman_tender,
                ]);
                $planStart = $request->pengumuman_tender;
            }elseif ($request->tahapan_id == 3) {
                $pengadaan->update([
                    'plan_prebid_meeting'=> $request->penyelesain_proses_pengadaan,
                ]);
                $planStart = $pengadaan->penyelesain_proses_pengadaan;
            }elseif ($request->tahapan_id == 4) {
                $pengadaan->update([
                    'plan_prebid_lapangan' => $request->plan_prebid_lapangan,
                    'pre_tender_no_nr' => $request->pre_tender_no_nr,
                    'pre_tender_tanggal' => $request->pre_tender_tanggal,
                    'ba_aanwijzing_no' => $request->ba_aanwijzing_no,
                    'ba_aanwijzing_tanggal' => $request->ba_aanwijzing_tanggal,
                    'form_a2_tkdn' => $request->form_a2_tkdn,
                    'form_a3_nilai_tkdn_barang' => $request->form_a3_nilai_tkdn_barang,
                    'tkdn_barang' => $request->tkdn_barang,
                    'form_a4_nilai_tkdn_jasa' => $request->form_a4_nilai_tkdn_jasa,
                    'tkdn_jasa' => $request->tkdn_jasa,
                    'form_a5_nilai_tkdn_jasa_barang' => $request->form_a5_nilai_tkdn_jasa_barang,
                    'tkdn_jasa_barang' => $request->tkdn_jasa_barang,
                ]);
                $planStart = $request->plan_prebid_lapangan;
            }elseif ($request->tahapan_id == 5) {
                $pengadaan->update([
                    'plan_pemasukan_penawaran' => $request->pemasukan_penawaran,
                    'ba_aanwijzing_lapangan_no' => $request->ba_aanwijzing_lapangan_no,
                    'ba_aanwijzing_lapangan_tanggal' => $request->ba_aanwijzing_lapangan_tanggal,
                ]);
                $planStart = $request->pemasukan_penawaran;
            }elseif ($request->tahapan_id == 6) {
                $pengadaan->update([
                    'plan_evaluasi_sampul' => $request->evaluasi_sampul,
                    'nilai_oe' => $request->nilai_oe,
                    'harga_penawaran' => $request->harga_penawaran,
                ]);
                $planStart = $request->evaluasi_sampul;
            }elseif ($request->tahapan_id == 7) {
                $pengadaan->update([
                    'plan_negosiasi' => $request->negosiasi,
                ]);
                $planStart = $request->negosiasi;
            }elseif ($request->tahapan_id == 8) {
                $pengadaan->update([
                    'plan_lhp_plan' => $request->lhp_plan,
                    'ba_negosiasi_no' => $request->ba_negosiasi_no,
                    'ba_negosiasi_tanggal' => $request->ba_negosiasi_tanggal,
                    'harga_setelah_negosiasi' => $request->harga_setelah_negosiasi,
                ]);
                $planStart = $request->plan_lhp_plan;
            }elseif ($request->tahapan_id == 9) {
                $pengadaan->update([
                    'plan_evaluasi_FPP' => $request->evaluasi_FPP,
                    'lhp_no' => $request->lhp_no,
                    'lhp_tgl' => $request->lhp_tgl,
                ]);
                $planStart = $request->evaluasi_FPP;
            }elseif ($request->tahapan_id == 10) {
                $pengadaan->update([
                    'plan_penunjukan_pemenang' => $request->penunjukan_pemenang,
                ]);
                $planStart = $request->penunjukan_pemenang;
            }elseif ($request->tahapan_id == 11) {
                $pengadaan->update([
                    // 'plan_po_plan' => $request->po_plan,
                    'nama_penyedia' => $request->nama_penyedia,
            //     ]);
            //     $planStart = $request->po_plan;
            // }elseif ($request->tahapan_id == 12) {
                // $pengadaan->update([
                    // 'plan_kontrak_plan' => $request->kontrak_plan,
                    'po_no' => $request->po_no,
                    'po_tgl' => $request->po_tgl,
            //     ]);
            // }elseif ($request->tahapan_id == 13) {
                //     $pengadaan->update([
                    'kontrak_no' => $request->kontrak_no,
                    'kontrak_tgl' => $request->kontrak_tgl,
                    'kontrak_jenis' => $request->kontrak_jenis,
                    'kontrak_masa' => $request->kontrak_masa,
                ]);
                $planStart = $request->kontrak_plan;
            }

            $riwayatPeng = RiwayatPengadaan::find($request->id_riwayat);
            if ($riwayatPeng) {
                $riwayatPeng->update([
                    'keterangan' => $request->keterangan,
                ]);
                $evaluasi = EvaluasiTahapan::where('riwayat_pengadaan_id', $riwayatPeng->id);
                if ($evaluasi) {
                    $evaluasi->update([
                        'plan_hk' => ($planStart == '' || $planStart == null) ? 0 : $planStart,
                    ]);
                }
                $log = LogTahapan::create([
                    'riwayat_pengadaan_id' => $riwayatPeng->id,
                    'title' => 'Edit Tahapan - '. $this->tahapanPengadaan($request->tahapan_id) .' - '.$this->statusPengadaan($riwayatPeng->status_pengadaan),
                    'user_id' => Auth::user()->id,
                    'keterangan' => $request->keterangan,
                ]);
            }
            return redirect()->back()->with('success', 'Pengadaan Berhasil Diedit Pada Tahap '.$this->tahapanPengadaan($request->tahapan_id));
        }catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function pengadaanDP3(Request $request, $id)
    {
        $pengadaan = Pengadaan::find($id);
        $pengadaan->update([
            'dp3_diterima_lengkap' => $request->plan_dp_3_belum_lengkap
        ]);
        return redirect()->back()->with('success', 'Pengadaan DP3 Berhasil Ditambahkan');
    }
    public function pengadaanEvaluasi(Request $request)
    {
        $id = $request->id;
        for ($i=0; $i < count($id); $i++) { 
            $evaluasi = EvaluasiTahapan::find($id[$i]);
            $evaluasi->update([
                'plan_hk' => $request->plan_hk[$i],
                'actual_hk' => $request->aktual_hk[$i],
                'alasan' => $request->keterangan[$i],
            ]);

            // $riwayatTahapan = RiwayatPengadaan::find($evaluasi->riwayat_pengadaan_id);
            // $pengadaan = Pengadaan::find($riwayatTahapan->id_pengadaan);
            // if ($riwayatTahapan->tahapan_pengadaan == 2) {
            //     $pengadaan->update([
            //         'plan_pengumuman_tender'=> $request->plan_hk[$i],
            //     ]);
            // }elseif ($riwayatTahapan->tahapan_pengadaan == 3) {
            //     $pengadaan->update([
            //         'plan_prebid_meeting'=> $request->plan_hk[$i],
            //     ]);
            // }elseif ($riwayatTahapan->tahapan_pengadaan == 4) {
            //     $pengadaan->update([
            //         'plan_prebid_lapangan' => $request->plan_hk[$i],
            //     ]);
            // }elseif ($riwayatTahapan->tahapan_pengadaan == 5) {
            //     $pengadaan->update([
            //         'plan_pemasukan_penawaran' => $request->plan_hk[$i],
            //     ]);
            // }elseif ($riwayatTahapan->tahapan_pengadaan == 6) {
            //     $pengadaan->update([
            //         'plan_evaluasi_sampul' => $request->plan_hk[$i],
            //     ]);
            // }elseif ($riwayatTahapan->tahapan_pengadaan == 7) {
            //     $pengadaan->update([
            //         'plan_negosiasi' => $request->plan_hk[$i],
            //     ]);
            // }elseif ($riwayatTahapan->tahapan_pengadaan == 8) {
            //     $pengadaan->update([
            //         'plan_lhp_plan' => $request->plan_hk[$i],
            //     ]);
            // }elseif ($riwayatTahapan->tahapan_pengadaan == 9) {
            //     $pengadaan->update([
            //         'plan_evaluasi_FPP' => $request->plan_hk[$i],
            //     ]);
            // }elseif ($riwayatTahapan->tahapan_pengadaan == 10) {
            //     $pengadaan->update([
            //         'plan_penunjukan_pemenang' => $request->plan_hk[$i],
            //     ]);
            // }
            
            $LogTahapan = LogTahapan::create([
                'riwayat_pengadaan_id' => $evaluasi->riwayat_pengadaan_id,
                'title' => 'Evaluasi Tahapan',
                'user_id' => Auth::user()->id,
                'keterangan' => 'Evaluasi Pekerjaan',
            ]);

        }
        return redirect()->back()->with('success', 'Evaluasi Pengadaan Berhasil Diedit');
    }
    public function pengadaanExportEvaluasi($id)
    {
        $data = ['id' => $id];
        return Excel::download(new ExportEvaluasiTahapan($data), 'evaluasi-tahapan.xlsx');
    }
    public function pengadaanDashbordGuest(){
        
        return view('guest.filter');
    }
    public function pengadaanDashbordFilter(Request $request){
        $pengadaan = '';
        $data = [];
        if ($request->id_fpp == 'all') {
            $pengadaan = Pengadaan::get();
        }else{
            $pengadaan = Pengadaan::where('id_fpp', $request->id_fpp)->get();
        }
        $data = $this->ambilPengadaans($pengadaan);
        return view('guest.index', compact('data'));
    }
}