<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\PengadaanTrait;
use App\Pengadaan;

class NotifikasiController extends Controller
{
    use PengadaanTrait;

    public function notifikasiPengadaan($id){
        $pengadaanMengetahui = Pengadaan::where('id_mengetahui', $id)->where('status', '1')->get();
        // dd($pengadaanMengetahui->all());
        $data['pengadaanMengetahui'] = $this->ambilPengadaans($pengadaanMengetahui);
        $pengadaanMenyetujui = Pengadaan::where('id_menyetujui', $id)->where('status', '2')->get();
        $data['pengadaanMenyetujui'] = $this->ambilPengadaans($pengadaanMenyetujui);
        return response()->json($data, 200);
    }
}