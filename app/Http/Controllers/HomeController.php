<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengadaan;
use App\RiwayatPengadaan;
use App\FungsiPemintaPengadaan;
use App\MetodePengadaan;
use App\Kategori;
use App\User;
use App\Mode;
use App\Tahapan;
use App\LogTahapan;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Traits\PengadaanTrait;

class HomeController extends Controller
{
    use PengadaanTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function homeIndex()
    {
        $role = Auth::user()->id_role;
        $data = [];
        if($role == 1 || $role == 3){
            $pengadaan = Pengadaan::get();
            $data = $this->ambilPengadaans($pengadaan);
        }else{
            $pengadaan = Pengadaan::where('id_pengadaan_pic','like', '%'.Auth::user()->id.'%')->get();
            $data = $this->ambilPengadaans($pengadaan);
        }
        // return response()->json($data, 200);
        return view('home.index', compact('data'));
    }
    public function aturThema(Request $request)
    {
        $mode = Mode::where('user_id', Auth::user()->id)->first();
        if ($mode) {
            $mode->update([
                'name' => $request->codeThema
            ]);
        }else{
            $mode = Mode::create([
                'user_id' => Auth::user()->id,
                'name' => $request->codeThema,
            ]);
        }
        return redirect()->back()->with('success', 'Berhasil Merubah Thema');
    }
}