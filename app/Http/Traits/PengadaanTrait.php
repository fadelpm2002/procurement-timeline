<?php

namespace App\Http\Traits;
use App\Pengadaan;
use App\RiwayatPengadaan;
use App\FungsiPemintaPengadaan;
use App\MetodePengadaan;
use App\Kategori;
use App\Tahapan;
use App\User;
use App\LogTahapan;
use App\EvaluasiTahapan;
use App\SubBidang;
use Carbon\Carbon;
use Auth;
use App\Models\Product;

trait PengadaanTrait {
    
    private function ambilPengadaans($pengadaans){
        $data = [];
        foreach ($pengadaans as $key => $item) {
            // if(Auth::user()->id_role == 1 || Auth::user()->id_role == 3){
                $data[$key] = $this->ambilPengadaan($item);
            // }elseif(Auth::user()->id_role == 2){
            //     $explodePelaksana = explode("||",$item->id_pengadaan_pic);
            //     if ($) {
            //         # code...
            //     }
            //     $data[$key] = $this->ambilPengadaan($item);
            // }
        } 
        return $data;
    }

    private function ambilPengadaan($item){
        $lastUpdate = [
            'id' => null,
            'id_tahapan' => null,
            'tahapan' => null,
            'keterangan' => null,
            'logProses' => 0,
            'selisihWaktu' => 0,
        ];
        $riwayat = RiwayatPengadaan::where('id_pengadaan', $item->id)
                                    ->orderBy('id', 'desc')
                                    ->first();
        if($riwayat){
            $tahapan = $this->tahapanPengadaan($riwayat->tahapan_pengadaan);
            $lastUpdateLog = LogTahapan::where('riwayat_pengadaan_id', $riwayat->id)
                            // ->select('tanggal_description')
                            ->orderBy('id', 'desc')
                            ->first();
            // dd($lastUpdateLog);

            if ($lastUpdateLog == null) {
                $lastUpdateDesc = '00-00-0000';
            }else{
                $lastUpdateDesc = Carbon::parse($lastUpdateLog->tanggal_description)->format('d F Y');
            }

            // $start = Carbon::parse($riwayat->created_at);
            $start = Carbon::parse($riwayat->updated_at);
            $end = Carbon::parse($riwayat->updated_at);
            $now = Carbon::now();
            $selisihWaktu = $start->diffInMinutes($now);
            
            $logProses = '';
            if ($selisihWaktu < 5) {
                $logProses = 'Last Update';
            }elseif($selisihWaktu > 5){
                $logProses = '';
            }

            $lastUpdate = [
                'id' => $riwayat->id,
                'id_tahapan' => $riwayat->tahapan_pengadaan,
                'tahapan' => $tahapan,
                'keterangan' => $riwayat->keterangan,
                'last_update' => $riwayat->updated_at == null ? "" :Carbon::parse($riwayat->updated_at)->format('d F Y'),
                'lastUpdateDesc' => $lastUpdateDesc,
                'logProses' => $logProses,
                'selisihWaktu' => $selisihWaktu,
            ];
        }
        
        $evaluasi = [];
        $log = [];
        $no = 0;
        $nos = 0;
        $riwayatTa = RiwayatPengadaan::where('id_pengadaan', $item->id)->get();
        foreach ($riwayatTa as $keys => $riwayatTahapan) {
            $tahapan = $this->tahapanPengadaan($riwayatTahapan->tahapan_pengadaan);
            $evaTahapan = EvaluasiTahapan::where('riwayat_pengadaan_id', $riwayatTahapan->id)->first();
            if ($evaTahapan) {
                $planStart = Carbon::parse($evaTahapan->plan_start);
                $planEnd = Carbon::parse($evaTahapan->plan_end);

                $aktualStart = Carbon::parse($evaTahapan->aktual_start);
                $aktualEnd = Carbon::parse($evaTahapan->aktual_end);

                $evaluasi[$no] = [
                    'id' => $evaTahapan->id,
                    'tahapan' => $tahapan,
                    'plan' => $planStart->diffInDays($planEnd),
                    'plan_hk' => $evaTahapan->plan_hk,
                    'aktual' => $aktualStart->diffInDays($aktualEnd),
                    'actual_hk' => $evaTahapan->actual_hk,
                    'alasan' => $evaTahapan->alasan,
                ];
                $no++;
            }
            $logTah = LogTahapan::where('riwayat_pengadaan_id', $riwayatTahapan->id)->get();
            foreach ($logTah as $key => $logTahapan) {
                $log[$nos] = [
                    'keterangan' => '('.Carbon::parse($logTahapan->created_at)->format('d/m/Y').') '.$logTahapan->title.' - '.$logTahapan->keterangan,
                ];
                $nos++;
            }

        }
        
        
        //initialisasi
        $id_pengadaan_pic = [];
        $pengadaan_pic = [];
        $pic = explode("||",$item->id_pengadaan_pic);
        for ($i=0; $i < count($pic); $i++) { 
            $pengadaanUsr = User::find($pic[$i]);
            $id_pengadaan_pic[]= $pic[$i];
            $pengadaan_pic[]= $pengadaanUsr->nama;
        }

        $subBidang = [];
        $subBidangId = [];
        $sub = explode("||",$item->sub_bidang);
        for ($i=0; $i < count($sub); $i++) { 
            $subbdg = $this->subBidang($sub[$i]);
            $subBidangId[]= $sub[$i];
            $subBidang[]= $subbdg;
        }

        $kategori = Kategori::find($item->id_kategori);
        $fpp = FungsiPemintaPengadaan::find($item->id_fpp);
        $metodePengadaan = MetodePengadaan::find($item->id_metode_pengadaan);
        // $namaMengajukan = User::find($item->id_mengajukan)->nama;
        // $namaMengetahui = User::find($item->id_mengetahui)->nama;
        // $namaMenyetujui = User::find($item->id_menyetujui)->nama;
        $data = [
            'id' => $item->id,
            'judul' => $item->judul,
            'id_kategori' => $item->id_kategori,
            'kategori' => $kategori->nama, 
            'kategori_csms' => $item->kategori_csms, 
            'tender' => $item->tender,
            'jenis_anggaran' => $item->jenis_anggaran,
            'tahun_anggaran' => $item->tahun_anggaran,
            'jenis_pengadaan' => $item->jenis_pengadaan,
            'dp3_diterima_lengkap' => $item->dp3_diterima_lengkap,
            'fpp_id' => $item->id_fpp,
            'fpp' => $fpp->nama,
            'plan_evaluasi_FPP' => $item->plan_evaluasi_FPP,
            'status' => $this->statusPengadaan($item->status),
            'id_status' => $item->status,
            'no_pr' => $item->no_pr,
            'sub_bidang_id' => $subBidangId,
            'sub_bidang' => $subBidang,
            'id_pengadaan_pic' => $id_pengadaan_pic,
            'pengadaan_pic' => $pengadaan_pic,
            'id_metode_pengadaan' => $item->id_metode_pengadaan,
            'metode_pengadaan' => $metodePengadaan->nama,
            // 'id_mengajukan' => $item->id_mengajukan,
            // 'nama_mengajukan' => $namaMengajukan,
            // 'ttd_mengajukan' => $item->ttd_mengajukan,
            // 'id_mengetahui' => $item->id_mengetahui,
            // 'nama_mengetahui' => $namaMengetahui,
            // 'ttd_mengetahui' => $item->ttd_mengetahui,
            // 'id_menyetujui' => $item->id_menyetujui,
            // 'nama_menyetujui' => $namaMenyetujui,
            // 'ttd_menyetujui' => $item->ttd_menyetujui,
            'keterangan_reject' => $item->keterangan_reject,
            'lastUpdate' => $lastUpdate,
            'plan_prebid_meeting' => $item->plan_prebid_meeting,
            'plan_dp_3_belum_lengkap' => $item->plan_dp_3_belum_lengkap,
            //penawaran
            'plan_pemasukan_penawaran' => $item->plan_pemasukan_penawaran, 
            'plan_evaluasi_sampul' => $item->plan_evaluasi_sampul,
            'plan_negosiasi' => $item->plan_negosiasi,
            'nilai_oe' => $item->nilai_oe,
            'harga_penawaran' => $item->harga_penawaran,
            'harga_setelah_negosiasi' => $item->harga_setelah_negosiasi,
            'nama_penyedia' => $item->nama_penyedia,
            'plan_pengumuman_tender' => $item->plan_pengumuman_tender,
            'plan_penunjukan_pemenang' => $item->plan_penunjukan_pemenang,
            'pengumuman_tender' => $item->pengumuman_tender,
            'penunjukan_pemenang' => $item->penunjukan_pemenang,
            'waktu_koreksi' => $item->waktu_koreksi,
            'penyelesain_proses_pengadaan' => $item->penyelesain_proses_pengadaan,
            //pre tender
            'plan_prebid_lapangan' => $item->plan_prebid_lapangan,
            'pre_tender_no_nr' => $item->pre_tender_no_nr,
            'pre_tender_tanggal' => $item->pre_tender_tanggal,
            //lhp
            'plan_lhp_plan' => $item->plan_lhp_plan,
            'lhp_no' => $item->lhp_no,
            'lhp_tgl' => $item->lhp_tgl,
            //po
            'plan_po_plan' => $item->plan_po_plan,
            'po_no' => $item->po_no,
            'po_tgl' => $item->po_tgl,
            //kontrak
            'plan_kontrak_plan' => $item->plan_kontrak_plan,
            'kontrak_no' => $item->kontrak_no,
            'kontrak_tgl' => $item->kontrak_tgl,
            'kontrak_jenis' => $item->kontrak_jenis,
            'kontrak_masa' => $item->kontrak_masa,
            //ba
            'ba_aanwijzing_no' => $item->ba_aanwijzing_no,
            'ba_aanwijzing_tanggal' => $item->ba_aanwijzing_tanggal,
            'ba_aanwijzing_lapangan_no' => $item->ba_aanwijzing_lapangan_no,
            'ba_aanwijzing_lapangan_tanggal' => $item->ba_aanwijzing_lapangan_tanggal,
            'ba_negosiasi_no' => $item->ba_negosiasi_no,
            'ba_negosiasi_tanggal' => $item->ba_negosiasi_tanggal,
            //tkdn
            'form_a2_tkdn' => $item->form_a2_tkdn,
            'form_a3_nilai_tkdn_barang' => $item->form_a3_nilai_tkdn_barang,
            'tkdn_barang' => $item->tkdn_barang,
            'form_a4_nilai_tkdn_jasa' => $item->form_a4_nilai_tkdn_jasa,
            'tkdn_jasa' => $item->tkdn_jasa,
            'form_a5_nilai_tkdn_jasa_barang' => $item->form_a5_nilai_tkdn_jasa_barang,
            'tkdn_jasa_barang' => $item->tkdn_jasa_barang,
            'evaluasi' => $evaluasi,
            'log' => $log,
            'created_at' => $item->created_at->translatedFormat('d F Y'),
        ];
        return $data;
    }

    private function statusPengadaan($id){
        $status = '';
        if ($id == 1){
            $status = 'Diajukan';
        }elseif($id == 2){
            $status = 'Diketahui';
        }elseif($id == 3){
            $status = 'Disetujui';
        }elseif($id == 4){
            $status = 'On Going';
        }elseif($id == 5){
            $status = 'On Hold';
        }elseif($id == 6){
            $status = 'Done';
        }elseif($id == 7){
            $status = 'Gagal';
        }elseif($id == 8){
            $status = 'Batal';
        }elseif($id == 9){
            $status = 'Reject Dokumen';
        }elseif($id == 10){
            $status = 'Batal Dokumen';
        }
        return $status;
    }

    private function subBidang($id){
        $subBidang = '';
        $sub = SubBidang::find($id);
        if ($sub) {
            $subBidang = $sub->code .' - '. $sub->description;
        }else{
            $subBidang = '';
        }
        
        return $subBidang;
    }
    private function tahapanPengadaan($id){
        $tahap = Tahapan::find($id);
        $tahapan = $tahap->nama;
        
        return $tahapan;
    }
    private function userName($id){
        $user = User::find($id);
        $nama = $user->nama;
        
        return $nama;
    }

}