<?php

namespace App\Exports;

use App\EvaluasiTahapan;
use App\RiwayatPengadaan;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\BeforeSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

use App\Http\Traits\PengadaanTrait;

class ExportEvaluasiTahapan implements FromView, ShouldAutoSize
{
    use PengadaanTrait;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($data)
    {
        $this->data = $data;
    }
    public function view(): View
    {
        $data = [];
        $no = 0;
        $riwayatTa = RiwayatPengadaan::where('id_pengadaan', $this->data['id'])->get();
        foreach ($riwayatTa as $keys => $riwayatTahapan) {
            $tahapan = $this->tahapanPengadaan($riwayatTahapan->tahapan_pengadaan);
            $evaTahapan = EvaluasiTahapan::where('riwayat_pengadaan_id', $riwayatTahapan->id)->first();
            if ($evaTahapan) {
                $planStart = Carbon::parse($evaTahapan->plan_start);
                $planEnd = Carbon::parse($evaTahapan->plan_end);

                $aktualStart = Carbon::parse($evaTahapan->aktual_start);
                $aktualEnd = Carbon::parse($evaTahapan->aktual_end);

                $data[$no] = [
                    'id' => $evaTahapan->id,
                    'tahapan' => $tahapan,
                    // 'plan' => $planStart->diffInDays($planEnd),
                    'plan' => $evaTahapan->plan_hk,
                    'aktual' => $aktualStart->diffInDays($aktualEnd),
                    'actual_hk' => $evaTahapan->actual_hk,
                    'alasan' => $evaTahapan->alasan,
                ];
                $no++;
            }
        }
        return view('pengadaan.export.EvaluasiTahapan',compact('data'));
    }
}
