<?php

namespace App\Exports;

use App\EvaluasiTahapan;
use App\RiwayatPengadaan;
use App\Pengadaan;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\BeforeSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

use App\Http\Traits\PengadaanTrait;


class ExportPengadaan implements FromView, ShouldAutoSize
{
    use PengadaanTrait;
    
    public function __construct($data)
    {
        $this->data = $data;
    }
    public function view(): View
    {
        $data = [];
        $no = 0;
        $pengadaan = Pengadaan::whereDate('created_at', '>=',$this->data['start'])
                                ->whereDate('created_at', '<=',$this->data['end'])
                                ->get();
        $data = $this->ambilPengadaans($pengadaan);
        // dd($data);
        return view('pengadaan.export.pengadaanExport',compact('data'));
    }
}