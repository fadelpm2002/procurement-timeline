<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FungsiPemintaPengadaan extends Model
{
    protected $fillable = [
        'nama',
    ];
}