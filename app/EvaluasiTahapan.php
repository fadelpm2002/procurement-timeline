<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluasiTahapan extends Model
{
    protected $fillable = [
        'riwayat_pengadaan_id',
        'plan_hk',
        'aktual_start',
        'aktual_end',
        'actual_hk',
        'alasan',
    ];
}
