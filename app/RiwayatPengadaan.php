<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatPengadaan extends Model
{
    protected $fillable = [
        'id_pengadaan', 
        'status_pengadaan',
        'tahapan_pengadaan',
        'pemeroses_id',
        'keterangan',
        'status',
    ];
}