<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubBidang extends Model
{
    protected $fillable = [
        'code',
        'description',
    ];
}
