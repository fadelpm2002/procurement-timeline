<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahapan extends Model
{
    protected $fillable = [
        'status_pengadaan',
        'nama',
    ];
}
