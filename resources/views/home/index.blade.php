@extends('layouts.layout2')
@section('title', 'Pengadaan - Procurement')
@section('main_wrapper')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-9 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Procurement</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header-right text-md-right col-md-3 col-3 d-md-block">
        <div class="form-group breadcrumb-right ">
        </div>
    </div>
</div>
@endsection
@section('main_content')
<section id="basic-datatable">
    <div class="row ">
        <div class="col-12 ">
            <div class="card ">
                <div class="card-header border-bottom mb-1" style="background-color: #27bd2f">
                    <h4 class="card-title text-white">List Pengadaan</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="myTable table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Judul Pekerjaan</th>
                                    <th>Kategori</th>
                                    <th>Pic Procurement</th>
                                    <th>Status </th>
                                    <th>Keterangan </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data as $key => $item)
                                <tr>
                                    <td>{{$key + 1}}</a></td>
                                    <td><span class="text-success font-weight-bolder">{{$item['judul']}} </span>
                                        <br>
                                        <span class="badge badge-light-warning">{{$item['created_at']}}</span>
                                        <br>
                                        <span class="text-info font-weight-bolder">{{$item['kategori']}}</span>
                                    </td>
                                    <td>
                                        <span class="text-success font-weight-bolder">{{$item['fpp']}}</span>
                                        <br>
                                        <span class="text-warning font-weight-bolder">{{$item['jenis_anggaran']}}</span>-
                                        <span class="text-muted font-weight-bolder">{{$item['tahun_anggaran']}}</span>
                                    </td>
                                    <td>
                                        @for ($i = 0; $i < count($item['pengadaan_pic']); $i++)
                                            <span class="text-success font-weight-bolder">{{$item['pengadaan_pic'][$i]}}</span><br>
                                        @endfor
                                    </td>
                                    <td>
                                        <span class="text-success font-weight-bolder">{{$item['lastUpdate']['tahapan']}}</span>
                                        <br>
                                        @if($item['lastUpdate']['tahapan'] )
                                            <sup class="text-muted ">{{$item['status']}}</sup>
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item['lastUpdate']['lastUpdateDesc'] != '00-00-0000')
                                        <span class="text-success">{{$item['lastUpdate']['lastUpdateDesc'] }}</span>
                                            
                                        @else
                                        <span class="text-success">{{$item['lastUpdate']['last_update'] }}</span>
                                            
                                        @endif
                                        <br>
                                        <span class="text-muted text-justify">{{$item['lastUpdate']['keterangan'] }}</span>

                                    </td>
                                </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.myTable').DataTable({
        "order": [
            [0, "desc"]
        ]
    });
});
</script>
@endsection