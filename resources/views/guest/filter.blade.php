@extends('layouts.app')
@section('title', 'Guest Procurement')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="auth-wrapper auth-v1 px-2">
                <div class="auth-inner py-2">
                    <!-- Forgot Password v1 -->
                    <div class="card mb-0">
                        <div class="card-body">
                            <a href="javascript:void(0);" class="brand-logo">
                            </a>

                            <h4 class="card-title mb-1 text-center text-primary font-weight-bolder">Procurement Timeline</h4>
                            <p class="card-text mb-2 text-center">Silahkan Pilih <span class="text-primary font-weight-bolder">FPP</span> anda</p>

                            <form class="" action="{{ route('pengadaan-dashboard-filter') }}" enctype="multipart/form-data" method="GET">
                                @csrf
                                <div class="form-group">
                                    <select class="pilihan form-control" name="id_fpp" id="fpp"aria-placeholder="silahkan pilih FPP" required>
                                        <option value=""></option>
                                        <option value="all">All</option>
                                        @php
                                        $fpp = App\FungsiPemintaPengadaan::get();
                                        @endphp
                                        @foreach ($fpp as $item)
                                        <option value="{{$item->id}}">{{$item->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button class="btn btn-primary btn-block" tabindex="2">Cari</button>
                            </form>

                            <p class="text-center mt-2">
                                <a href="{{ route('login') }}"> <i data-feather="chevron-left"></i> Kembali Ke
                                    Halaman login
                                </a>
                            </p>
                        </div>
                    </div>
                    <!-- /Forgot Password v1 -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- END: Content-->
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
    $('.pilihan').select2();
</script>

@endsection