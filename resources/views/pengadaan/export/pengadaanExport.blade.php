
          <table class="table align-items-center table-flush table-hover" id="dataTableHover">
            <thead>
              <tr>
                  <th rowspan="2">No</th>
                  <th rowspan="2">judul</th>
                  <th rowspan="2">kategori</th>
                  <th rowspan="2">tender</th>
                  <th>Priode</th>
                  <th rowspan="2">jenis_anggaran</th>
                  <th rowspan="2">jenis_pengadaan</th>
                  <th rowspan="2">tahun_anggaran</th>
                  <th rowspan="2">fpp</th>
                  <th rowspan="2">no_pr</th>
                  <th rowspan="2">metode_pengadaan</th>
                  <th rowspan="2">kategori_csms</th>
                  <th rowspan="2">nilai_oe</th>
                  <th rowspan="2">harga_penawaran</th>
                  <th rowspan="2">harga_setelah_negosiasi</th>
                  <th rowspan="2">nilai_cost_saving</th>
                  <th style="text-align: center;" colspan="7">TKDN (Base on Dok. Pengadaan)</th>
                  <th style="text-align: center;" colspan="2">Pre Tender</th>
                  <th style="text-align: center;" colspan="2">BA Aanwijzing</th>
                  <th style="text-align: center;" colspan="2">BA Aanwijzing Lapangan</th>
                  <th style="text-align: center;" colspan="2">BA Negosiasi</th>
                  <th style="text-align: center;" colspan="2">PO</th>
                  <th style="text-align: center;" colspan="4">Kontrak</th>
                  <th style="text-align: center;" colspan="2">LHP</th>
                  <th rowspan="2">Penyedia Barang/Jasa</th>
                  <th style="text-align: center;" colspan="2">Tanggal</th>
                  {{-- <th rowspan="2">Waktu Penyelesaian Proses Pengadaan (Hari Kerja)</th> --}}
                  <th rowspan="2">PIC Procurement</th>
                  <th rowspan="2">status</th>
                  <th rowspan="2">tahapan</th>
                  <th rowspan="2">alasan</th>
              </tr>
              <tr>
                  <th>DP3 Diterima Lengkap</th>
                  <th>form A2<br>TKDN %</th>
                  <th>form A3 Nilai <br>TKDN Barang Rp</th>
                  <th>TKDN Barang %</th>
                  <th>form A4 Nilai <br>TKDN Jasa Rp</th>
                  <th>TKDN Jasa</th>
                  <th>form A5 Nilai <br>TKDN Jasa Barang Rp</th>
                  <th>TKDN Jasa Barang</th>
                  <th>No.NR</th>
                  <th style="text-align: center;">Tgl</th>
                  <th style="text-align: center;">No</th>
                  <th style="text-align: center;">Tgl</th>
                  <th style="text-align: center;">No</th>
                  <th style="text-align: center;">Tgl</th>
                  <th style="text-align: center;">No</th>
                  <th style="text-align: center;">Tgl</th>
                  <th style="text-align: center;">No</th>
                  <th style="text-align: center;">Tgl</th>
                  <th style="text-align: center;">No</th>
                  <th style="text-align: center;">Tgl</th>
                  <th style="text-align: center;">Jenis</th>
                  <th style="text-align: center;">Masa Kontra</th>
                  <th style="text-align: center;">No</th>
                  <th style="text-align: center;">Tgl</th>
                  <th>Pengumuman Tender</th>
                  <th>Penunjukan Pemenang</th>
              </tr>
          </thead>
           <tbody>
            @for ($i = 0; $i < count($data); $i++)
            @php
                $nilai = 0;
                $nilaiEO = $data[$i]['nilai_oe'] == null ? 0 : $data[$i]['nilai_oe'];
                $harga_penawaran = $data[$i]['harga_penawaran'] == null ? 0 : $data[$i]['harga_penawaran'];
                $harga_setelah_negosiasi = $data[$i]['harga_setelah_negosiasi'] == null ? 0 : $data[$i]['harga_setelah_negosiasi'];

                if($nilaiEO-$harga_setelah_negosiasi > 0){
                  $nilai = ($nilaiEO-$harga_setelah_negosiasi)/$nilaiEO;
                }
            @endphp
              <tr>
                <td>{{$i + 1}}</td>
                <td>{{$data[$i]['judul']}}</td>
                <td>{{$data[$i]['kategori']}}</td>
                <td>{{$data[$i]['tender']}}</td>
                <td>{{$data[$i]['dp3_diterima_lengkap']}}</td>
                <td>{{$data[$i]['jenis_anggaran']}}</td>
                <td>{{$data[$i]['jenis_pengadaan']}}</td>
                <td>{{$data[$i]['tahun_anggaran']}}</td>
                <td>{{$data[$i]['fpp']}}</td>
                <td>{{$data[$i]['no_pr']}}</td>
                <td>{{$data[$i]['metode_pengadaan']}}</td>
                <td>{{$data[$i]['kategori_csms']}}</td>
                <td>{{$data[$i]['nilai_oe']}}</td>
                <td>{{$data[$i]['harga_penawaran']}}</td>
                <td>{{$data[$i]['harga_setelah_negosiasi']}}</td>
                <td>{{round($nilai*100)}}%</td>
                <td>{{$data[$i]['form_a2_tkdn']}}</td>
                <td>{{$data[$i]['form_a3_nilai_tkdn_barang']}}</td>
                <td>{{$data[$i]['tkdn_barang']}}</td>
                <td>{{$data[$i]['form_a4_nilai_tkdn_jasa']}}</td>
                <td>{{$data[$i]['tkdn_jasa']}}</td>
                <td>{{$data[$i]['form_a5_nilai_tkdn_jasa_barang']}}</td>
                <td>{{$data[$i]['tkdn_jasa_barang']}}</td>
                <td>{{$data[$i]['pre_tender_no_nr']}}</td>
                <td>{{$data[$i]['pre_tender_tanggal'] == null ? " " : Carbon\Carbon::parse($data[$i]['pre_tender_tanggal'])->format('d-m-Y')}}</td>
                <td>{{$data[$i]['ba_aanwijzing_no']}}</td>
                <td>{{$data[$i]['ba_aanwijzing_tanggal'] == null ? " " : Carbon\Carbon::parse($data[$i]['ba_aanwijzing_tanggal'])->format('d-m-Y')}}</td>
                <td>{{$data[$i]['ba_aanwijzing_lapangan_no']}}</td>
                <td>{{$data[$i]['ba_aanwijzing_lapangan_tanggal'] == null ? "" : Carbon\Carbon::parse($data[$i]['ba_aanwijzing_lapangan_tanggal'])->format('d-m-Y')}}</td>
                <td>{{$data[$i]['ba_negosiasi_no']}}</td>
                <td>{{$data[$i]['ba_negosiasi_tanggal'] == null ? " " : Carbon\Carbon::parse($data[$i]['ba_negosiasi_tanggal'])->format('d-m-Y')}}</td>
                <td>{{$data[$i]['po_no']}}</td>
                <td>{{$data[$i]['po_tgl'] == null ? " " : Carbon\Carbon::parse($data[$i]['po_tgl'])->format('d-m-Y')}}</td>
                <td>{{$data[$i]['kontrak_no']}}</td>
                <td>{{$data[$i]['kontrak_tgl'] == null ? " " : Carbon\Carbon::parse($data[$i]['kontrak_tgl'])->format('d-m-Y')}}</td>
                <td>{{$data[$i]['kontrak_jenis']}}</td>
                <td>{{$data[$i]['kontrak_masa'] == null ? " " : Carbon\Carbon::parse($data[$i]['kontrak_masa'])->format('d-m-Y')}}</td>
                <td>{{$data[$i]['lhp_no']}}</td>
                <td>{{$data[$i]['lhp_tgl'] == null ? " " : Carbon\Carbon::parse($data[$i]['lhp_tgl'])->format('d-m-Y')}}</td>
                <td>{{$data[$i]['nama_penyedia']}}</td>
                <td>{{$data[$i]['pengumuman_tender'] == null ? " " : Carbon\Carbon::parse($data[$i]['pengumuman_tender'])->format('d-m-Y')}}</td>
                <td>{{$data[$i]['penunjukan_pemenang'] == null ? " " : Carbon\Carbon::parse($data[$i]['penunjukan_pemenang'])->format('d-m-Y')}}</td>
                {{-- <td>{{$data[$i]['penyelesain_proses_pengadaan']}}</td> --}}
                <td>
                  @for ($x = 0; $x < count($data[$i]['pengadaan_pic']); $x++)
                      @php
                          $no = $x + 1;
                      @endphp
                      @if (count($data[$i]['pengadaan_pic']) == $no)
                          {{$data[$i]['pengadaan_pic'][$x]}}
                      @else
                          {{$data[$i]['pengadaan_pic'][$x]}},
                      @endif
                  @endfor
                </td>
                <td>{{$data[$i]['status']}}</td>
                <td>{{$data[$i]['lastUpdate']['tahapan']}}</td>
                <td>
                  @for ($x = 0; $x < count($data[$i]['log']); $x++)
                      {{$data[$i]['log'][$x]['keterangan']}}
                      <br>
                  @endfor
                </td>
              </tr>
            @endfor
          </tbody>
          </table>
