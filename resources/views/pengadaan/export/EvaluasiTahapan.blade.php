
          <table class="table align-items-center table-flush table-hover" id="dataTableHover">
            <thead>
              <tr>
                  <th>No</th>
                  <th>Tahapan</th>
                  <th>Plan (hari Kerja)</th>
                  <th>Actual (hari Kerja)</th>
                  <th>Alasan</th>
              </tr>
          </thead>
           <tbody>
            @php
                $totalPlan = 0;
                $totalAktual = 0;
            @endphp
            @for ($i = 0; $i < count($data); $i++)
                @php
                    $totalPlan += $data[$i]['plan'];
                    $totalAktual += $data[$i]['actual_hk'] == null ? $data[$i]['aktual'] : $data[$i]['actual_hk'];
                @endphp
              <tr>
                <td>{{$i + 1}}</td>
                <td>{{$data[$i]['tahapan']}}</td>
                <td>{{$data[$i]['plan']}}</td>
                <td>{{$data[$i]['actual_hk'] == null ? $data[$i]['aktual'] : $data[$i]['actual_hk']}}</td>
                <td>{{$data[$i]['alasan']}}</td>
              </tr>
            @endfor
              <tr>
                <td colspan="2" class="text-center">Total</td>
                <td>{{$totalPlan}}</td>
                <td>{{$totalAktual}}</td>
                <td></td>
              </tr>
          </tbody>
          </table>
