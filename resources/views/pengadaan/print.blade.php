<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Donasi Print - HIPE </title>
    <link rel="apple-touch-icon" href="{{asset('templateV2')}}/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('templateV2')}}/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/pages/app-invoice-print.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="invoice-print p-3">
                    <div class="d-flex justify-content-between flex-md-row flex-column pb-2">
                        <div>
                            <div class="d-flex mb-1">
                                <h3 class="text-primary font-weight-bold ml-1">Donasi</h3>
                            </div>
                            <p class="card-text mb-25">Perumahan, Jl. Borneo Paradiso No</p>
                            <p class="card-text mb-25">Kel, Sepinggan, Kec. Balikpapan Selatan, Kota Balikpapan</p>
                            <p class="card-text mb-0">Phone No. 0812-5237-7799</p>
                        </div>
                        <div class="mt-md-0 mt-2">
                            <h4 class="invoice-title">
                                <span class="invoice-number">{{$data['no_doc']}}</span>
                                <br>
                                @if ($data['status'] == 1)
                                    <span class="badge badge-light-info">Dibuat</span>
                                @elseif($data['status'] == 2)
                                    <span class="badge badge-light-warning">Diketahui</span>
                                @elseif($data['status'] == 3)
                                    <span class="badge badge-light-success">Tutup</span>
                                {{-- @elseif($data['status'] == 4)
                                    <span class="badge badge-light-success">Tutup</span>
                                @elseif($data['status'] == 5)
                                    <span class="badge badge-light-danger">Reject</span>
                                @elseif($data['status'] == 0)
                                    <span class="badge badge-light-secondary">Batal</span> --}}
                                @endif
                               
                            </h4>
                            <div class="invoice-date-wrapper">
                                <p class="invoice-date-title">Jenis:</p>
                                <p class="invoice-date">{{$data['jenis_donasi']}}</p>
                            </div>
                            <div class="invoice-date-wrapper">
                                <p class="invoice-date-title">Tanggal Dibuat:</p>
                                <p class="invoice-date">{{$data['tanggal']}}</p>
                            </div>
                        </div>
                    </div>
                @if ($data['nominal'] != 0)
                  <hr class="invoice-spacing" />
                    <div class="card-body invoice-padding pt-0">
                        <div class="row invoice-spacing">
                                <div class="col-xl-8 p-0">
                                    <h6 class="mb-2">Total Donasi </h6>
                                    @if ($data['konfirmasi_penerimaan'] == 1)
                                        <h6 class="mb-25">Donasi Telah Di Pindahkan Ke Rekening <br><span class="text-warning"> {{$data['no_rek']}}</span></h6>
                                        <h6 class="mb-25">Sebesar Rp. {{number_format($data['nominal'],0)}}</h6>
                                    @else
                                        <h6 class="mb-25">Rp. {{number_format($data['nominal'],0)}}</h6>
                                    @endif
                                    
                                </div>
                            {{-- <div class="col-xl-4 p-0 mt-xl-0 mt-2">
                                <h6 class="mb-2">No.Rekening:</h6>
                                <p class="card-text mb-25">{{$data['no_rekening']}}</p>
                                <p class="card-text mb-25">{{$data['bank']}} - {{$data['name_pengguna']}}</p>
                            </div> --}}
                        </div>
                    </div>
                  @endif
                  @if (count($data['items']) > 0)
                    <hr class="invoice-spacing" />
                    <div class="row ">
                    <div class="col-12 ">
                        <div class="card ">
                            <div class="card-header">
                            <h5 class="text-success invoice-logo">List Donasi</h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="myTable table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Barang</th>
                                                <th>Qty</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for ($i = 0; $i < count($data['items']); $i++)
                                            <tr>
                                                <td>{{$i + 1}}</td>
                                                <td>
                                                    <span class="card-text mb-0 font-weight-bolder">{{$data['items'][$i]['name_barang']}}</span>
                                                </td>
                                                <td>{{$data['items'][$i]['qty']}}</td>
                                                <td>
                                                </td>
                                            </tr>
                                            @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                @endif
                    
                </div>

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('templateV2')}}/app-assets/js/core/app-menu.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('templateV2')}}/app-assets/js/scripts/pages/app-invoice-print.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
</body>
<!-- END: Body-->

</html>