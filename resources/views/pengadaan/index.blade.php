@extends('layouts.layout2')
@section('title', 'Pengadaan - Procurement')
@section('main_wrapper')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-9 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Procurement</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Pengadaan</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header-right text-md-right col-md-3 col-3 d-md-block">
        <div class="form-group breadcrumb-right ">
            <div class="dropdown text-right">
                <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i data-feather="grid">
                    </i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <small class="dropdown-item text-muted">Navigation</small>
                    <a class="dropdown-item exportJR" href="/pengadaan-index-timeline"><i class="mr-1"
                        data-feather='package'></i><span class="align-middle">Index Pengadaan Timeline
                        </span>
                    </a>
                    @if(Auth::user()->id == 1 || Auth::user()->id == 2)
                    <small class="dropdown-item text-muted">Aksi</small>
                    <a class="dropdown-item exportJR" data-toggle="modal" data-target="#createPengadaan"><i class="mr-1"
                            data-feather='package'></i><span class="align-middle">Tambah
                            Pengadaan</span></a>
                    @endif
                    <a class="dropdown-item" data-toggle="modal" data-target="#exportPengadaan"><i class="mr-1"
                            data-feather='file-text'></i>
                        <span class="align-middle">Export Pengadaan</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('main_content')
<section id="basic-datatable">
    <div class="row ">
        <div class="col-12 ">
            <div class="card ">
                <div class="card-header border-bottom mb-1" style="background-color: #27bd2f">
                    <h4 class="card-title text-white">List Pengadaan</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('pengadaan.filter') }}" method="GET" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-5 col-12">
                                <div class="form-group">
                                    <label for="start">Start Date</label>
                                    <input type="date" class="form-control" placeholder="" name="start" id="start"
                                        required />
                                </div>
                            </div>
                            <div class="col-md-5 col-12">
                                <div class="form-group">
                                    <label for="end">End Date</label>
                                    <input type="date" class="form-control" placeholder="" name="end" id="end"
                                        required />
                                </div>
                            </div>
                            <div class="col-md-2 col-12">
                                <div class="form-group d-flex flex-wrap mt-2">
                                    <button type="submit" class="btn btn-primary mr-1">filter</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="myTable table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Judul Pekerjaan</th>
                                    <th>FPP</th>
                                    <th>Pic Procurement</th>
                                    <th>Status </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data as $key => $item)
                                <tr> 
                                    <td>{{$key + 1}}</a></td>
                                    <td><span class="text-success font-weight-bolder">{{$item['judul']}} </span>
                                        <br>
                                        <span class="badge badge-light-warning">{{$item['created_at']}}</span>
                                        <br>
                                        <span class="text-info font-weight-bolder">{{$item['kategori']}}</span>
                                    </td>
                                    <td>
                                        <span class="text-success font-weight-bolder">{{$item['fpp']}}</span>
                                        <br>
                                        <span class="text-warning font-weight-bolder">{{$item['jenis_anggaran']}}</span>-
                                        <span class="text-muted font-weight-bolder">{{$item['tahun_anggaran']}}</span>
                                    </td>
                                    <td>
                                        @for ($i = 0; $i < count($item['pengadaan_pic']); $i++)
                                            <span class="text-success font-weight-bolder">{{$item['pengadaan_pic'][$i]}}</span><br>
                                        @endfor
                                    </td>
                                    <td>
                                        <span class="text-success font-weight-bolder">{{$item['lastUpdate']['tahapan']}}</span>
                                        <br>
                                        @if($item['lastUpdate']['tahapan'] )
                                            <sup class="text-warning ">{{$item['status']}}</sup>
                                        @else
                                        @endif
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-sm dropdown-toggle hide-arrow"
                                                data-toggle="dropdown">
                                                <i data-feather="more-vertical"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{route('pengadaan-detail',$item['id'])}}">
                                                    <i data-feather="file-text" class="mr-50"></i>
                                                    <span>Detail</span>
                                                </a>
                                                {{-- <a class="dropdown-item" href="../../pengadaan-detail/{{$item['id']}}"
                                                >
                                                <i data-feather="file-text" class="mr-50"></i>
                                                <span>View Timeline</span>
                                                </a> --}}
                                                {{-- <a class="dropdown-item" data-toggle="modal"
                                                    data-target="#changeStatus"><i class="mr-50"
                                                        data-feather='file-text'></i>
                                                    <span class="align-middle">Ganti Status</span>
                                                </a> --}}
                                                {{-- <a class="dropdown-item" href="../../pengadaan-detail/{{$item['id']}}">
                                                    <i data-feather="file-text" class="mr-50"></i>
                                                    <span>Last Update Status</span>
                                                </a>
                                                <a class="dropdown-item" href="../../pengadaan-detail/{{$item['id']}}">
                                                    <i data-feather="file-text" class="mr-50"></i>
                                                    <span>Evaluasi Pekerjaan</span>
                                                </a>
                                                <a class="dropdown-item" href="../../pengadaan-detail/{{$item['id']}}">
                                                    <i data-feather="file-text" class="mr-50"></i>
                                                    <span>Edit Form</span>
                                                </a> --}}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>

{{-- @include('finance_accounting.donasi.modal.export')--}}
{{-- @include('pengadaan.modal.createPermintaan') --}}
@include('pengadaan.modal.createPengadaan')
{{-- @include('pengadaan.modal.changeStatus') --}}
@include('pengadaan.modal.exportPengadaan')
{{-- @include('finance_accounting.donasi.modal.eskalasiDokumen') --}}
{{-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript">
function setEskalasi(id) {
    let data = id.split('||');
    $('.title-eskalasi').html('Apakah Anda yakin Untuk Melakukan Eskalasi Pada Dokumen' + data[1] + ' ?');
    $('.id_eskalasi').val(data[0]);
    //     $('.sub-detail').html(data[1] +' - '+ data[3] +'<br>Teknisi '+ data[6]);
    //     $('.vehicle_detail').html(data[4]);
    //     $('.device_detail').html(data[5]);
    //     // let doc_wo = data['']
}
$(document).ready(function() {
    $("#anjing").select2({
        tags: true,
        // data: ["Clare","Cork","South Dublin"]
    });
});

function setHapus(id) {
    let data = id.split('||');

    $('.code').val(1);
    $('.rekening_id').val(data[0]);
    // $('.no_rekening').html(data[1]);
    $('.pesan').html('Apakah Anda Yakin Untuk Membekukan Rekening <span class="text-danger">' + data[1] + '</span> ?');
}

function setAktif(id) {
    let data = id.split('||');

    $('.code').val(2);
    $('.rekening_id').val(data[0]);
    $('.pesan').html('Apakah Anda Yakin Untuk Aktifkan <span class="text-success">' + data[1] + '</span> ?');
}
//check box function
$('#checkAll').on('click', function() {
    if ($(this).prop("checked") == true) {
        $('.checkedStatus').prop('checked', true);
        var dos = [];
        $(".checkedStatus").each(function(i) {
            dos[i] = $(this).val();
        });
        // console.log(dos);
        var loc2 = dos.filter(function(el) {
            return el != '';
        });
        $('.allCheckBox').val(loc2);
        console.log(loc2);
        $('.tombolGeneral').show();
    } else {
        $('.checkedStatus').prop('checked', false);
        $('.tombolGeneral').hide();
    }
});

$('.checkedStatus').on('click', function() {
    var ary = [];
    $("input[name='user_id[]']:checked").each(function(i) {
        ary[i] = $(this).val();
    });
    // console.log(ary.length);
    console.log(ary);
    $('.selectCheckBox').val(ary);
    if ($(this).prop("checked") != true && ary.length == 0) {
        $('.tombolGeneral').hide();
        $('#checkAll').prop('checked', false);
    } else {
        $('.tombolGeneral').show();
    }
});

function setDetail(id) {
    let data = id.split('||');
    console.log(data[2]);
    $('.title-detail').html(data[2]);
    $('.sub-detail').html(data[1] + ' - ' + data[3] + '<br>Teknisi ' + data[6]);
    $('.vehicle_detail').html(data[4]);
    $('.device_detail').html(data[5]);
    // let doc_wo = data['']
}

//data table function
$(document).ready(function() {
    $('.myTable').DataTable({
        "order": [
            [0, "desc"]
        ]
    });
});

//chart header Job report code:2
$(window).on('load', function() {
    'use strict';

    //asset color
    var $textHeadingColor = '#5e5873';
    var $strokeColor = '#ebe9f1';
    var $labelColor = '#e7eef7';
    var $avgSessionStrokeColor2 = '#ebf0f7';
    var $budgetStrokeColor2 = '#dcdae3';
    var $goalStrokeColor2 = '#51e5a8';
    var $revenueStrokeColor2 = '#d0ccff';
    var $textMutedColor = '#b9b9c3';
    var $salesStrokeColor2 = '#df87f2';
    var $white = '#fff';
    var $earningsStrokeColor2 = '#28c76f66';
    var $earningsStrokeColor3 = '#28c76f33';

    var goalChartOptions;
    var goalChart;

    var $goalOverviewChart = document.querySelector('#summaryJR');

    // Goal Overview  Chart
    // -----------------------------
    goalChartOptions = {
        chart: {
            height: 245,
            type: 'radialBar',
            sparkline: {
                enabled: true
            },
            dropShadow: {
                enabled: true,
                blur: 3,
                left: 1,
                top: 1,
                opacity: 0.1
            }
        },
        colors: [$goalStrokeColor2],
        plotOptions: {
            radialBar: {
                offsetY: -10,
                startAngle: -150,
                endAngle: 150,
                hollow: {
                    size: '77%'
                },
                track: {
                    background: $strokeColor,
                    strokeWidth: '50%'
                },
                dataLabels: {
                    name: {
                        show: false
                    },
                    value: {
                        color: $textHeadingColor,
                        fontSize: '2.86rem',
                        fontWeight: '600'
                    }
                }
            }
        },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'horizontal',
                shadeIntensity: 0.5,
                gradientToColors: [window.colors.solid.success],
                inverseColors: true,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100]
            }
        },
        series: [20],
        stroke: {
            lineCap: 'round'
        },
        grid: {
            padding: {
                bottom: 30
            }
        }
    };
    goalChart = new ApexCharts($goalOverviewChart, goalChartOptions);
    goalChart.render();


});
</script>
@endsection