<h6>Pra Tender</h6>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="pre_tender_no_nr" class="form-label">No.NR</label>
                                        <input type="text" class="form-control" id="pre_tender_no_nr" value="{{old('pre_tender_no_nr')}}"
                                            placeholder="Optional" name="pre_tender_no_nr" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="pre_tender_tanggal" class="form-label">Tanggal</label>
                                        <input type="date" class="form-control" id="pre_tender_tanggal" value="{{old('pre_tender_tanggal')}}"
                                            placeholder="Optional" name="pre_tender_tanggal" />
                                    </div>
                                </div>
                            </div>
                            <h6>BA Aanwijzing</h6>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="ba_aanwijzing_no" class="form-label">No</label>
                                        <input type="text" class="form-control" id="ba_aanwijzing_no" value="{{old('ba_aanwijzing_no')}}"
                                            placeholder="Optional" name="ba_aanwijzing_no" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="ba_aanwijzing_tanggal" class="form-label">Tanggal</label>
                                        <input type="date" class="form-control" id="ba_aanwijzing_tanggal" value="{{old('ba_aanwijzing_tanggal')}}"
                                            placeholder="Optional" name="ba_aanwijzing_tanggal" />
                                    </div>
                                </div>
                            </div>
                            <h6>BA Aanwijzing Lapangan</h6>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="ba_aanwijzing_lapangan_no" class="form-label">No</label>
                                        <input type="text" class="form-control" id="ba_aanwijzing_lapangan_no" value="{{old('ba_aanwijzing_lapangan_no')}}"
                                            placeholder="Optional" name="ba_aanwijzing_lapangan_no" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="ba_aanwijzing_lapangan_tanggal" class="form-label">Tanggal</label>
                                        <input type="date" class="form-control" id="ba_aanwijzing_lapangan_tanggal" value="{{old('ba_aanwijzing_lapangan_tanggal')}}"
                                            placeholder="Optional" name="ba_aanwijzing_lapangan_tanggal" />
                                    </div>
                                </div>
                            </div>
                            <h6>TKDN</h6>
                            <hr>
                            <div class="row">
                                <div class="col-md-3 col-3">
                                    <div class="form-group">
                                        <label for="form_a2_tkdn" class="form-label">Form A2</label>
                                        <input type="text" class="form-control" id="form_a2_tkdn" value="{{old('form_a2_tkdn')}}"
                                            placeholder="Optional" name="form_a2_tkdn" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-3">
                                    <div class="form-group">
                                        <label for="form_a3_nilai_tkdn_barang" class="form-label">Form A3 Barang</label>
                                        <input type="text" class="form-control" id="form_a3_nilai_tkdn_barang" value="{{old('form_a3_nilai_tkdn_barang')}}"
                                            placeholder="Optional" name="form_a3_nilai_tkdn_barang" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-3">
                                    <div class="form-group">
                                        <label for="tkdn_barang" class="form-label">Barang</label>
                                        <input type="text" class="form-control" id="tkdn_barang" value="{{old('tkdn_barang')}}"
                                            placeholder="Optional" name="tkdn_barang" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-3">
                                    <div class="form-group">
                                        <label for="form_a4_nilai_tkdn_jasa" class="form-label">Form A4 Jasa</label>
                                        <input type="text" class="form-control" id="form_a4_nilai_tkdn_jasa" value="{{old('form_a4_nilai_tkdn_jasa')}}"
                                            placeholder="Optional" name="form_a4_nilai_tkdn_jasa" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-4">
                                    <div class="form-group">
                                        <label for="tkdn_jasa" class="form-label">Jasa</label>
                                        <input type="text" class="form-control" id="tkdn_jasa" value="{{old('tkdn_jasa')}}"
                                            placeholder="Optional" name="tkdn_jasa" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-4">
                                    <div class="form-group">
                                        <label for="tkdn_jasa" class="form-label">Form A5 Barang & Jasa</label>
                                        <input type="text" class="form-control" id="tkdn_jasa" value="{{old('tkdn_jasa')}}"
                                            placeholder="Optional" name="tkdn_jasa" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-4">
                                    <div class="form-group">
                                        <label for="tkdn_jasa_barang" class="form-label">Barang & Jasa</label>
                                        <input type="text" class="form-control" id="tkdn_jasa_barang" value="{{old('tkdn_jasa_barang')}}"
                                            placeholder="Optional" name="tkdn_jasa_barang" />
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="keterangan">Deskription</label>
                                            <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                                name="keterangan"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>