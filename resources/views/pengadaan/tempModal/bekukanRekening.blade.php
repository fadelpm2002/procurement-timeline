<div class="modal fade text-left" id="importJR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27bd2f">
                <h4 class="modal-title text-white" id="myModalLabel33">Bekukan Rekening</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="" action="{{url('bekukan-rekening')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" class="code" name="code">
                <input type="hidden" class="rekening_id" name="rekening_id">
                <div class="modal-body">
                    <h5 class="pesan"></h5>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lanjutkan</button>
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>