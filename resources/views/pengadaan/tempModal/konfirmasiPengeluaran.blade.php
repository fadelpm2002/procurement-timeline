<div class="modal fade text-left" id="konfirmasiPengeluaran" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27bd2f">
                <h4 class="modal-title text-white" id="myModalLabel33">Konfirmasi Penerimaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="" action="{{url('donasi-konfirmasi-penerimaan')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" class="id" name="id" value="{{$data['id']}}">
                {{-- <input type="hidden" class="item_id" name="item_id" value="{{$data['item_id']}}"> --}}
                <div class="modal-body">
                    <h5>Konfirmasi Penerimaan Uang </h5>
                    <p class="no-doc-konfirmasi"></p>
                    <div class="form-group">
                        <label for="invoice-subject" class="form-label">No.Rekening</label>
                        <select class="select2 form-control no-rek-konfirmasi" name="no_rek" aria-placeholder="silahkan pilih nama bank">
                            <option value=""></option>
                            @php
                                $rek = App\Rekening::where('status',1)->get();
                            @endphp
                            @foreach ($rek as $rekening)
                            <option value="{{$rekening->id}}">{{$rekening->no_rekening}} - {{$rekening->name_rekening}}</option>
                            @endforeach
                        </select>
                    </div>
                        <div class="form-group">
                            <label for="label-textarea">Nominal</label>
                            <div class="input-group input-group-merge mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp.</span>
                                </div>
                                {{-- <input type="number" class="form-control nominal-konfirmasi" value="{{$data['nominal']}}" placeholder="100.000" name="nominal" required/> --}}
                                <input type="text" class="form-control numeral-mask" id="numeral-formatting" value="{{$data['nominal']}}" placeholder="100.000" name="nominal" required/>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lanjutkan</button>
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>