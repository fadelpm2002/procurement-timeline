<div class="modal fade" id="eskalasiDokumen" tabindex="-1" role="dialog" aria-labelledby="eskalasiDokumen" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header " style="background-color: #27bd2f">
                <h5 class="modal-title text-white" id="eskalasiDokumen" > Form Eskalasi Dokumen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/eskalasi-pengerluaran-uang" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                <div class="row">
                   
                <input type="hidden" class="id_eskalasi" name="id">
                    <div class="col-md-12 col-12">
                        <H4 class="title-eskalasi"> </H4>
                        <small class="text-muted">Silahkan Pilih Delegasi</small>
                        <div class="form-group">
                            <label for="invoice-subject" class="form-label">Delegasi</label>
                            <select class="select2 form-control" name="mengetahui" aria-placeholder="silahkan pilih nama bank" required>
                                @php
                                    $user = App\User::where('status_user',1)->get();
                                @endphp
                                <option value=""></option>
                                @foreach ($user as $users)
                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                    
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
                <br>
                <br>
                <br>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Accept</button>
            </div>
        </form>
        </div>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">

let by = 1;
tambahItens(by);
function tambahItens(number) {
    // let pilih = 'selectpicker';
    let CRtambahItens = '';
        CRtambahItens += '<div class ="row" id ="tambahCameraNew">'+
            '<div class="col-md-4 col-4">'+
                '<div class="form-group">'+
                    '<label for="invoice-subject" class="form-label">Saldo Rekening</label>'+
                    '<div class="input-group input-group-merge mb-2">'+
                        '<div class="input-group-prepend">'+
                            '<span class="input-group-text">Rp.</span>'+
                        '</div>'+
                       '<input type="text" class="form-control numeral-mask" id="numeral-formatting" value="{{old('nominal')}}" placeholder="100.000" name="nominal[]" required/>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-md-4 col-4">'+
                '<div class="form-group">'+
                    '<label for="invoice-from" class="form-label">Deskripsi</label>'+
                    '<input type="text" class="form-control" id="invalid-state" value="{{old('tanggal')}}" placeholder="64278xxxxxxx" name="deskripsi_item[]" />'+
                '</div>'+
            '</div>'+
            '<div class="col-md-2">'+
                '<button type="button" class="btn_tambah_items btn btn-icon btn-flat-success">+</button>'+
                '<button type="button" class="remove btn btn-icon btn-flat-danger">-</button>'+
                // '<button type="button" class="btn btn-gradient-danger remove" >-</button>'+
            '</div>'+
        '</div>';
    if (number > 1) {
        $('.new-items').append(CRtambahItens);
    }
}
$(document).on('click', '.btn_tambah_items', function() {
    by++;
    tambahItens(by);
});
$(document).on('click', '.remove', function(){
    by--;
    $(this).closest("#tambahCameraNew").remove();
});

$(function() {
    $(".testing").select2({
        tags: true,
        createTag: function (params) {
          return {
            id: params.term,
            text: params.term,
            newOption: true
          }
        },
        templateResult: function (data) {
          var $result = $("<span></span>");
    
          $result.text(data.text);
    
          if (data.newOption) {
            $result.append(" <em>(new)</em>");
          }
    
          return $result;
        }
    });
});
    
</script>