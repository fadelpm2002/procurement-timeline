<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header " style="background-color: #27bd2f">
                <h5 class="modal-title text-white" id="exampleModalScrollableTitle"> Form Pemgajuan Donasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/donasi-create" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12 col-12 jenis_donasi_col">
                            <div class="form-group">
                                <label for="invoice-from" class="form-label">Jenis Donasi</label>
                                <select class="select2 form-control jenis_donasi" name="jenis"
                                    aria-placeholder="silahkan pilih nama bank" required>
                                    <option value=""></option>
                                    <option value="1">Infaq</option>
                                    <option value="2">Infaq Jumat</option>
                                    <option value="3">Zakat Maal</option>
                                    <option value="4">Zakat Fitrah</option>
                                    <option value="5">Sedekah Barang</option>
                                    <option value="6">Lain-lain</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-12 tanggal_penerima_col">
                            <div class="form-group">
                                <label for="invoice-from" class="form-label">Tanggal Penerimaan</label>
                                <input type="date" class="form-control" id="invalid-state" value="{{old('tanggal')}}"
                                    placeholder="" name="tanggal_penerimaan" required />
                            </div>
                        </div>
                        <div class="col-md-4 col-12 nominal_pembayaran">
                            <div class="form-group">
                                <label for="invoice-subject" class="form-label">No.Rekening</label>
                                <select class="select2 form-control" name="no_rek"
                                    aria-placeholder="silahkan pilih nama bank" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-12 nominal_pembayaran">
                            <div class="form-group">
                                <label for="invoice-subject" class="form-label">Nominal Pembayaran</label>
                                <div class="input-group input-group-merge mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </div>
                                    <input type="text" class="form-control numeral-mask" id="numeral-formatting"
                                        value="" placeholder="100.000" name="nominal" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-12">
                            <div class="form-group">
                                <label for="label-textarea">Deskription</label>
                                <textarea class="form-control" id="label-textarea" rows="3" placeholder=""
                                    name="description"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 col-12">
                            <div class="form-group">
                                <label for="invoice-from" class="form-label">Nama Pemberi</label>
                                <select class="form-control testing" data-live-search="true" name="name_pemberi"
                                    style="width: 100%;">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label for="invoice-from" class="form-label">email</label>
                                <input type="text" class="form-control" id="invalid-state" value="{{old('tanggal')}}"
                                    placeholder="donasi@umat.com" name="email" />
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label for="invoice-from" class="form-label">No.HP</label>
                                <input type="text" class="form-control" id="invalid-state" value="{{old('tanggal')}}"
                                    placeholder="0853xxxxxxxx" name="no_hp" />
                            </div>
                        </div>
                        <div class="col-md-12 col-12">
                            <div class="form-group">
                                <label for="label-textarea">Alamat</label>
                                <textarea class="form-control" id="label-textarea" rows="3" placeholder="Alamat Umat"
                                    name="alamat"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row itembarang" style="display: none">
                        <div class="col-md-4 col-4">
                            <div class="form-group">
                                <label for="invoice-subject" class="form-label">Nama Barang</label>
                                <div class="input-group input-group-merge mb-2">
                                    <input type="text" class="form-control" value="" placeholder=""" name="
                                        name_barang[]" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-4">
                            <div class="form-group">
                                <label for="invoice-from" class="form-label">Qty Barang</label>
                                <input type="number" class="form-control" id="invalid-state" value="{{old('tanggal')}}"
                                    placeholder="64278xxxxxxx" name="qty[]" />
                            </div>
                        </div>
                        <div class="col-md-2 col-2">
                            <button type="button" class="btn_tambah_items btn btn-icon btn-flat-success">
                                +
                            </button>
                        </div>
                    </div>
                    <div class="new-items"></div>
                    {{-- <div class="row">
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label for="invoice-subject" class="form-label">Mengetahui</label>
                                <select class="select2 form-control" name="mengetahui" aria-placeholder="silahkan pilih nama bank" required>
                                    @php
                                        $user = App\User::where('status_user',1)->get();
                                    @endphp
                                    <option value=""></option>
                                    @foreach ($user as $users)
                                        <option value="{{$users->id}}">{{$users->nama}}</option>

                    @endforeach
                    </select>
            </div>
        </div>
        <div class="col-md-6 col-6">
            <div class="form-group">
                <label for="invoice-subject" class="form-label">Menyetujui</label>
                <select class="select2 form-control" name="menyetujui" aria-placeholder="silahkan pilih nama bank"
                    required>
                    <option value=""></option>
                    @foreach ($user as $users)
                    <option value="{{$users->id}}">{{$users->name}}</option>

                    @endforeach
                </select>
            </div>
        </div>
    </div> --}}
    <br>
    <br>
    <br>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary">Accept</button>
</div>
</form>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(function() {
    $(document).on('change', '.jenis_donasi', function() {
        if ($(this).val() == 5) {
            $('.itembarang').show(500);
            $('.nominal_pembayaran').css("display", "none");
            $('.nominal_pembayaran').hide(500);
            $('.jenis_donasi_col').removeClass("col-md-12");
            $('.jenis_donasi_col').addClass("col-md-6");
            $('.tanggal_penerima_col').addClass("col-md-6");
            // $('.nominal_pembayaran').hide(500);
        } else {
            $('.itembarang').hide(500);
            $('.nominal_pembayaran').show(500);
            $('.jenis_donasi_col').removeClass("col-md-12");
            $('.jenis_donasi_col').addClass("col-md-12");
            $('.tanggal_penerima_col').removeClass("col-md-6");
            $('.tanggal_penerima_col').addClass("col-md-4");
        }
    })
})

///
let bay = 1;
barangDonasi(bay);

function barangDonasi(bay) {
    // let pilih = 'selectpicker';
    let CRbarangDonasi = '';
    CRbarangDonasi += '<div class ="row" id ="tambahBarangDonasi">' +
        '<div class="col-md-4 col-4">' +
        '<div class="form-group">' +
        '<label for="invoice-subject" class="form-label">Nama Barang</label>' +
        '<input type="text" class="form-control" value="" placeholder="" name="name_barang[]"/>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-4 col-4">' +
        '<div class="form-group">' +
        '<label for="invoice-from" class="form-label">qty</label>' +
        '<input type="number" class="form-control" id="invalid-state" value="{{old('
    tanggal ')}}" placeholder="" name="qty[]" />' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2">' +
        '<button type="button" class="btn_tambah_items btn btn-icon btn-flat-success">+</button>' +
        '<button type="button" class="remove btn btn-icon btn-flat-danger">-</button>' +
        // '<button type="button" class="btn btn-gradient-danger remove" >-</button>'+
        '</div>' +
        '</div>';
    if (bay > 1) {
        $('.new-items').append(CRbarangDonasi);
    }
}
$(document).on('click', '.btn_tambah_items', function() {
    bay++;
    barangDonasi(bay);
});
$(document).on('click', '.remove', function() {
    bay--;
    $(this).closest("#tambahBarangDonasi").remove();
});


///
$(function() {
    $(".testing").select2({
        tags: true,
        createTag: function(params) {
            return {
                id: params.term,
                text: params.term,
                newOption: true
            }
        },
        templateResult: function(data) {
            var $result = $("<span></span>");

            $result.text(data.text);

            if (data.newOption) {
                $result.append(" <em>(new)</em>");
            }

            return $result;
        }
    });
});
</script>