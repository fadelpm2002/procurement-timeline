<div class="modal fade text-left" id="changeStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27bd2f">
                <h4 class="modal-title text-white" id="myModalLabel33">Change Status</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="" action="/pengadaan-ubah-status/{{$data['id']}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @method('PUT')
                <input type="hidden" class="id" name="id" value="">
                <div class="modal-body">
                    <div class="col-md-12 col-12">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="select2 form-control status" name="status"
                                aria-placeholder="silahkan pilih Status" required>
                                <option value=""></option>
                                <option value="4">On Going</option>
                                <option value="5">On Hold</option>
                                <option value="6">Done</option>
                                <option value="7">Gagal</option>
                                <option value="8">Batal</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lanjutkan</button>
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>