@extends('layouts.layout2')
@section('title', 'Pengadaan - Procurement')
@section('main_wrapper')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-9 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Procurement</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Pengadaan</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header-right text-md-right col-md-3 col-3 d-md-block">
        <div class="form-group breadcrumb-right ">
            <div class="dropdown text-right">
                <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i data-feather="grid">
                    </i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item exportJR" href="/pengadaan-index"><i class="mr-1"
                        data-feather='package'></i><span class="align-middle">Index Tabel Pengadaan
                        </span>
                    </a>
                    @if(Auth::user()->id == 1 || Auth::user()->id == 2)
                    <a class="dropdown-item exportJR" data-toggle="modal" data-target="#createPengadaan"><i class="mr-1"
                            data-feather='package'></i><span class="align-middle">Tambah
                            Pengadaan</span></a>
                    @endif
                    <a class="dropdown-item" data-toggle="modal" data-target="#exportPengadaan"><i class="mr-1"
                            data-feather='file-text'></i>
                        <span class="align-middle">Export Pengadaan</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('main_content')
<section id="basic-datatable">
    <div class="row ">
        <div class="col-md-4 col-12">
            <div class="card ">
                <div class="card-header border-bottom mb-1" style="background-color: #0f3935">
                    <h4 class="card-title font-weight-bolder text-white">List Pengadaan</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="myTable table">
                            <thead>
                                <tr>
                                    <th style="display: none"></th>
                                    <th style="display: none"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data as $key => $item)
                                <tr>
                                   <td style="display: none">
                                        {{$key+1}}
                                   </td>
                                    <td>
                                        <div class="card">
                                            <div class="card-body"  style="border: 1px solid; padding: 10px;box-shadow: 5px 9px;">
                                                <div class="row">
                                                    <div class="col-10">
                                                        <button class="btn text-left" id="{{$item['id']}}" onclick="setTimeline(this.id)" >
                                                            <span class="text-success font-weight-bolder">{{$item['judul']}} </span>
                                                            <br>
                                                            <span class="badge badge-light-warning">{{$item['created_at']}}</span>
                                                            <br>
                                                            <span class="badge badge-light-primary">{{$item['status']}}</span> -
                                                            @if($item['lastUpdate']['tahapan'] )
                                                                <span class="badge badge-light-info">{{$item['lastUpdate']['tahapan']}}</span>
                                                            @endif
                                                        </button>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="dropdown">
                                                            <button type="button" class="btn btn-sm dropdown-toggle hide-arrow"
                                                                data-toggle="dropdown">
                                                                <i data-feather="more-vertical"></i>
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <a class="dropdown-item" href="../../pengadaan-detail/{{$item['id']}}">
                                                                    <i data-feather="file-text" class="mr-50"></i>
                                                                    <span>Detail</span>
                                                                </a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-12">
            <div class="card ">
                <div class="card-header border-bottom mb-1" style="background-color: #0f3935">
                    <h4 class="card-title font-weight-bolder text-white">Timeline</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-update-tahapan" action="" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @method('PUT')
                                <input type="hidden" class="id_tahapan" name="id_tahapan" value="">
                                <input type="hidden" class="tahapan" name="tahapan" >
                                <div class="form-tahapan"></div>
                            <div class="col-md-6 col-6">
                                <button type="submit" class="btn btn-outline-primary simpan" style="display: none" name="tombol" value="0">Simpan</button>
                                <button type="submit" class="btn btn-outline-success lanjutkan" style="display: none" name="tombol" value="1">Lanjutkan</button>
                                {{-- <button class="btn btn-danger" data-dismiss="modal">Cancel</button> --}}
                            </form>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <div class="timelien"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@include('pengadaan.modal.createPengadaan')
@include('pengadaan.modal.exportPengadaan')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript">

//data table function
function setTimeline(id) {
    //url form
    $('.form-update-tahapan').attr("action", '/pengadaan-update-tahapan/'+id+'');

    //pengadaan
    fetch('../pengadaan-detail-api/' + id + '', {
            method: 'get'
        })
        .then(response => response.json())
        .then(data => {
            $('.id_tahapan').val(data['lastUpdate']['id']);
            $('.tahapan').val(data['lastUpdate']['id_tahapan']);
            let keterangan = data['lastUpdate']['keterangan'] == null ? "" : data['lastUpdate']['keterangan'];
            if (data['lastUpdate']['id_tahapan'] == 1) {
                //create form
                let form = '<div class="row">'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""name="keterangan">'+keterangan+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').hide(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 2) {
                //create form
                let plan_pengumuman_tender = data['plan_pengumuman_tender'] == null ? "" : data['plan_pengumuman_tender'];

                let form = '<div class="row">'+
                    '<div class="col-md-12 col-12">'+
                        '<div class="form-group">'+
                           ' <label>Status</label>'+
                            '<select class="pilihan form-control status" name="status_pengadaan"'+
                               ' aria-placeholder="silahkan pilih Status" >'+
                               '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                '<option value="4">On Going</option>'+
                               ' <option value="5">On Hold</option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-12 col-12">'+
                        '<div class="form-group">'+
                            '<label for="pengumuman_tender" class="form-label">Pengumuman Tender</label>'+
                            '<input type="date" class="form-control" id="pengumuman_tender" value="'+ plan_pengumuman_tender +'" placeholder="Optional" name="pengumuman_tender" />'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-12 col-12">'+
                        '<div class="form-group">'+
                            '<label for="keterangan">Deskription</label>'+
                            '<textarea class="form-control" class="keterangan" rows="3" placeholder=""name="keterangan">'+keterangan+'</textarea>'+
                        '</div>'+
                    '</div>'+
                '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 3) {
                //create form
                let plan_prebid_meeting = data['plan_prebid_meeting'] == null ? "" : data['plan_prebid_meeting'];

                let form = '<div class="row">'+
                            '<div class="col-md-12 col-12">'+
                                '<div class="form-group">'+
                                ' <label>Status</label>'+
                                    '<select class="select2 form-control status" name="status_pengadaan"'+
                                    ' aria-placeholder="silahkan pilih Status" >'+
                                        '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                        '<option value="4">On Going</option>'+
                                    ' <option value="5">On Hold</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="penyelesain_proses_pengadaan" class="form-label">Plan Prebid Meeting </label>'+
                                        '<div class="input-group input-group-merge mb-2">'+
                                            '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="penyelesain_proses_pengadaan" value="'+ plan_prebid_meeting +'" name="penyelesain_proses_pengadaan"/>'+
                                            '<div class="input-group-append">'+
                                                '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                            '<label for="keterangan">Deskription</label>'+
                                            '<textarea class="form-control" class="keterangan" rows="3" placeholder=""name="keterangan">'+ keterangan +'</textarea>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 4) {
                //create form
                let plan_prebid_lapangan = data['plan_prebid_lapangan'] == null ? "" : data['plan_prebid_lapangan'];
                let pre_tender_no_nr = data['pre_tender_no_nr'] == null ? "" : data['pre_tender_no_nr'];
                let pre_tender_tanggal = data['pre_tender_tanggal'] == null ? "" : data['pre_tender_tanggal'];
                let ba_aanwijzing_no = data['ba_aanwijzing_no'] == null ? "" : data['ba_aanwijzing_no'];
                let ba_aanwijzing_tanggal = data['ba_aanwijzing_tanggal'] == null ? "" : data['ba_aanwijzing_tanggal'];
                let form_a2_tkdn = data['form_a2_tkdn'] == null ? "" : data['form_a2_tkdn'];
                let form_a3_nilai_tkdn_barang = data['form_a3_nilai_tkdn_barang'] == null ? "" : data['form_a3_nilai_tkdn_barang'];
                let tkdn_barang = data['tkdn_barang'] == null ? "" : data['tkdn_barang'];
                let form_a4_nilai_tkdn_jasa = data['form_a4_nilai_tkdn_jasa'] == null ? "" : data['form_a4_nilai_tkdn_jasa'];
                let tkdn_jasa = data['tkdn_jasa'] == null ? "" : data['tkdn_jasa'];
                let form_a5_nilai_tkdn_jasa_barang = data['form_a5_nilai_tkdn_jasa_barang'] == null ? "" : data['form_a5_nilai_tkdn_jasa_barang'];
                let tkdn_jasa_barang = data['tkdn_jasa_barang'] == null ? "" : data['tkdn_jasa_barang'];

                let form = '<h6>Pra Tender</h6>'+
                    '<hr>'+
                    '<div class="row">'+
                        '<div class="col-md-6 col-6">'+
                                '<div class="form-group">'+
                                ' <label>Status</label>'+
                                    '<select class="select2 form-control status" name="status_pengadaan"'+
                                    ' aria-placeholder="silahkan pilih Status" >'+
                                        '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                        '<option value="4">On Going</option>'+
                                    ' <option value="5">On Hold</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                        '<div class="col-md-6 col-6">'+
                            '<div class="form-group">'+
                                '<label for="plan_prebid_lapangan" class="form-label">Plan Prebid Lapangan </label>'+
                                '<input type="date" class="form-control" id="plan_prebid_lapangan" value="'+plan_prebid_lapangan+'"'+
                                   ' placeholder="Optional" name="plan_prebid_lapangan" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 col-6">'+
                            '<div class="form-group">'+
                                '<label for="pre_tender_no_nr" class="form-label">No.NR</label>'+
                                '<input type="text" class="form-control" id="pre_tender_no_nr" value="'+pre_tender_no_nr+'"'+
                                    'placeholder="Optional" name="pre_tender_no_nr" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 col-6">'+
                            '<div class="form-group">'+
                                '<label for="pre_tender_tanggal" class="form-label">Tanggal</label>'+
                                '<input type="date" class="form-control" id="pre_tender_tanggal" value="'+pre_tender_tanggal+'"'+
                                    'placeholder="Optional" name="pre_tender_tanggal" />'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<h6>BA Aanwijzing</h6>'+
                   ' <hr>'+
                    '<div class="row">'+
                        '<div class="col-md-6 col-6">'+
                            '<div class="form-group">'+
                                '<label for="ba_aanwijzing_no" class="form-label">No</label>'+
                                '<input type="text" class="form-control" id="ba_aanwijzing_no" value="'+ba_aanwijzing_no+'"'+
                                    'placeholder="Optional" name="ba_aanwijzing_no" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 col-6">'+
                            '<div class="form-group">'+
                                '<label for="ba_aanwijzing_tanggal" class="form-label">Tanggal</label>'+
                                '<input type="date" class="form-control" id="ba_aanwijzing_tanggal" value="'+ba_aanwijzing_tanggal+'"'+
                                    'placeholder="Optional" name="ba_aanwijzing_tanggal" />'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<h6>TKDN</h6>'+
                    '<hr>'+
                    '<div class="row">'+
                        '<div class="col-md-3 col-3">'+
                            '<div class="form-group">'+
                                '<label for="form_a2_tkdn" class="form-label">Form A2</label>'+
                                '<input type="text" class="form-control" id="form_a2_tkdn" value="'+form_a2_tkdn+'"'+
                                    'placeholder="Optional" name="form_a2_tkdn" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-3 col-3">'+
                            '<div class="form-group">'+
                                '<label for="form_a3_nilai_tkdn_barang" class="form-label">Form A3 Barang</label>'+
                               ' <input type="text" class="form-control" id="form_a3_nilai_tkdn_barang" value="'+form_a3_nilai_tkdn_barang+'"'+
                                    'placeholder="Optional" name="form_a3_nilai_tkdn_barang" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-3 col-3">'+
                            '<div class="form-group">'+
                                '<label for="tkdn_barang" class="form-label">Barang</label>'+
                                '<input type="text" class="form-control" id="tkdn_barang" value="'+tkdn_barang+'"'+
                                   ' placeholder="Optional" name="tkdn_barang" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-3 col-3">'+
                            '<div class="form-group">'+
                                '<label for="form_a4_nilai_tkdn_jasa" class="form-label">Form A4 Jasa</label>'+
                                '<input type="text" class="form-control" id="form_a4_nilai_tkdn_jasa" value="'+form_a4_nilai_tkdn_jasa+'"'+
                                    'placeholder="Optional" name="form_a4_nilai_tkdn_jasa" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4 col-4">'+
                            '<div class="form-group">'+
                                '<label for="tkdn_jasa" class="form-label">Jasa</label>'+
                                '<input type="text" class="form-control" id="tkdn_jasa" value="'+tkdn_jasa+'"'+
                                    'placeholder="Optional" name="tkdn_jasa" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4 col-4">'+
                            '<div class="form-group">'+
                                '<label for="form_a5_nilai_tkdn_jasa_barang" class="form-label">Form A5 Barang & Jasa</label>'+
                                '<input type="text" class="form-control" id="form_a5_nilai_tkdn_jasa_barang" value="'+form_a5_nilai_tkdn_jasa_barang+'"'+
                                    'placeholder="Optional" name="form_a5_nilai_tkdn_jasa_barang" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4 col-4">'+
                            '<div class="form-group">'+
                                '<label for="tkdn_jasa_barang" class="form-label">Barang & Jasa</label>'+
                                '<input type="text" class="form-control" id="tkdn_jasa_barang" value="'+tkdn_jasa_barang+'"'+
                                   ' placeholder="Optional" name="tkdn_jasa_barang" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-12 col-12">'+
                            '<div class="form-group">'+
                                    '<label for="keterangan">Deskription</label>'+
                                    '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                        'name="keterangan">'+ keterangan +'</textarea>'
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 5) {
                let plan_pemasukan_penawaran = data['plan_pemasukan_penawaran'] == null ? "" : data['plan_pemasukan_penawaran'];
                let ba_aanwijzing_lapangan_no = data['ba_aanwijzing_lapangan_no'] == null ? "" : data['ba_aanwijzing_lapangan_no'];
                let ba_aanwijzing_lapangan_tanggal = data['ba_aanwijzing_lapangan_tanggal'] == null ? "" : data['ba_aanwijzing_lapangan_tanggal'];

                //create form
                let form = '<h6>BA Aanwijzing Lapangan</h6>'+
                            '<hr>'+
                            '<div class="row">'+
                                '<div class="col-md-12 col-12">'+
                                '<div class="form-group">'+
                                ' <label>Status</label>'+
                                    '<select class="select2 form-control status" name="status_pengadaan"'+
                                    ' aria-placeholder="silahkan pilih Status" >'+
                                        '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                        '<option value="4">On Going</option>'+
                                    ' <option value="5">On Hold</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                               ' <div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="pemasukan_penawaran" class="form-label">Plan Pemasukan Penawaran</label>'+
                                        '<input type="date" class="form-control" id="pemasukan_penawaran" value="'+ plan_pemasukan_penawaran +'"'+
                                            'placeholder="Optional" name="pemasukan_penawaran" />'+
                                    '</div>'+
                                '</div>'+
                               ' <div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="ba_aanwijzing_lapangan_no" class="form-label">No</label>'+
                                        '<input type="text" class="form-control" id="ba_aanwijzing_lapangan_no" value="'+ ba_aanwijzing_lapangan_no +'"'+
                                            'placeholder="Optional" name="ba_aanwijzing_lapangan_no" />'+
                                    '</div>'+
                                '</div>'+
                               ' <div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="ba_aanwijzing_lapangan_tanggal" class="form-label">Tanggal</label>'+
                                        '<input type="date" class="form-control" id="ba_aanwijzing_lapangan_tanggal" value="'+ ba_aanwijzing_lapangan_tanggal +'"'+
                                            'placeholder="Optional" name="ba_aanwijzing_lapangan_tanggal" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+keterangan+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 6) {
                let plan_evaluasi_sampul = data['plan_evaluasi_sampul'] == null ? "" : data['plan_evaluasi_sampul'];
                let nilai_oe = data['nilai_oe'] == null ? "" : data['nilai_oe'];
                let harga_penawaran = data['harga_penawaran'] == null ? "" : data['harga_penawaran'];
                //create form
                let form = '<div class="row">'+
                            '<div class="col-md-12 col-12">'+
                                '<div class="form-group">'+
                                ' <label>Status</label>'+
                                    '<select class="select2 form-control status" name="status_pengadaan"'+
                                    ' aria-placeholder="silahkan pilih Status" >'+
                                        '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                        '<option value="4">On Going</option>'+
                                    ' <option value="5">On Hold</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="evaluasi_sampul" class="form-label">Plan Evaluasi Sampul</label>'+
                                        '<input type="date" class="form-control" id="evaluasi_sampul" value="'+ plan_evaluasi_sampul +'"'+
                                            'placeholder="Optional" name="evaluasi_sampul" />'+
                                    '</div>'+
                                '</div>'+
                                    '<div class="col-md-4 col-4">'+
                                        '<div class="form-group">'+
                                            '<label for="nilai_oe" class="form-label">Nilai OE(sebelum ppn)</label>'+
                                           ' <input type="number" class="form-control" id="nilai_oe" value="'+ nilai_oe +'"'+
                                               ' placeholder="Optional" name="nilai_oe" />'+
                                            '<div class="total_oe"></div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-4 col-4">'+
                                        '<div class="form-group">'+
                                            '<label for="harga_penawaran" class="form-label">Harga Penawaran (sebelum PPN)</label>'+
                                            '<input type="number" class="form-control" id="harga_penawaran" value="'+ harga_penawaran +'"'+
                                               ' placeholder="Optional" name="harga_penawaran" />'+
                                            '<div class="total_harga_penawaran"></div>'+
                                        '</div>'+
                                    '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+keterangan+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 7) {
                let plan_negosiasi = data['plan_negosiasi'] == null ? "" : data['plan_negosiasi'];
                //create form
                let form = '<div class="row">'+
                            '<div class="col-md-6 col-6">'+
                                '<div class="form-group">'+
                                ' <label>Status</label>'+
                                    '<select class="select2 form-control status" name="status_pengadaan"'+
                                    ' aria-placeholder="silahkan pilih Status" >'+
                                        '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                        '<option value="4">On Going</option>'+
                                    ' <option value="5">On Hold</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                                '<div class="col-md-6 col-6">'+
                                    '<div class="form-group">'+
                                        '<label for="negosiasi" class="form-label">Plan Negosiasi</label>'+
                                        '<input type="date" class="form-control" id="negosiasi" value="'+ plan_negosiasi +'"'+
                                            'placeholder="Optional" name="negosiasi" />'+
                                   ' </div>'+
                               ' </div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+keterangan+'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 8) {
                let lhp_plan = data['plan_lhp_plan'] == null ? "" : data['plan_lhp_plan'];
                let harga_setelah_negosiasi = data['harga_setelah_negosiasi'] == null ? "" : data['harga_setelah_negosiasi'];
                //create form
                let form = '<div class="row">'+
                            '<div class="col-md-12 col-12">'+
                                '<div class="form-group">'+
                                ' <label>Status</label>'+
                                    '<select class="select2 form-control status" name="status_pengadaan"'+
                                    ' aria-placeholder="silahkan pilih Status" >'+
                                        '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                        '<option value="4">On Going</option>'+
                                    ' <option value="5">On Hold</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                                '<div class="col-md-6 col-6">'+
                                    '<div class="form-group">'+
                                        '<label for="lhp_plan" class="form-label">Plan LHP</label>'+
                                        '<input type="date" class="form-control" id="lhp_plan" value="'+ lhp_plan +'"'+
                                            'placeholder="Optional" name="lhp_plan" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-6 col-6">'+
                                    '<div class="form-group">'+
                                        '<label for="harga_setelah_negosiasi" class="form-label">Harga setelah Negosiasi(sebelum PPN)</label>'+
                                        '<input type="number" class="form-control" id="harga_setelah_negosiasi" value="'+ harga_setelah_negosiasi +'"'+
                                            'placeholder="Optional" name="harga_setelah_negosiasi" />'+
                                    '</div>'+
                                    '<div class="total_harga_setelah_negosiasi"></div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+keterangan+'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 9) {
                let plan_evaluasi_FPP = data['plan_evaluasi_FPP'] == null ? "" : data['plan_evaluasi_FPP'];
                let lhp_no = data['lhp_no'] == null ? "" : data['lhp_no'];
                let lhp_tgl = data['lhp_tgl'] == null ? "" : data['lhp_tgl'];

                //create form
                let form = '<div class="row">'+
                            '<div class="col-md-12 col-12">'+
                                '<div class="form-group">'+
                                ' <label>Status</label>'+
                                    '<select class="select2 form-control status" name="status_pengadaan"'+
                                    ' aria-placeholder="silahkan pilih Status" >'+
                                        '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                        '<option value="4">On Going</option>'+
                                    ' <`option value="5">On Hold</>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="evaluasi_FPP" class="form-label">Plan Evaluasi FPP</label>'+
                                        '<input type="date" class="form-control" id="evaluasi_FPP" value="'+ plan_evaluasi_FPP +'"'+
                                            'placeholder="Optional" name="evaluasi_FPP" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="lhp_no" class="form-label">LHP No Dokumen</label>'+
                                        '<input type="text" class="form-control" id="lhp_no" value="'+ lhp_no +'"'+
                                            'placeholder="Optional" name="lhp_no" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="lhp_tgl" class="form-label">LHP No Tanggal</label>'+
                                        '<input type="date" class="form-control" id="lhp_tgl" value="'+ lhp_tgl +'"'+
                                            'placeholder="Optional" name="lhp_tgl" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+keterangan+'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 10) {
                let plan_penunjukan_pemenang = data['plan_penunjukan_pemenang'] == null ? "" : data['plan_penunjukan_pemenang'];
                //create form
                let form = '<div class="row">'+
                    '<div class="col-md-12 col-12">'+
                                '<div class="form-group">'+
                                ' <label>Status</label>'+
                                    '<select class="select2 form-control status" name="status_pengadaan"'+
                                    ' aria-placeholder="silahkan pilih Status" >'+
                                        '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                        '<option value="4">On Going</option>'+
                                    ' <option value="5">On Hold</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="penunjukan_pemenang" class="form-label">Plan Pengumuman Pemenang</label>'+
                                        '<input type="date" class="form-control" id="penunjukan_pemenang" value="'+ plan_penunjukan_pemenang +'"'+
                                            'placeholder="Optional" name="penunjukan_pemenang" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+keterangan+'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 11) {
                let plan_po_plan = data['plan_po_plan'] == null ? "" : data['plan_po_plan'];
                let nama_penyedia = data['nama_penyedia'] == null ? "" : data['nama_penyedia'];
                //create form
                let form = '<div class="row">'+
                            '<div class="col-md-12 col-12">'+
                                '<div class="form-group">'+
                                ' <label>Status</label>'+
                                    '<select class="select2 form-control status" name="status_pengadaan"'+
                                    ' aria-placeholder="silahkan pilih Status" >'+
                                        '<option value="'+data['id_status']+'">'+data['status']+'</option>'+
                                        '<option value="4">On Going</option>'+
                                    ' <option value="5">On Hold</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                                '<div class="col-md-6 col-6">'+
                                    '<div class="form-group">'+
                                        '<label for="po_plan" class="form-label">Plan PO</label>'+
                                        '<input type="date" class="form-control" id="po_plan" value="'+ plan_po_plan +'"'+
                                            'placeholder="Optional" name="po_plan" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-6 col-6">'+
                                    '<div class="form-group">'+
                                        '<label for="nama_penyedia" class="form-label">Penyedia Barang/Jasa</label>'+
                                        '<input type="Text" class="form-control" id="nama_penyedia" value="'+ nama_penyedia +'"'+
                                            'placeholder="Optional" name="nama_penyedia" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+keterangan+'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 12) {
                let plan_kontrak_plan = data['plan_kontrak_plan'] == null ? "" : data['plan_kontrak_plan'];
                let po_no = data['po_no'] == null ? "" : data['po_no'];
                let po_tgl = data['po_tgl'] == null ? "" : data['po_tgl'];

                //create form
                let form = '<div class="row">'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="kontrak_plan" class="form-label">Plan Kontrak</label>'+
                                        '<input type="date" class="form-control" id="kontrak_plan" value="'+ plan_kontrak_plan +'"'+
                                            'placeholder="Optional" name="kontrak_plan" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-6 col-6">'+
                                    '<div class="form-group">'+
                                        '<label for="po_no" class="form-label">PO No Dokumen</label>'+
                                        '<input type="text" class="form-control" id="po_no" value="'+ po_no +'"'+
                                            'placeholder="Optional" name="po_no" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-6 col-6">'+
                                    '<div class="form-group">'+
                                        '<label for="po_tgl" class="form-label">PO Tanggal</label>'+
                                        '<input type="date" class="form-control" id="po_tgl" value="'+ po_tgl +'"'+
                                            'placeholder="Optional" name="po_tgl" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+keterangan+'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if (data['lastUpdate']['id_tahapan'] == 13) {
                let kontrak_no = data['kontrak_no'] == null ? "" : data['kontrak_no'];
                let kontrak_tgl = data['kontrak_tgl'] == null ? "" : data['kontrak_tgl'];
                let kontrak_jenis = data['kontrak_jenis'] == null ? "" : data['kontrak_jenis'];
                let kontrak_masa = data['kontrak_masa'] == null ? "" : data['kontrak_masa'];
                //create form
                let form = '<div class="row">'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="kontrak_no" class="form-label">No Dokumen Kontrak</label>'+
                                        '<input type="text" class="form-control" id="kontrak_no" value="'+ kontrak_no +'"'+
                                            'placeholder="Optional" name="kontrak_no" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                       ' <label for="kontrak_tgl" class="form-label">Tanggal Kontrak</label>'+
                                        '<input type="date" class="form-control" id="kontrak_tgl" value="'+ kontrak_tgl +'"'+
                                            'placeholder="Optional" name="kontrak_tgl" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="kontrak_jenis" class="form-label">Jenis Kontrak</label>'+
                                        '<input type="text" class="form-control" id="kontrak_jenis" value="'+ kontrak_jenis +'"'+
                                            'placeholder="Optional" name="kontrak_jenis" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="kontrak_masa" class="form-label">Masa Kontrak</label>'+
                                        '<input type="date" class="form-control" id="kontrak_masa" value="'+ kontrak_masa +'"'+
                                            'placeholder="Optional" name="kontrak_masa" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+keterangan+'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.simpan').show(500);
                $('.lanjutkan').show(500);
                $('.form-tahapan').html(form);
            }else if(data['lastUpdate']['id_tahapan'] == 14){
                //create form
                let form = '';

                //funtion show and hide
                $('.simpan').hide(500);
                $('.lanjutkan').hide(500);
                $('.form-tahapan').html(form);
            }
        }).catch(err => console.log(err));

    //timeline
    fetch('../pengadaan-timeline/' + id + '', {
            method: 'get'
        })
        .then(response => response.json())
        .then(data => {
            let timeline = '';
            for (let i = 0; i < data.length; i++) {
                let badge = '';
                if (data[i]['id_tahapan'] == '1') {
                    badge = 'info||info';
                } else if (data[i]['id_tahapan'] == '2') {
                    badge = 'warning||warning';
                } else if (data[i]['id_tahapan'] == '3') {
                    badge = 'success||success';
                } else if (data[i]['id_tahapan'] == '4') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '5') {
                    badge = 'waring||waring';
                } else if (data[i]['id_tahapan'] == '6') {
                    badge = 'primary||primary';
                } else if (data[i]['id_tahapan'] == '7') {
                    badge = 'success||success';
                } else if (data[i]['id_tahapan'] == '8') {
                    badge = 'primary||primary';
                } else if (data[i]['id_tahapan'] == '9') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '10') {
                    badge = 'waring||waring';
                } else if (data[i]['id_tahapan'] == '11') {
                    badge = 'success||success';
                } else if (data[i]['id_tahapan'] == '12') {
                    badge = 'info||info';
                } else if (data[i]['id_tahapan'] == '13') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '14') {
                    badge = 'info||info';
                } else if (data[i]['id_tahapan'] == '15') {
                    badge = 'warning||warning';
                } else if (data[i]['id_tahapan'] == '16') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '17') {
                    badge = 'primary||primary';
                } else if (data[i]['id_tahapan'] == '18') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '19') {
                    badge = 'primary||primary';
                } else if (data[i]['id_tahapan'] == '20') {
                    badge = 'info||info';
                } else {
                    badge = 'secondary||secondary';
                }
                timeline += '<div class="collapse-margin" id="accordionExample">'+
                                '<div class="card">'+
                                    '<div class="card-header" id="heading'+i+'" data-toggle="collapse" role="button" data-target="#collapse'+i+'" aria-expanded="false" aria-controls="collapse'+i+'">'+
                                        ' <span class="lead collapse-title">'+
                                            '<span class="text-primary font-weight-bolder">'+ data[i]['tahapan_pengadaan'] +
                                            ' - <small class="badge badge-pill badge-light-warning">'+ data[i]['created_at'] +'';
                                            if (data[i]['status'] == 1) {
                                                timeline += '</small><small class="badge badge-pill badge-light-success">Done</small>';
                                            }else{
                                                timeline += '</small><small class="badge badge-pill badge-light-secondary">Proses</small>';
                                            }
                            timeline += '</span>'+
                                    '</div>'+
                                '</div>'+
                                '<div id="collapse'+ i +'" class="collapse" aria-labelledby="heading'+ i +'" data-parent="#accordionExample">'+
                                    '<div class="card-body">'+
                                        '<div class="row">'+
                                            '<div class="col-lg-12">'+
                                                '<div class="card">'+
                                                    '<div class="card-body" >'+
                                                        '<ul class="timeline">';
                                                            for (let x = 0; x < data[i]['log'].length; x++) {
                                                                timeline += '<li class="timeline-item">' +
                                                                                '<span class="timeline-point timeline-point-primary' + badge.split('||')[0] +
                                                                                ' timeline-point-indicator"></span>' +
                                                                                '<div class="timeline-event">' +
                                                                                    '<div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1 ">' +
                                                                                        '<h6 class="font-weight-bolder text-'+ badge.split('||')[0] +'">' + data[i]['log'][x]['title'] + '</h6>' +
                                                                                    '</div>' +
                                                                                '<div>' +
                                                                                    '<span class="badge badge-pill badge-light-' + badge.split('||')[1] + '">' + data[i]['log'][x]['created_at'] + ' </span>' +
                                                                                    '<span class="badge badge-pill badge-light-' + badge.split('||')[1] + '">' + data[i]['log'][x]['user_id'] + ' </span>' +
                                                                                '<hr>'+
                                                                                '<div class="rounded card-body" style="background-color: #417489; border: 1px solid; padding: 10px;box-shadow: 5px 9px;">'+
                                                                                    '<div class="row">'+
                                                                                        '<div class="col-lg-12">';
                                                                                            if (data[i]['log'][x]['keterangan'] != null) {
                                                                                                timeline += '<span class="font-weight-bolder" style="color: #D9D9D9">' + data[i]['log'][x]['keterangan'] + '</span>';
                                                                                            }else{
                                                                                                timeline += '<span></span>';
                                                                                            }
                                                                    timeline += '</div>'+
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>' +
                                                                '</div>' +
                                                            '</li>';
                                                            }
                                             timeline +='</ul>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
            }
            $('.timelien').html(timeline);
        }).catch(err => console.log(err));
}
$(document).ready(function() {
    $('.myTable').DataTable({
        searching: false,
        paging: false,
        scrollY: 400,
        "order": [
            [0, "desc"]
        ]
    });

    //-----------------------------------------------------------------
        //nilai_oe
        $(document).on('keyup', '#nilai_oe', function() {
            let nilaiOe = $('#nilai_oe').val();
            let total_oe = '';
            if (nilaiOe > 0) {
                let hasilPPN = (nilaiOe*0.11)+parseInt(nilaiOe);
                total_oe += '<hr><div class="alert alert-warning" role="alert">'+
                                '<h4 class="alert-heading">Total Nilai OE Dengan PPN</h4>'+
                                '<div class="alert-body">'+
                                    ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(nilaiOe) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                                '</div>'+
                            '</div>';
                $('.total_oe').html(total_oe);
            }else{
                total_oe += '';
                $('.total_oe').html(total_oe);
            }
            
        })
        let nilaiOe = $('#nilai_oe').val();
        let total_oe = '';
        if (nilaiOe > 0) {
            let hasilPPN = (nilaiOe*0.11)+parseInt(nilaiOe);
            total_oe += '<hr><div class="alert alert-warning" role="alert">'+
                            '<h4 class="alert-heading">Total Nilai OE Dengan PPN</h4>'+
                            '<div class="alert-body">'+
                                ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(nilaiOe) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                            '</div>'+
                        '</div>';
            $('.total_oe').html(total_oe);
        }else{
            total_oe += '';
            $('.total_oe').html(total_oe);
        }
        //-----------------------------------------------------------------
        //harga_penawaran
        $(document).on('keyup', '#harga_penawaran', function() {
            let harga_penawaran = $('#harga_penawaran').val();
            let total_harga_penawaran = '';
            if (harga_penawaran > 0) {
                let hasilPPN = (harga_penawaran*0.11)+parseInt(harga_penawaran);
                total_harga_penawaran += '<hr><div class="alert alert-warning" role="alert">'+
                                '<h4 class="alert-heading">Total Harga Penawaran Dengan PPN</h4>'+
                                '<div class="alert-body">'+
                                    ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_penawaran) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                                '</div>'+
                            '</div>';
                $('.total_harga_penawaran').html(total_harga_penawaran);
            }else{
                total_harga_penawaran += '';
                $('.total_harga_penawaran').html(total_harga_penawaran);
            }
            
        })
        let harga_penawaran = $('#harga_penawaran').val();
        let total_harga_penawaran = '';
        if (harga_penawaran > 0) {
            let hasilPPN = (harga_penawaran*0.11)+parseInt(harga_penawaran);
            total_harga_penawaran += '<hr><div class="alert alert-warning" role="alert">'+
                            '<h4 class="alert-heading">Total Harga Penawaran Dengan PPN</h4>'+
                            '<div class="alert-body">'+
                                ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_penawaran) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                            '</div>'+
                        '</div>';
            $('.total_harga_penawaran').html(total_harga_penawaran);
        }else{
            total_harga_penawaran += '';
            $('.total_harga_penawaran').html(total_harga_penawaran);
        }
        //harga_setelah_negosiasi
        $(document).on('keyup', '#harga_setelah_negosiasi', function() {
            let harga_setelah_negosiasi = $('#harga_setelah_negosiasi').val();
            let total_harga_setelah_negosiasi = '';
            if (harga_setelah_negosiasi > 0) {
                let hasilPPN = (harga_setelah_negosiasi*0.11)+parseInt(harga_setelah_negosiasi);
                total_harga_setelah_negosiasi += '<hr><div class="alert alert-warning" role="alert">'+
                                '<h4 class="alert-heading">Total Harga Setelah Negoisasi Dengan PPN</h4>'+
                                '<div class="alert-body">'+
                                    ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_setelah_negosiasi) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                                '</div>'+
                            '</div>';
                $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
            }else{
                total_harga_setelah_negosiasi += '';
                $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
            }
            
        })
        let harga_setelah_negosiasi = $('#harga_setelah_negosiasi').val();
        let total_harga_setelah_negosiasi = '';
        if (harga_setelah_negosiasi > 0) {
            let hasilPPN = (harga_setelah_negosiasi*0.11)+parseInt(harga_setelah_negosiasi);
            total_harga_setelah_negosiasi += '<hr><div class="alert alert-warning" role="alert">'+
                            '<h4 class="alert-heading">Total Harga Setelah Negoisasi Dengan PPN</h4>'+
                            '<div class="alert-body">'+
                                ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_setelah_negosiasi) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                            '</div>'+
                        '</div>';
            $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
        }else{
            total_harga_setelah_negosiasi += '';
            $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
        }
});

</script>
@endsection