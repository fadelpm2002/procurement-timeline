@extends('layouts.layout2')
@section('title', 'Detail Pengadaan - Procurement')
@section('main_wrapper')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Pengadaan</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a>
                        <li class="breadcrumb-item"><a href="{{route('pengadaan-index-timeline')}}">Pengadaan</a></li>
                        <li class="breadcrumb-item active">Detail Pengadaan</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('main_content')
@if (session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@endif
<section class="invoice-preview-wrapper">
    <div class="row invoice-preview">
        <!-- Invoice -->
        <div class="col-xl-9 col-md-8 col-12">
            <div class="card invoice-preview-card">
                <div class="card-body invoice-padding pb-0">
                    <!-- Header starts -->
                    <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                        <div>
                            <div class="logo-wrapper">
                                <h3 class="text-primary invoice-logo font-weight-bolder">{{$data['judul']}}</h3>
                            </div>
                            <p class="card-text mb-0 font-weight-bolder">Kategori : </p>{{$data['kategori']}}
                            <p class="card-text mb-0 font-weight-bolder">Tender Ke {{$data['tender']}}</p>
                            <p class="card-text mb-0 font-weight-bolder">Jenis Anggaran : </p>
                            {{$data['jenis_anggaran']}} - {{$data['tahun_anggaran']}}
                            <p class="card-text mb-0 font-weight-bolder">Sub.Bidang : </p>
                            @for ($i = 0; $i < count($data['sub_bidang']); $i++)
                                @php
                                    $no = $i + 1;
                                @endphp
                                @if (count($data['sub_bidang']) == $no)
                                    {{$data['sub_bidang'][$i]}}
                                @else
                                    {{$data['sub_bidang'][$i]}}<br>
                                @endif
                            @endfor
                            <p class="card-text mb-0 font-weight-bolder">No.PR : </p>{{$data['no_pr']}}
                            <p class="card-text mb-0 font-weight-bolder">DP3 diterima lengkap : </p>
                            {{$data['dp3_diterima_lengkap'] == null ? "" : Carbon\Carbon::parse($data['dp3_diterima_lengkap'])->format('d-m-Y') }}
                            <p class="card-text mb-0 font-weight-bolder">Penyedia Barang/Jasa : </p>
                            {{$data['nama_penyedia']}}
                        </div>
                        <div class="mt-md-0 mt-2">
                            <h4 class="invoice-title">
                                <span class="invoice-number">{{$data['created_at']}}</span>
                                <br>
                                <span class="badge badge-light-warning">{{$data['status']}}</span>
                            </h4>
                            <div class="invoice-date-wrapper">
                                <p class="card-text mb-0 font-weight-bolder">Jenis Pengadaan:</p>
                                {{$data['jenis_pengadaan']}}
                                <p class="card-text mb-0 font-weight-bolder">Fungsi Peminta Pengadaan:</p>
                                {{$data['fpp']}}
                                <p class="card-text mb-0 font-weight-bolder">Metode Pengadaan:</p>
                                {{$data['metode_pengadaan']}}
                                <p class="card-text mb-0 font-weight-bolder">PIC:</p>
                                
                                @for ($i = 0; $i < count($data['pengadaan_pic']); $i++)
                                    @php
                                        $no = $i + 1;
                                    @endphp
                                    @if (count($data['pengadaan_pic']) == $no)
                                        {{$data['pengadaan_pic'][$i]}}
                                    @else
                                        {{$data['pengadaan_pic'][$i]}},
                                    @endif
                                @endfor
                                <p class="card-text mb-0 font-weight-bolder">Tahapan:</p>
                                <span class="text-warning">{{$data['lastUpdate']['tahapan']}}</span>
                            </div>
                        </div>
                    </div>
                    <!-- Header ends -->
                </div>
                {{-- @if (count($data['items']) > 0) --}}
                <hr class="invoice-spacing" />
                <div class="row ">
                    <div class="col-12 ">
                        <div class="card ">
                            <div class="card-body">
                                <div class="table-responsive">
                                    @php
                                        $nilai = 0;
                                        $nilaiEO = $data['nilai_oe'] == null ? 0 : $data['nilai_oe'];
                                        $harga_penawaran = $data['harga_penawaran'] == null ? 0 : $data['harga_penawaran'];
                                        $harga_setelah_negosiasi = $data['harga_setelah_negosiasi'] == null ? 0 : $data['harga_setelah_negosiasi'];

                                        if($nilaiEO-$harga_setelah_negosiasi > 0){
                                            $nilai = ($nilaiEO-$harga_setelah_negosiasi)/$nilaiEO;
                                        }
                                        $descCostSaving = '';
                                        if (round($nilai*100) > 0) {
                                            $descCostSaving = 'Sudah Dibawah OE';
                                        }elseif(round($nilai*100) < 0){
                                            $descCostSaving = 'Masih Diatas OE';
                                        }elseif(round($nilai*100) == 0){
                                            $descCostSaving = 'Sama Dengan OE';
                                        }
                                    @endphp
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                {{-- <th>Nilai OE<br>(sebelum PPN)</th> --}}
                                                <th>Harga Penawaran<br>(sebelum PPN)</th>
                                                <th>Harga Setelah<br>Negosiasi<br>(sebelum PPN)</th>
                                                <th>Cost Saving<br>(Hasil Nego THD OE)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                {{-- <td>Rp {{number_format($nilaiEO,0,',','.')}},-</td> --}}
                                                <td>Rp {{number_format($harga_penawaran,0,',','.')}},-</td>
                                                <td>Rp {{number_format($harga_setelah_negosiasi,0,',','.')}},-
                                                </td>
                                                {{-- <td>{{round($nilai*100)}}%</td> --}}
                                                <td>{{$descCostSaving}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr class="invoice-spacing" />
                        <div class="card ">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center" colspan="7">TKDN<br>(Base on Dok. Pengadaan)
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>Form A2<br>TKDN %</th>
                                                <th>Form A3<br>Nilai TKDN Barang (Rp)</th>
                                                <th>TKDN Barang (%)</th>
                                                <th>Form A4<br>Nilai TKDN Jasa (Rp)</th>
                                                <th>TKDN Jasa %</th>
                                                <th>Form A5<br>TKDN Barang & Jasa (Rp)</th>
                                                <th>TKDN Barang & Jasa (%)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $data['form_a2_tkdn'] }}%</td>
                                                {{-- <td>{{ $data['form_a2_tkdn'] }}%</td> --}}
                                                <td>Rp {{$data['form_a3_nilai_tkdn_barang'] == null ? "" :number_format($data['form_a3_nilai_tkdn_barang'],0,',','.')}},-
                                                </td>
                                                <td>{{ $data['tkdn_barang'] }}%</td>
                                                {{-- <td>{{ $data['tkdn_barang'] }}%</td> --}}
                                                <td>Rp {{$data['form_a4_nilai_tkdn_jasa'] == null ? "" :number_format($data['form_a4_nilai_tkdn_jasa'],0,',','.')}},-
                                                </td>
                                                <td>{{ $data['tkdn_jasa'] }}%</td>
                                                {{-- <td>{{ $data['tkdn_jasa'] }}%</td> --}}
                                                <td>Rp
                                                    {{$data['form_a5_nilai_tkdn_jasa_barang'] == null ? "" :number_format($data['form_a5_nilai_tkdn_jasa_barang'],0,',','.')}},-
                                                </td>
                                                <td>{{ $data['tkdn_jasa_barang'] }}%</td>
                                                {{-- <td>{{ $data['tkdn_jasa_barang'] }}%</td> --}}
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr class="invoice-spacing" />
                        <div class="card ">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Pra Tender</th>
                                                <th>BA Aanwijzing</th>
                                                <th>BA Aanwijzing <br>Lapangan</th>
                                                <th>BA Negosiasi</th>
                                                <th>LHP</th>
                                                <th>PO</th>
                                                <th>Kontrak</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>No.</td>
                                                <td>{{ $data['pre_tender_no_nr'] }}</td>
                                                <td>{{ $data['ba_aanwijzing_no'] }}</td>
                                                <td>{{ $data['ba_aanwijzing_lapangan_no'] }}</td>
                                                {{-- <td>{{ $data[''] }}</td> --}}
                                                <td>{{ $data['ba_negosiasi_no'] }}</td>
                                                <td>{{ $data['lhp_no'] }}</td>
                                                <td>{{ $data['po_no'] }}</td>
                                                <td>{{ $data['kontrak_no'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal</td>
                                                <td>{{$data['pre_tender_tanggal'] == null ? "" : Carbon\Carbon::parse($data['pre_tender_tanggal'])->format('d-m-Y') }}</td>
                                                <td>{{$data['ba_aanwijzing_tanggal'] == null ? "" : Carbon\Carbon::parse($data['ba_aanwijzing_tanggal'])->format('d-m-Y') }}</td>
                                                <td>{{$data['ba_aanwijzing_lapangan_tanggal'] == null ? "" : Carbon\Carbon::parse($data['ba_aanwijzing_lapangan_tanggal'])->format('d-m-Y') }}</td>
                                                {{-- <td>{{ $data[''] }}</td> --}}
                                                <td>{{$data['ba_negosiasi_tanggal'] == null ? " " : Carbon\Carbon::parse($data['ba_negosiasi_tanggal'])->format('d-m-Y')}}</td>
                                                <td>{{$data['lhp_tgl'] == null ? " " : Carbon\Carbon::parse( $data['lhp_tgl'])->format('d-m-Y') }}</td>
                                                <td>{{$data['po_tgl'] == null ? " " : Carbon\Carbon::parse( $data['po_tgl'])->format('d-m-Y') }}</td>
                                                <td>{{$data['kontrak_tgl'] == null ? " " : Carbon\Carbon::parse( $data['po_tgl'])->format('d-m-Y') }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr class="invoice-spacing" />
                        <div class="card ">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Pengumuman Tender</th>
                                                <th>Penunjukan Pemenang</th>
                                                <th>Jenis Kontrak</th>
                                                <th>masa Kontrak</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$data['pengumuman_tender'] == null ? "0" : Carbon\Carbon::parse($data['pengumuman_tender'])->format('d-m-Y')}}</td>
                                                <td>{{$data['penunjukan_pemenang'] == null ? "0": Carbon\Carbon::parse($data['penunjukan_pemenang'])->format('d-m-Y')}}</td>
                                                <td>{{$data['kontrak_jenis'] == null ? " " : $data['kontrak_jenis'] }}</td>
                                                <td>{{$data['kontrak_masa'] == null ? " " : Carbon\Carbon::parse( $data['kontrak_masa'])->format('d-m-Y') }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- @endif --}}
                <hr class="invoice-spacing" />
                @if ($data['keterangan_reject'] != null)
                <div class="card-body">
                    @if ($data['id_status'] == 10)
                    <h4 class="card-title">Keterangan Batal :</h4>
                    @elseif ($data['id_status'] == 9)
                    <h4 class="card-title">Keterangan Reject :</h4>
                    @endif
                    <p class="card-text">
                        {{$data['keterangan_reject']}}
                    </p>
                </div>
                <hr class="invoice-spacing" />
                @endif
                <div class="row">
                    {{-- <div class="col-12 ">
                        <div class="card ">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-4">
                                        <span class="text-center card-text mb-20 font-weight-bolder">Mengajukan</span>
                                        <br>
                                        <img class="img-fluid my-2"
                                            src="{{asset('templateV2')}}/app-assets/images/docs/{{$data['ttd_mengajukan']}}"
                                            alt="Card image cap" style="height: 100px" />
                                        <br>
                                        @if ($data['id_status'] == 1 )
                                        <button type="button" class="btn btn-icon btn-outline-secondary exportJR"
                                            data-toggle="modal" data-target="#importJR" id="">
                                            <i data-feather='stop-circle'></i>
                                        </button>
                                        @endif
                                        <hr>
                                        <span
                                            class="text-center card-text mb-20 font-weight-bolder">{{$data['nama_mengajukan']}}</span>
                                    </div>
                                    <div class="col-md-4 col-4">
                                        <span class="text-center card-text mb-20 font-weight-bolder">Mengetahui</span>
                                        <br>
                                        @if ($data['ttd_mengetahui'] == null && $data['id_status'] == 1 &&
                                        ($data['id_mengetahui'] == Auth::user()->id || Auth::user()->id == 1))
                                        <form action="/pengadaan-mengetahui/{{$data['id']}}" method="post"
                                            enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @method('PUT')
                                            <input type="hidden" value="{{$data['id']}}" name="id" />
                                            <button type="submit" class="btn btn-icon btn-outline-success">
                                                <i data-feather="check"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon btn-outline-danger"
                                                data-toggle="modal" data-target="#rejectDoc" id="">
                                                <i data-feather="x"></i>
                                            </button>
                                        </form>
                                        @elseif ($data['ttd_mengetahui'] != null)
                                        <img class="img-fluid my-2"
                                            src="{{asset('templateV2')}}/app-assets/images/docs/{{$data['ttd_mengetahui']}}"
                                            alt="Card image cap" style="height: 100px" />
                                        @else
                                        <br>
                                        <br>
                                        <br>
                                        @endif
                                        <hr>
                                        <span
                                            class="text-center card-text mb-20 font-weight-bolder">{{$data['nama_mengetahui']}}</span>
                                    </div>
                                    <div class="col-md-4 col-4">
                                        <span class="text-center card-text mb-20 font-weight-bolder">Menyetujui</span>
                                        <br>
                                        @if ($data['ttd_menyetujui'] == null && $data['id_status'] == 2 &&
                                        ($data['id_menyetujui'] == Auth::user()->id || Auth::user()->id == 1))
                                        <br>
                                        <br>
                                        <br>
                                        <form action="/pengadaan-menyetujui/{{$data['id']}}" method="post"
                                            enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @method('PUT')
                                            <input type="hidden" value="{{$data['id']}}" name="id" />
                                            <button type="submit" class="btn btn-icon btn-outline-success">
                                                <i data-feather="check"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon btn-outline-danger"
                                                data-toggle="modal" data-target="#rejectDoc" id="">
                                                <i data-feather="x"></i>
                                            </button>
                                        </form>
                                        @elseif ($data['ttd_menyetujui'] != null)
                                        <img class="img-fluid my-2"
                                            src="{{asset('templateV2')}}/app-assets/images/docs/{{$data['ttd_menyetujui']}}"
                                            alt="Card image cap" style="height: 100px" />

                                        @else
                                        <br>
                                        <br>
                                        <br>
                                        @endif
                                        <hr>
                                        <span
                                            class="text-center card-text mb-20 font-weight-bolder">{{$data['nama_menyetujui']}}</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <br>
                <br>
                <!-- Invoice Description ends -->
            </div>
        </div>
        <!-- /Invoice -->

        <!-- Invoice Actions -->
        <div class="col-xl-3 col-md-4 col-12 invoice-actions mt-md-0 mt-2">
            <div class="card">
                <div class="card-body">
                    <a class="btn btn-outline-info btn-block mb-75" data-toggle="modal" data-target="#editPengadaan">
                        Ubah Pengadaan
                    </a>
                    @if ($data['id_status'] < 7 )
                        <a class="btn btn-outline-primary btn-block mb-75" data-toggle="modal" data-target="#changeStatus">
                            Ubah Status
                        </a>
                        <a class="btn btn-outline-warning btn-block mb-75" data-toggle="modal" data-target="#editTahapan">
                            Ubah Tahapan
                        </a>
                        
                    @endif
                    @if ($data['lastUpdate']['id_tahapan'] > 10 && $data['lastUpdate']['id_tahapan'] < 14 && ($data['id_status'] != 7 || $data['id_status'] != 8 ))
                        <a class="btn btn-outline-danger btn-block mb-75" href="../pengadaan-export-evaluasi/{{$data['id']}}">
                            Ekspor Evaluasi
                        </a>
                        <a class="btn btn-outline-danger btn-block mb-75" data-toggle="modal" data-target="#evaluasiPekerjaan">
                            Evaluasi Pekerjaan
                        </a>
                    @endif
                    {{-- @if ($data['lastUpdate']['id_tahapan'] < 14)
                        <a class="btn btn-outline-danger btn-block mb-75" data-toggle="modal" data-target="#updateTahapan">
                            Update Tahapan
                        </a>
                    @endif --}}
                </div>
            </div>
        </div>
        <!-- /Invoice Actions -->
    </div>
</section>
@include('pengadaan.modal.viewTimeline')
@include('pengadaan.modal.rejectDokumen')
@include('pengadaan.modal.batalDokumen')
@include('pengadaan.modal.changeStatus')
@include('pengadaan.modal.updateTahapan')
@include('pengadaan.modal.editPengadaan')
@include('pengadaan.modal.editTahapan')
@include('pengadaan.modal.evaluasi')
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
function setTimeline(id) {
    fetch('../pengadaan-timeline/' + id + '', {
            method: 'get'
        })
        .then(response => response.json())
        .then(data => {
            let timeline = '<ul class="timeline">';
            for (let i = 0; i < data.length; i++) {
                let badge = '';
                if (data[i]['id_tahapan'] == '1') {
                    badge = 'info||info';
                } else if (data[i]['id_tahapan'] == '2') {
                    badge = 'warning||warning';
                } else if (data[i]['id_tahapan'] == '3') {
                    badge = 'success||success';
                } else if (data[i]['id_tahapan'] == '4') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '5') {
                    badge = 'waring||waring';
                } else if (data[i]['id_tahapan'] == '6') {
                    badge = 'primary||primary';
                } else if (data[i]['id_tahapan'] == '7') {
                    badge = 'success||success';
                } else if (data[i]['id_tahapan'] == '8') {
                    badge = 'primary||primary';
                } else if (data[i]['id_tahapan'] == '9') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '10') {
                    badge = 'waring||waring';
                } else if (data[i]['id_tahapan'] == '11') {
                    badge = 'success||success';
                } else if (data[i]['id_tahapan'] == '12') {
                    badge = 'info||info';
                } else if (data[i]['id_tahapan'] == '13') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '14') {
                    badge = 'info||info';
                } else if (data[i]['id_tahapan'] == '15') {
                    badge = 'warning||warning';
                } else if (data[i]['id_tahapan'] == '16') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '17') {
                    badge = 'primary||primary';
                } else if (data[i]['id_tahapan'] == '18') {
                    badge = 'danger||danger';
                } else if (data[i]['id_tahapan'] == '19') {
                    badge = 'primary||primary';
                } else if (data[i]['id_tahapan'] == '20') {
                    badge = 'info||info';
                } else {
                    badge = 'secondary||secondary';
                }
                timeline += '<li class="timeline-item">' +
                    '<span class="timeline-point timeline-point-' + badge.split('||')[0] +
                    ' timeline-point-indicator"></span>' +
                    '<div class="timeline-event">' +
                    '<div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1 ">' +
                        '<h6 class="font-weight-bolder text-'+ badge.split('||')[0] +'">' + data[i]['tahapan_pengadaan'] + '</h6>' +
                    '</div>' +
                    '<div>' +
                    '<span class="badge badge-pill badge-light-' + badge.split('||')[1] + '">' + data[i][
                        'created_at'
                    ] + '</span>' +
                    '</div>' +
                    '<hr />' ;
                    if (data[i]['keterangan'] != null) {
                        timeline += '<p>' + data[i]['keterangan'] + '</p>';
                    }else{
                        timeline += '<p></p>';
                    }
                    '</div>' +
                    '</li>';
            }
            timeline += '<ul>';
            $('.timelien').html(timeline);
        }).catch(err => console.log(err));
}

</script>
@endsection