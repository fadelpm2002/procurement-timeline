<div class="modal fade" id="editTahapan" tabindex="-1" role="dialog" aria-labelledby="createPengadaanTitle"
    aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header " style="background-color: #27bd2f">
                <h5 class="modal-title text-white" id="createPengadaanTitle"> Edit Tahapan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-5">
                <form class="" action="{{route('pengadaan-edit-tahapan',$data['id'])}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @method('PUT')
                <input type="hidden" name="id_pengadaan" class="id_pengadaan" value="{{$data['id']}}">
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <label for="tahapan" class="form-label">Tahapan</label>
                                <select class="select2 form-control" name="tahapan" id="tahapan"
                                    aria-placeholder="silahkan pilih Tahapan" required>
                                    @php
                                        $riwayat = App\RiwayatPengadaan::where('tahapan_pengadaan', '<>',1)->where('id_pengadaan', $data['id'])->get();
                                    @endphp
                                    <option value=""></option>
                                    @foreach ($riwayat as $riwayats)
                                    @php
                                        $tahap = App\Tahapan::find($riwayats->tahapan_pengadaan);
                                    @endphp
                                        <option value="{{$riwayats->id}}||{{$tahap->id}}||{{$riwayats->keterangan}}">{{$tahap->nama}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
                    <hr>
                    <div class="form-tahapan"></div>
                <br><br><br>
                <br><br><br>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-primary" name="tombol" value="0">Simpan</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
                </form>
        </div>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
    // nilaiOe
    $(document).ready(function() {
    //-----------------------------------------------------------------
    //tahapan
    $(document).on('change', '#tahapan', function() {
            let splitData = $(this).val().split("||");
            let id_pengadaan = $('.id_pengadaan').val();
    //--------------------------------------------------------------------
    //pengadaan
    fetch('../pengadaan-detail-api/' + id_pengadaan + '', {
            method: 'get'
        })
        .then(response => response.json())
        .then(data => {

            let keterangan = data['lastUpdate']['keterangan'] == null ? "" : data['lastUpdate']['keterangan'];
            if (splitData[1] == 1) {
                //create form
                let form = '<div class="row">'+
                                '<div class="col-md-12 col-12">'+
                                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""name="keterangan">'+ splitData[2] +'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 2) {
                //create form
                let plan_pengumuman_tender = data['plan_pengumuman_tender'] == null ? "" : data['plan_pengumuman_tender'];

                let form = '<div class="row">'+
                    '<div class="col-md-12 col-12">'+
                        '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                        '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                        '<div class="form-group">'+
                        '<label for="pengumuman_tender" class="form-label">Plan Evaluasi Prakualifikasi</label>'+
                        '<div class="input-group input-group-merge mb-2">'+
                            '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="penyelesain_proses_pengadaan" value="'+ plan_pengumuman_tender +'" name="pengumuman_tender"/>'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-12 col-12">'+
                        '<div class="form-group">'+
                            '<label for="keterangan">Deskription</label>'+
                            '<textarea class="form-control" class="keterangan" rows="3" placeholder=""name="keterangan">'+ splitData[2] +'</textarea>'+
                        '</div>'+
                    '</div>'+
                '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 3) {
                //create form
                let plan_prebid_meeting = data['plan_prebid_meeting'] == null ? "" : data['plan_prebid_meeting'];

                let form = '<div class="row">'+
                                '<div class="col-md-12 col-12">'+
                                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                    '<div class="form-group">'+
                                    '<label for="penyelesain_proses_pengadaan" class="form-label">Plan Prebid Meeting </label>'+
                                    '<div class="input-group input-group-merge mb-2">'+
                                        '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="penyelesain_proses_pengadaan" value="'+ plan_prebid_meeting +'" name="penyelesain_proses_pengadaan"/>'+
                                        '<div class="input-group-append">'+
                                            '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                            '<label for="keterangan">Deskription</label>'+
                                            '<textarea class="form-control" class="keterangan" rows="3" placeholder=""name="keterangan">'+ splitData[2] +'</textarea>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 4) {
                //create form
                let plan_prebid_lapangan = data['plan_prebid_lapangan'] == null ? "" : data['plan_prebid_lapangan'];
                let pre_tender_no_nr = data['pre_tender_no_nr'] == null ? "" : data['pre_tender_no_nr'];
                let pre_tender_tanggal = data['pre_tender_tanggal'] == null ? "" : data['pre_tender_tanggal'];
                let ba_aanwijzing_no = data['ba_aanwijzing_no'] == null ? "" : data['ba_aanwijzing_no'];
                let ba_aanwijzing_tanggal = data['ba_aanwijzing_tanggal'] == null ? "" : data['ba_aanwijzing_tanggal'];
                let form_a2_tkdn = data['form_a2_tkdn'] == null ? "" : data['form_a2_tkdn'];
                let form_a3_nilai_tkdn_barang = data['form_a3_nilai_tkdn_barang'] == null ? "" : data['form_a3_nilai_tkdn_barang'];
                let tkdn_barang = data['tkdn_barang'] == null ? "" : data['tkdn_barang'];
                let form_a4_nilai_tkdn_jasa = data['form_a4_nilai_tkdn_jasa'] == null ? "" : data['form_a4_nilai_tkdn_jasa'];
                let tkdn_jasa = data['tkdn_jasa'] == null ? "" : data['tkdn_jasa'];
                let form_a5_nilai_tkdn_jasa_barang = data['form_a5_nilai_tkdn_jasa_barang'] == null ? "" : data['form_a5_nilai_tkdn_jasa_barang'];
                let tkdn_jasa_barang = data['tkdn_jasa_barang'] == null ? "" : data['tkdn_jasa_barang'];

                let form = '<h6>Pra Tender</h6>'+
                    '<hr>'+
                    '<div class="row">'+
                        '<div class="col-md-12 col-12">'+
                            '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                            '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                            '<div class="form-group">'+
                            '<label for="plan_prebid_lapangan" class="form-label">Plan Prebid Lapangan </label>'+
                                '<div class="input-group input-group-merge mb-2">'+
                                    '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="plan_prebid_lapangan" value="'+ plan_prebid_lapangan +'" name="plan_prebid_lapangan"/>'+
                                    '<div class="input-group-append">'+
                                        '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                    '</div>'+
                                '</div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-md-6 col-6">'+
                            '<div class="form-group">'+
                                '<label for="pre_tender_no_nr" class="form-label">No.NR</label>'+
                                '<input type="text" class="form-control" id="pre_tender_no_nr" value="'+pre_tender_no_nr+'"'+
                                    'placeholder="Optional" name="pre_tender_no_nr" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 col-6">'+
                            '<div class="form-group">'+
                                '<label for="pre_tender_tanggal" class="form-label">Tanggal</label>'+
                                '<input type="date" class="form-control" id="pre_tender_tanggal" value="'+pre_tender_tanggal+'"'+
                                    'placeholder="Optional" name="pre_tender_tanggal" />'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<h6>BA Aanwijzing</h6>'+
                   ' <hr>'+
                    '<div class="row">'+
                        '<div class="col-md-6 col-6">'+
                            '<div class="form-group">'+
                                '<label for="ba_aanwijzing_no" class="form-label">No</label>'+
                                '<input type="text" class="form-control" id="ba_aanwijzing_no" value="'+ba_aanwijzing_no+'"'+
                                    'placeholder="Optional" name="ba_aanwijzing_no" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 col-6">'+
                            '<div class="form-group">'+
                                '<label for="ba_aanwijzing_tanggal" class="form-label">Tanggal</label>'+
                                '<input type="date" class="form-control" id="ba_aanwijzing_tanggal" value="'+ba_aanwijzing_tanggal+'"'+
                                    'placeholder="Optional" name="ba_aanwijzing_tanggal" />'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<h6>TKDN</h6>'+
                    '<hr>'+
                    '<div class="row">'+
                        '<div class="col-md-3 col-3">'+
                            '<div class="form-group">'+
                                '<label for="form_a2_tkdn" class="form-label">Form A2</label>'+
                                '<input type="text" class="form-control" id="form_a2_tkdn" value="'+form_a2_tkdn+'"'+
                                    'placeholder="Optional" name="form_a2_tkdn" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-3 col-3">'+
                            '<div class="form-group">'+
                                '<label for="form_a3_nilai_tkdn_barang" class="form-label">Form A3 Barang</label>'+
                               ' <input type="text" class="form-control" id="form_a3_nilai_tkdn_barang" value="'+form_a3_nilai_tkdn_barang+'"'+
                                    'placeholder="Optional" name="form_a3_nilai_tkdn_barang" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-3 col-3">'+
                            '<div class="form-group">'+
                                '<label for="tkdn_barang" class="form-label">Barang</label>'+
                                '<input type="text" class="form-control" id="tkdn_barang" value="'+tkdn_barang+'"'+
                                   ' placeholder="Optional" name="tkdn_barang" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-3 col-3">'+
                            '<div class="form-group">'+
                                '<label for="form_a4_nilai_tkdn_jasa" class="form-label">Form A4 Jasa</label>'+
                                '<input type="text" class="form-control" id="form_a4_nilai_tkdn_jasa" value="'+form_a4_nilai_tkdn_jasa+'"'+
                                    'placeholder="Optional" name="form_a4_nilai_tkdn_jasa" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4 col-4">'+
                            '<div class="form-group">'+
                                '<label for="tkdn_jasa" class="form-label">Jasa</label>'+
                                '<input type="text" class="form-control" id="tkdn_jasa" value="'+tkdn_jasa+'"'+
                                    'placeholder="Optional" name="tkdn_jasa" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4 col-4">'+
                            '<div class="form-group">'+
                                '<label for="form_a5_nilai_tkdn_jasa_barang" class="form-label">Form A5 Barang & Jasa</label>'+
                                '<input type="text" class="form-control" id="form_a5_nilai_tkdn_jasa_barang" value="'+form_a5_nilai_tkdn_jasa_barang+'"'+
                                    'placeholder="Optional" name="form_a5_nilai_tkdn_jasa_barang" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4 col-4">'+
                            '<div class="form-group">'+
                                '<label for="tkdn_jasa_barang" class="form-label">Barang & Jasa</label>'+
                                '<input type="text" class="form-control" id="tkdn_jasa_barang" value="'+tkdn_jasa_barang+'"'+
                                   ' placeholder="Optional" name="tkdn_jasa_barang" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-12 col-12">'+
                            '<div class="form-group">'+
                                    '<label for="keterangan">Deskription</label>'+
                                    '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                        'name="keterangan">'+ splitData[2] +'</textarea>'
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 5) {
                let plan_pemasukan_penawaran = data['plan_pemasukan_penawaran'] == null ? "" : data['plan_pemasukan_penawaran'];
                let ba_aanwijzing_lapangan_no = data['ba_aanwijzing_lapangan_no'] == null ? "" : data['ba_aanwijzing_lapangan_no'];
                let ba_aanwijzing_lapangan_tanggal = data['ba_aanwijzing_lapangan_tanggal'] == null ? "" : data['ba_aanwijzing_lapangan_tanggal'];

                //create form
                let form = '<h6>BA Aanwijzing Lapangan</h6>'+
                            '<hr>'+
                            '<div class="row">'+
                                '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                                '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                               ' <div class="col-md-4 col-4">'+
                                '<div class="form-group">'+
                                    '<label for="pemasukan_penawaran" class="form-label">Plan Pemasukan Penawaran</label>'+
                                    '<div class="input-group input-group-merge mb-2">'+
                                        '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="pemasukan_penawaran" value="'+ plan_pemasukan_penawaran +'" name="pemasukan_penawaran"/>'+
                                        '<div class="input-group-append">'+
                                            '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '</div>'+
                               ' <div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="ba_aanwijzing_lapangan_no" class="form-label">No</label>'+
                                        '<input type="text" class="form-control" id="ba_aanwijzing_lapangan_no" value="'+ ba_aanwijzing_lapangan_no +'"'+
                                            'placeholder="Optional" name="ba_aanwijzing_lapangan_no" />'+
                                    '</div>'+
                                '</div>'+
                               ' <div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="ba_aanwijzing_lapangan_tanggal" class="form-label">Tanggal</label>'+
                                        '<input type="date" class="form-control" id="ba_aanwijzing_lapangan_tanggal" value="'+ ba_aanwijzing_lapangan_tanggal +'"'+
                                            'placeholder="Optional" name="ba_aanwijzing_lapangan_tanggal" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+ splitData[2] +'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 6) {
                let plan_evaluasi_sampul = data['plan_evaluasi_sampul'] == null ? "" : data['plan_evaluasi_sampul'];
                let nilai_oe = data['nilai_oe'] == null ? "" : data['nilai_oe'];
                let harga_penawaran = data['harga_penawaran'] == null ? "" : data['harga_penawaran'];
                //create form
                let form = '<div class="row">'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                    '<label for="evaluasi_sampul" class="form-label">Plan Evaluasi Sampul</label>'+
                                        '<div class="input-group input-group-merge mb-2">'+
                                            '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="evaluasi_sampul" value="'+ plan_evaluasi_sampul +'" name="evaluasi_sampul"/>'+
                                        '<div class="input-group-append">'+
                                            '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '</div>'+
                                    '<div class="col-md-4 col-4">'+
                                        '<div class="form-group">'+
                                            '<label for="nilai_oe" class="form-label">Nilai OE(sebelum ppn)</label>'+
                                           ' <input type="number" class="form-control" id="nilai_oe" value="'+ nilai_oe +'"'+
                                               ' placeholder="Optional" name="nilai_oe" />'+
                                            '<div class="total_oe"></div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-4 col-4">'+
                                        '<div class="form-group">'+
                                            '<label for="harga_penawaran" class="form-label">Harga Penawaran (sebelum PPN)</label>'+
                                            '<input type="number" class="form-control" id="harga_penawaran" value="'+ harga_penawaran +'"'+
                                               ' placeholder="Optional" name="harga_penawaran" />'+
                                            '<div class="total_harga_penawaran"></div>'+
                                        '</div>'+
                                    '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+ splitData[2] +'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 7) {
                let plan_negosiasi = data['plan_negosiasi'] == null ? "" : data['plan_negosiasi'];
                //create form
                let form = '<div class="row">'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                    '<label for="negosiasi" class="form-label">Plan Negosiasi</label>'+
                                    '<div class="input-group input-group-merge mb-2">'+
                                        '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="negosiasi" value="'+ plan_negosiasi +'" name="negosiasi"/>'+
                                        '<div class="input-group-append">'+
                                            '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                        '</div>'+
                                    '</div>'+
                                ' </div>'+
                               ' </div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+ splitData[2] +'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 8) {
                let lhp_plan = data['plan_lhp_plan'] == null ? "" : data['plan_lhp_plan'];
                let harga_setelah_negosiasi = data['harga_setelah_negosiasi'] == null ? "" : data['harga_setelah_negosiasi'];
                let ba_negosiasi_no = data['ba_negosiasi_no'] == null ? "" : data['ba_negosiasi_no'];
                let ba_negosiasi_tanggal = data['ba_negosiasi_tanggal'] == null ? "" : data['ba_negosiasi_tanggal'];
                //create form
                let form = '<div class="row">'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                    '<label for="lhp_plan" class="form-label">Plan LHP</label>'+
                                    '<div class="input-group input-group-merge mb-2">'+
                                        '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="lhp_plan" value="'+ lhp_plan +'" name="lhp_plan"/>'+
                                        '<div class="input-group-append">'+
                                            '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '</div>'+
                                ' <div class="col-md-4 col-4">'+
                                '<div class="form-group">'+
                                    '<label for="ba_negosiasi_no" class="form-label">No</label>'+
                                    '<input type="text" class="form-control" id="ba_negosiasi_no" value="'+ ba_negosiasi_no +'"'+
                                        'placeholder="Optional" name="ba_negosiasi_no" />'+
                                '</div>'+
                            '</div>'+
                            ' <div class="col-md-4 col-4">'+
                                '<div class="form-group">'+
                                    '<label for="ba_negosiasi_tanggal" class="form-label">Tanggal</label>'+
                                    '<input type="date" class="form-control" id="ba_negosiasi_tanggal" value="'+ ba_negosiasi_tanggal +'"'+
                                        'placeholder="Optional" name="ba_negosiasi_tanggal" />'+
                                '</div>'+
                            '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="harga_setelah_negosiasi" class="form-label">Harga setelah Negosiasi(sebelum PPN)</label>'+
                                        '<input type="number" class="form-control" id="harga_setelah_negosiasi" value="'+ harga_setelah_negosiasi +'"'+
                                            'placeholder="Optional" name="harga_setelah_negosiasi" />'+
                                    '</div>'+
                                    '<div class="total_harga_setelah_negosiasi"></div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+ splitData[2] +'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 9) {
                let plan_evaluasi_FPP = data['plan_evaluasi_FPP'] == null ? "" : data['plan_evaluasi_FPP'];
                let lhp_no = data['lhp_no'] == null ? "" : data['lhp_no'];
                let lhp_tgl = data['lhp_tgl'] == null ? "" : data['lhp_tgl'];

                //create form
                let form = '<div class="row">'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                    '<label for="evaluasi_FPP" class="form-label">Plan Pengumuman Pemenang</label>'+
                                    '<div class="input-group input-group-merge mb-2">'+
                                        '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="evaluasi_FPP" value="'+ plan_evaluasi_FPP +'" name="evaluasi_FPP"/>'+
                                        '<div class="input-group-append">'+
                                            '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                        '</div>'+
                                    '</div>'+
                                        
                                '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="lhp_no" class="form-label">LHP No Dokumen</label>'+
                                        '<input type="text" class="form-control" id="lhp_no" value="'+ lhp_no +'"'+
                                            'placeholder="Optional" name="lhp_no" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="lhp_tgl" class="form-label">LHP No Tanggal</label>'+
                                        '<input type="date" class="form-control" id="lhp_tgl" value="'+ lhp_tgl +'"'+
                                            'placeholder="Optional" name="lhp_tgl" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+ splitData[2] +'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 10) {
                let plan_penunjukan_pemenang = data['plan_penunjukan_pemenang'] == null ? "" : data['plan_penunjukan_pemenang'];
                let nama_penyedia = data['nama_penyedia'] == null ? "" : data['nama_penyedia'];
                //create form
                let form = '<div class="row">'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<div class="form-group">'+
                                            '<label for="penunjukan_pemenang" class="form-label">plan PO & Kontrak</label>'+
                                            '<div class="input-group input-group-merge mb-2">'+
                                                '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="penunjukan_pemenang" value="'+ plan_penunjukan_pemenang +'" name="penunjukan_pemenang"/>'+
                                                '<div class="input-group-append">'+
                                                    '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                                '</div>'+
                                            '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="nama_penyedia" class="form-label">Penyedia Barang/Jasa</label>'+
                                        '<input type="Text" class="form-control" id="nama_penyedia" value="'+ nama_penyedia +'"'+
                                            'placeholder="Optional" name="nama_penyedia" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+ splitData[2] +'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if (splitData[1] == 11) {
                let plan_po_plan = data['plan_po_plan'] == null ? "" : data['plan_po_plan'];
                
                //create form
                let form = '<div class="row">'+
                    // '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                    // '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                // '<div class="col-md-12 col-12">'+
                                //     '<div class="form-group">'+
                                //         '<label for="po_plan" class="form-label">Plan PO</label>'+
                                //         '<div class="input-group input-group-merge mb-2">'+
                                //             '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="po_plan" value="'+ plan_po_plan +'" name="po_plan"/>'+
                                //             '<div class="input-group-append">'+
                                //                 '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                //             '</div>'+
                                //         '</div>'+
                                //     '</div>'+
                                // '</div>'+
                                // '<div class="col-md-6 col-6">'+
                                //     '<div class="form-group">'+
                                //         '<label for="nama_penyedia" class="form-label">Penyedia Barang/Jasa</label>'+
                                //         '<input type="Text" class="form-control" id="nama_penyedia" value="'+ nama_penyedia +'"'+
                                //             'placeholder="Optional" name="nama_penyedia" />'+
                                //     '</div>'+
                                // '</div>'+
                            //     '<div class="col-md-12 col-12">'+
                            //         '<div class="form-group">'+
                            //             '<label for="keterangan">Deskription</label>'+
                            //             '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                            //                 'name="keterangan">'+ splitData[2] +'</textarea>'+
                            //        ' </div>'+
                            //    ' </div>'+
                            '</div>';

                //funtion show and hide
            //     $('.form-tahapan').html(form);
            // }else if (splitData[1] == 12) {
                let plan_kontrak_plan = data['plan_kontrak_plan'] == null ? "" : data['plan_kontrak_plan'];
                let po_no = data['po_no'] == null ? "" : data['po_no'];
                let po_tgl = data['po_tgl'] == null ? "" : data['po_tgl'];

                //create form
                form += '<div class="row">'+
                    // '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                    // '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                // '<div class="col-md-12 col-12">'+
                                //     '<div class="form-group">'+
                                //     '<label for="kontrak_plan" class="form-label">Plan Kontrak</label>'+
                                //     '<div class="input-group input-group-merge mb-2">'+
                                //         '<input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="kontrak_plan" value="'+ plan_kontrak_plan +'" name="kontrak_plan"/>'+
                                //         '<div class="input-group-append">'+
                                //             '<span class="input-group-text" id="basic-addon6">Hari</span>'+
                                //         '</div>'+
                                //     '</div>'+
                                // '</div>'+
                                // '</div>'+
                                '<div class="col-md-6 col-6">'+
                                    '<div class="form-group">'+
                                        '<label for="po_no" class="form-label">PO No Dokumen</label>'+
                                        '<input type="text" class="form-control" id="po_no" value="'+ po_no +'"'+
                                            'placeholder="Optional" name="po_no" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-6 col-6">'+
                                    '<div class="form-group">'+
                                        '<label for="po_tgl" class="form-label">PO Tanggal</label>'+
                                        '<input type="date" class="form-control" id="po_tgl" value="'+ po_tgl +'"'+
                                            'placeholder="Optional" name="po_tgl" />'+
                                    '</div>'+
                                '</div>'+
                            //     '<div class="col-md-12 col-12">'+
                            //         '<div class="form-group">'+
                            //             '<label for="keterangan">Deskription</label>'+
                            //             '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                            //                 'name="keterangan">'+ splitData[2] +'</textarea>'+
                            //        ' </div>'+
                            //    ' </div>'+
                            '</div>';

                //funtion show and hide
            //     $('.form-tahapan').html(form);
            // }else if (splitData[1] == 13) {
                let kontrak_no = data['kontrak_no'] == null ? "" : data['kontrak_no'];
                let kontrak_tgl = data['kontrak_tgl'] == null ? "" : data['kontrak_tgl'];
                let kontrak_jenis = data['kontrak_jenis'] == null ? "" : data['kontrak_jenis'];
                let kontrak_masa = data['kontrak_masa'] == null ? "" : data['kontrak_masa'];
                //create form
                form += '<div class="row">'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[1] +'" placeholder="Optional" name="tahapan_id" />'+
                    '<input type="hidden" class="form-control" id="id_riwayat" value="'+ splitData[0] +'" placeholder="Optional" name="id_riwayat" />'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="kontrak_no" class="form-label">No Dokumen Kontrak</label>'+
                                        '<input type="text" class="form-control" id="kontrak_no" value="'+ kontrak_no +'"'+
                                            'placeholder="Optional" name="kontrak_no" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                       ' <label for="kontrak_tgl" class="form-label">Tanggal Kontrak</label>'+
                                        '<input type="date" class="form-control" id="kontrak_tgl" value="'+ kontrak_tgl +'"'+
                                            'placeholder="Optional" name="kontrak_tgl" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="kontrak_jenis" class="form-label">Jenis Kontrak</label>'+
                                        '<input type="text" class="form-control" id="kontrak_jenis" value="'+ kontrak_jenis +'"'+
                                            'placeholder="Optional" name="kontrak_jenis" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4 col-4">'+
                                    '<div class="form-group">'+
                                        '<label for="kontrak_masa" class="form-label">Masa Kontrak</label>'+
                                        '<input type="date" class="form-control" id="kontrak_masa" value="'+ kontrak_masa +'"'+
                                            'placeholder="Optional" name="kontrak_masa" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-12">'+
                                    '<div class="form-group">'+
                                        '<label for="keterangan">Deskription</label>'+
                                        '<textarea class="form-control" class="keterangan" rows="3" placeholder=""'+
                                            'name="keterangan">'+ splitData[2] +'</textarea>'+
                                   ' </div>'+
                               ' </div>'+
                            '</div>';

                //funtion show and hide
                $('.form-tahapan').html(form);
            }else if(splitData[1] == 14){
                //create form
                let form = '';

                //funtion show and hide
                $('.simpan').hide(500);
                $('.lanjutkan').hide(500);
                $('.form-tahapan').html(form);
            }
        }).catch(err => console.log(err));
        
        })
        //-----------------------------------------------------------------
        //nilai_oe
        $(document).on('keyup', '#nilai_oe', function() {
            let nilaiOe = $('#nilai_oe').val();
            let total_oe = '';
            if (nilaiOe > 0) {
                let hasilPPN = (nilaiOe*0.11)+parseInt(nilaiOe);
                total_oe += '<hr><div class="alert alert-warning" role="alert">'+
                                '<h4 class="alert-heading">Total Nilai OE Dengan PPN</h4>'+
                                '<div class="alert-body">'+
                                    ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(nilaiOe) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                                '</div>'+
                            '</div>';
                $('.total_oe').html(total_oe);
            }else{
                total_oe += '';
                $('.total_oe').html(total_oe);
            }
            
        })
        let nilaiOe = $('#nilai_oe').val();
        let total_oe = '';
        if (nilaiOe > 0) {
            let hasilPPN = (nilaiOe*0.11)+parseInt(nilaiOe);
            total_oe += '<hr><div class="alert alert-warning" role="alert">'+
                            '<h4 class="alert-heading">Total Nilai OE Dengan PPN</h4>'+
                            '<div class="alert-body">'+
                                ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(nilaiOe) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                            '</div>'+
                        '</div>';
            $('.total_oe').html(total_oe);
        }else{
            total_oe += '';
            $('.total_oe').html(total_oe);
        }
        //-----------------------------------------------------------------
        //harga_penawaran
        $(document).on('keyup', '#harga_penawaran', function() {
            let harga_penawaran = $('#harga_penawaran').val();
            let total_harga_penawaran = '';
            if (harga_penawaran > 0) {
                let hasilPPN = (harga_penawaran*0.11)+parseInt(harga_penawaran);
                total_harga_penawaran += '<hr><div class="alert alert-warning" role="alert">'+
                                '<h4 class="alert-heading">Total Harga Penawaran Dengan PPN</h4>'+
                                '<div class="alert-body">'+
                                    ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_penawaran) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                                '</div>'+
                            '</div>';
                $('.total_harga_penawaran').html(total_harga_penawaran);
            }else{
                total_harga_penawaran += '';
                $('.total_harga_penawaran').html(total_harga_penawaran);
            }
            
        })
        let harga_penawaran = $('#harga_penawaran').val();
        let total_harga_penawaran = '';
        if (harga_penawaran > 0) {
            let hasilPPN = (harga_penawaran*0.11)+parseInt(harga_penawaran);
            total_harga_penawaran += '<hr><div class="alert alert-warning" role="alert">'+
                            '<h4 class="alert-heading">Total Harga Penawaran Dengan PPN</h4>'+
                            '<div class="alert-body">'+
                                ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_penawaran) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                            '</div>'+
                        '</div>';
            $('.total_harga_penawaran').html(total_harga_penawaran);
        }else{
            total_harga_penawaran += '';
            $('.total_harga_penawaran').html(total_harga_penawaran);
        }
        //------------------------------------------------------
        //harga_setelah_negosiasi
        $(document).on('keyup', '#harga_setelah_negosiasi', function() {
            let harga_setelah_negosiasi = $('#harga_setelah_negosiasi').val();
            let total_harga_setelah_negosiasi = '';
            if (harga_setelah_negosiasi > 0) {
                let hasilPPN = (harga_setelah_negosiasi*0.11)+parseInt(harga_setelah_negosiasi);
                total_harga_setelah_negosiasi += '<hr><div class="alert alert-warning" role="alert">'+
                                '<h4 class="alert-heading">Total Harga Setelah Negoisasi Dengan PPN</h4>'+
                                '<div class="alert-body">'+
                                    ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_setelah_negosiasi) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                                '</div>'+
                            '</div>';
                $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
            }else{
                total_harga_setelah_negosiasi += '';
                $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
            }
            
        })
        let harga_setelah_negosiasi = $('#harga_setelah_negosiasi').val();
        let total_harga_setelah_negosiasi = '';
        if (harga_setelah_negosiasi > 0) {
            let hasilPPN = (harga_setelah_negosiasi*0.11)+parseInt(harga_setelah_negosiasi);
            total_harga_setelah_negosiasi += '<hr><div class="alert alert-warning" role="alert">'+
                            '<h4 class="alert-heading">Total Harga Setelah Negoisasi Dengan PPN</h4>'+
                            '<div class="alert-body">'+
                                ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_setelah_negosiasi) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                            '</div>'+
                        '</div>';
            $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
        }else{
            total_harga_setelah_negosiasi += '';
            $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
        }
    })
</script>