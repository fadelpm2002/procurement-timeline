<div class="modal fade" id="updateTahapan" tabindex="-1" role="dialog" aria-labelledby="createPengadaanTitle"
    aria-hidden="true">
    @if ($data['lastUpdate']['id_tahapan'] == '1')
        <div class="modal-dialog modal-dialog-scrollable" role="document">
    @else
        <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    @endif
        <div class="modal-content">
            <div class="modal-header " style="background-color: #27bd2f">
                <h5 class="modal-title text-white" id="createPengadaanTitle"> Update Tahapan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-5">
                <form class="" action="{{route('pengadaan-update-tahapan',$data['id'])}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @method('PUT')
                    <input type="hidden" class="id" name="id" value="{{$data['id']}}">
                    {{-- <div class="row"> --}}
                    @if ($data['lastUpdate']['id_tahapan'] == '1')
                        <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                        <input type="hidden" class="" name="tahapan" value="1">
                        <div class="col-md-12">
                            <h2 class="text-center">Update <span class="font-weight-bolder text-primary">Tahapan</span></h2>
                            <br>
                            <h3>Apakah Anda Yakin Lanjutkan Ke Tahapan <span class="font-weight-bolder text-primary">DP3 Belum Lengkap</span> ?</h3>
                        </div>
                    @elseif ($data['lastUpdate']['id_tahapan'] == '2')
                        <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                        <input type="hidden" class="" name="tahapan" value="2">
                        <div class="col-md-12 col-12">
                            <div class="form-group">
                                <label for="pengumuman_tender" class="form-label">Pengumuman Tender</label>
                                <input type="date" class="form-control" id="pengumuman_tender" value="{{$data['plan_pengumuman_tender']}}"
                                    placeholder="Optional" name="pengumuman_tender" />
                            </div>
                        </div>
                        <div class="col-md-12 col-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="keterangan">Deskription</label>
                                    <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                        name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                </div>
                            </div>
                        </div>
                        @elseif ($data['lastUpdate']['id_tahapan'] == '3')
                        <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                        <input type="hidden" class="" name="tahapan" value="3">
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <label for="penyelesain_proses_pengadaan" class="form-label">Plan Prebid Meeting </label>
                                    {{-- <input type="number" class="form-control" id="penyelesain_proses_pengadaan" value="{{$data['plan_prebid_meeting']}}"
                                        placeholder="Optional" name="penyelesain_proses_pengadaan" /> --}}
                                    <div class="input-group input-group-merge mb-2">
                                        <input type="number" class="form-control" placeholder="durasi Hari" aria-label="durasi Hari" aria-describedby="basic-addon6" id="penyelesain_proses_pengadaan" value="{{$data['plan_prebid_meeting']}}"  name="penyelesain_proses_pengadaan"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon6">Hari</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="keterangan">Deskription</label>
                                        <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                            name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                    </div>
                                </div>
                            </div>
                        @elseif ($data['lastUpdate']['id_tahapan'] == '4')
                        
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="4">
                            <h6>Pra Tender</h6>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="plan_prebid_lapangan" class="form-label">Plan Prebid Lapangan </label>
                                        <input type="date" class="form-control" id="plan_prebid_lapangan" value="{{$data['plan_prebid_lapangan']}}"
                                            placeholder="Optional" name="plan_prebid_lapangan" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="pre_tender_no_nr" class="form-label">No.NR</label>
                                        <input type="text" class="form-control" id="pre_tender_no_nr" value="{{$data['pre_tender_no_nr']}}"
                                            placeholder="Optional" name="pre_tender_no_nr" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="pre_tender_tanggal" class="form-label">Tanggal</label>
                                        <input type="date" class="form-control" id="pre_tender_tanggal" value="{{$data['pre_tender_tanggal']}}"
                                            placeholder="Optional" name="pre_tender_tanggal" />
                                    </div>
                                </div>
                            </div>
                            <h6>BA Aanwijzing</h6>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="ba_aanwijzing_no" class="form-label">No</label>
                                        <input type="text" class="form-control" id="ba_aanwijzing_no" value="{{$data['ba_aanwijzing_no']}}"
                                            placeholder="Optional" name="ba_aanwijzing_no" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="ba_aanwijzing_tanggal" class="form-label">Tanggal</label>
                                        <input type="date" class="form-control" id="ba_aanwijzing_tanggal" value="{{$data['ba_aanwijzing_tanggal']}}"
                                            placeholder="Optional" name="ba_aanwijzing_tanggal" />
                                    </div>
                                </div>
                            </div>
                            <h6>TKDN</h6>
                            <hr>
                            <div class="row">
                                <div class="col-md-3 col-3">
                                    <div class="form-group">
                                        <label for="form_a2_tkdn" class="form-label">Form A2</label>
                                        <input type="text" class="form-control" id="form_a2_tkdn" value="{{$data['form_a2_tkdn']}}"
                                            placeholder="Optional" name="form_a2_tkdn" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-3">
                                    <div class="form-group">
                                        <label for="form_a3_nilai_tkdn_barang" class="form-label">Form A3 Barang</label>
                                        <input type="text" class="form-control" id="form_a3_nilai_tkdn_barang" value="{{$data['form_a3_nilai_tkdn_barang']}}"
                                            placeholder="Optional" name="form_a3_nilai_tkdn_barang" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-3">
                                    <div class="form-group">
                                        <label for="tkdn_barang" class="form-label">Barang</label>
                                        <input type="text" class="form-control" id="tkdn_barang" value="{{$data['tkdn_barang']}}"
                                            placeholder="Optional" name="tkdn_barang" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-3">
                                    <div class="form-group">
                                        <label for="form_a4_nilai_tkdn_jasa" class="form-label">Form A4 Jasa</label>
                                        <input type="text" class="form-control" id="form_a4_nilai_tkdn_jasa" value="{{$data['form_a4_nilai_tkdn_jasa']}}"
                                            placeholder="Optional" name="form_a4_nilai_tkdn_jasa" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-4">
                                    <div class="form-group">
                                        <label for="tkdn_jasa" class="form-label">Jasa</label>
                                        <input type="text" class="form-control" id="tkdn_jasa" value="{{$data['tkdn_jasa']}}"
                                            placeholder="Optional" name="tkdn_jasa" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-4">
                                    <div class="form-group">
                                        <label for="form_a5_nilai_tkdn_jasa_barang" class="form-label">Form A5 Barang & Jasa</label>
                                        <input type="text" class="form-control" id="form_a5_nilai_tkdn_jasa_barang" value="{{$data['form_a5_nilai_tkdn_jasa_barang']}}"
                                            placeholder="Optional" name="form_a5_nilai_tkdn_jasa_barang" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-4">
                                    <div class="form-group">
                                        <label for="tkdn_jasa_barang" class="form-label">Barang & Jasa</label>
                                        <input type="text" class="form-control" id="tkdn_jasa_barang" value="{{$data['tkdn_jasa_barang']}}"
                                            placeholder="Optional" name="tkdn_jasa_barang" />
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="keterangan">Deskription</label>
                                            <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                                name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif ($data['lastUpdate']['id_tahapan'] == '5')
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="5">
                            <h6>BA Aanwijzing Lapangan</h6>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4 col-4">
                                        <div class="form-group">
                                            <label for="pemasukan_penawaran" class="form-label">Plan Pemasukan Penawaran</label>
                                            <input type="date" class="form-control" id="pemasukan_penawaran" value="{{$data['plan_pemasukan_penawaran']}}"
                                                placeholder="Optional" name="pemasukan_penawaran" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-4">
                                        <div class="form-group">
                                            <label for="ba_aanwijzing_lapangan_no" class="form-label">No</label>
                                            <input type="text" class="form-control" id="ba_aanwijzing_lapangan_no" value="{{$data['ba_aanwijzing_lapangan_no']}}"
                                                placeholder="Optional" name="ba_aanwijzing_lapangan_no" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-4">
                                        <div class="form-group">
                                            <label for="ba_aanwijzing_lapangan_tanggal" class="form-label">Tanggal</label>
                                            <input type="date" class="form-control" id="ba_aanwijzing_lapangan_tanggal" value="{{$data['ba_aanwijzing_lapangan_tanggal']}}"
                                                placeholder="Optional" name="ba_aanwijzing_lapangan_tanggal" />
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="keterangan">Deskription</label>
                                                <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                                    name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @elseif ($data['lastUpdate']['id_tahapan'] == '6')
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="6">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="evaluasi_sampul" class="form-label">Plan Evaluasi Sampul</label>
                                        <input type="date" class="form-control" id="evaluasi_sampul" value="{{$data['plan_evaluasi_sampul']}}"
                                            placeholder="Optional" name="evaluasi_sampul" />
                                    </div>
                                </div>
                                    <div class="col-md-6 col-6">
                                        <div class="form-group">
                                            <label for="nilai_oe" class="form-label">Nilai OE(sebelum ppn)</label>
                                            <input type="number" class="form-control" id="nilai_oe" value="{{$data['nilai_oe']}}"
                                                placeholder="Optional" name="nilai_oe" />
                                            <div class="total_oe"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <div class="form-group">
                                            <label for="harga_penawaran" class="form-label">Harga Penawaran (sebelum PPN)</label>
                                            <input type="number" class="form-control" id="harga_penawaran" value="{{$data['harga_penawaran']}}"
                                                placeholder="Optional" name="harga_penawaran" />
                                            <div class="total_harga_penawaran"></div>
                                        </div>
                                    </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="keterangan">Deskription</label>
                                            <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                                name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif ($data['lastUpdate']['id_tahapan'] == '7')
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="7">
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <label for="negosiasi" class="form-label">Plan Negosiasi</label>
                                    <input type="date" class="form-control" id="negosiasi" value="{{$data['plan_negosiasi']}}"
                                        placeholder="Optional" name="negosiasi" />
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="keterangan">Deskription</label>
                                        <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                            name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                    </div>
                                </div>
                            </div>
                        @elseif ($data['lastUpdate']['id_tahapan'] == '8')
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="8">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="lhp_plan" class="form-label">Plan LHP</label>
                                        <input type="date" class="form-control" id="lhp_plan" value="{{$data['plan_lhp_plan']}}"
                                            placeholder="Optional" name="lhp_plan" />
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="ba_negosiasi_no" class="form-label">No</label>
                                        <input type="text" class="form-control" id="ba_negosiasi_no" value="{{$data['ba_negosiasi_no']}}"
                                            placeholder="Optional" name="ba_negosiasi_no" />
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="ba_negosiasi_tanggal" class="form-label">Tanggal</label>
                                        <input type="date" class="form-control" id="ba_negosiasi_tanggal" value="{{$data['ba_negosiasi_tanggal']}}"
                                            placeholder="Optional" name="ba_negosiasi_tanggal" />
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="harga_setelah_negosiasi" class="form-label">Harga setelah Negosiasi(sebelum PPN)</label>
                                        <input type="text" class="form-control" id="harga_setelah_negosiasi" value="{{$data['harga_setelah_negosiasi']}}"
                                            placeholder="Optional" name="harga_setelah_negosiasi" />
                                    </div>
                                    <div class="total_harga_setelah_negosiasi"></div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="keterangan">Deskription</label>
                                            <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                                name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif ($data['lastUpdate']['id_tahapan'] == '9')
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="9">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="evaluasi_FPP" class="form-label">Plan Evaluasi FPP</label>
                                        <input type="date" class="form-control" id="evaluasi_FPP" value="{{$data['plan_evaluasi_FPP']}}"
                                            placeholder="Optional" name="evaluasi_FPP" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="lhp_no" class="form-label">LHP No Dokumen</label>
                                        <input type="text" class="form-control" id="lhp_no" value="{{$data['lhp_no']}}"
                                            placeholder="Optional" name="lhp_no" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="lhp_tgl" class="form-label">LHP No Tanggal</label>
                                        <input type="date" class="form-control" id="lhp_tgl" value="{{$data['lhp_tgl']}}"
                                            placeholder="Optional" name="lhp_tgl" />
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="keterangan">Deskription</label>
                                            <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                                name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif ($data['lastUpdate']['id_tahapan'] == '10')
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="10">
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <label for="penunjukan_pemenang" class="form-label">Plan Pengumuman Pemenang</label>
                                    <input type="date" class="form-control" id="penunjukan_pemenang" value="{{$data['plan_penunjukan_pemenang']}}"
                                        placeholder="Optional" name="penunjukan_pemenang" />
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="keterangan">Deskription</label>
                                        <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                            name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                    </div>
                                </div>
                            </div>
                    {{-- </div> --}}
                        @elseif ($data['lastUpdate']['id_tahapan'] == '11')
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="11">
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <label for="po_plan" class="form-label">Plan PO</label>
                                    <input type="date" class="form-control" id="po_plan" value="{{$data['plan_po_plan']}}"
                                        placeholder="Optional" name="po_plan" />
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <label for="nama_penyedia" class="form-label">Penyedia Barang/Jasa</label>
                                    <input type="Text" class="form-control" id="nama_penyedia" value="{{$data['nama_penyedia']}}"
                                        placeholder="Optional" name="nama_penyedia" />
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="keterangan">Deskription</label>
                                        <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                            name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                    </div>
                                </div>
                            </div>
                        @elseif ($data['lastUpdate']['id_tahapan'] == '12')
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="12">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="kontrak_plan" class="form-label">Plan Kontrak</label>
                                        <input type="date" class="form-control" id="kontrak_plan" value="{{$data['plan_kontrak_plan']}}"
                                            placeholder="Optional" name="kontrak_plan" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="po_no" class="form-label">PO No Dokumen</label>
                                        <input type="text" class="form-control" id="po_no" value="{{$data['po_no']}}"
                                            placeholder="Optional" name="po_no" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="po_tgl" class="form-label">PO Tanggal</label>
                                        <input type="date" class="form-control" id="po_tgl" value="{{$data['po_tgl']}}"
                                            placeholder="Optional" name="po_tgl" />
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="keterangan">Deskription</label>
                                            <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                                name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @elseif ($data['lastUpdate']['id_tahapan'] == '13')
                            <input type="hidden" class="" name="id_tahapan" value="{{$data['lastUpdate']['id']}}">
                            <input type="hidden" class="" name="tahapan" value="13">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="kontrak_no" class="form-label">No Dokumen Kontrak</label>
                                        <input type="text" class="form-control" id="kontrak_no" value="{{$data['kontrak_no']}}"
                                            placeholder="Optional" name="kontrak_no" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-4">
                                    <div class="form-group">
                                        <label for="kontrak_tgl" class="form-label">Tanggal Kontrak</label>
                                        <input type="date" class="form-control" id="kontrak_tgl" value="{{$data['kontrak_tgl']}}"
                                            placeholder="Optional" name="kontrak_tgl" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-4">
                                    <div class="form-group">
                                        <label for="kontrak_jenis" class="form-label">Jenis Kontrak</label>
                                        <input type="text" class="form-control" id="kontrak_jenis" value="{{$data['kontrak_jenis']}}"
                                            placeholder="Optional" name="kontrak_jenis" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-4">
                                    <div class="form-group">
                                        <label for="kontrak_masa" class="form-label">Masa Kontrak</label>
                                        <input type="date" class="form-control" id="kontrak_masa" value="{{$data['kontrak_masa']}}"
                                            placeholder="Optional" name="kontrak_masa" />
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="keterangan">Deskription</label>
                                            <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                                name="keterangan">{{$data['lastUpdate']['keterangan']}}</textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endif
                    {{-- </div> --}}
                    @if ($data['lastUpdate']['id_tahapan'] == '1')
                    @else
                        <br>
                        <br>
                        <br>
                    @endif
            </div>
            <div class="modal-footer">
                @if ($data['lastUpdate']['id_tahapan'] == '1')
                    <button type="submit" class="btn btn-outline-success" name="tombol" value="1">Lanjutkan</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                @else
                    <button type="submit" class="btn btn-outline-primary" name="tombol" value="0">Simpan</button>
                    <button type="submit" class="btn btn-outline-success" name="tombol" value="1">Lanjutkan</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                @endif
            </div>
            </form>
        </div>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
    // nilaiOe
    $(document).ready(function() {
        //-----------------------------------------------------------------
        //nilai_oe
        $(document).on('keyup', '#nilai_oe', function() {
            let nilaiOe = $('#nilai_oe').val();
            let total_oe = '';
            if (nilaiOe > 0) {
                let hasilPPN = (nilaiOe*0.11)+parseInt(nilaiOe);
                total_oe += '<hr><div class="alert alert-warning" role="alert">'+
                                '<h4 class="alert-heading">Total Nilai OE Dengan PPN</h4>'+
                                '<div class="alert-body">'+
                                    ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(nilaiOe) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                                '</div>'+
                            '</div>';
                $('.total_oe').html(total_oe);
            }else{
                total_oe += '';
                $('.total_oe').html(total_oe);
            }
            
        })
        let nilaiOe = $('#nilai_oe').val();
        let total_oe = '';
        if (nilaiOe > 0) {
            let hasilPPN = (nilaiOe*0.11)+parseInt(nilaiOe);
            total_oe += '<hr><div class="alert alert-warning" role="alert">'+
                            '<h4 class="alert-heading">Total Nilai OE Dengan PPN</h4>'+
                            '<div class="alert-body">'+
                                ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(nilaiOe) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                            '</div>'+
                        '</div>';
            $('.total_oe').html(total_oe);
        }else{
            total_oe += '';
            $('.total_oe').html(total_oe);
        }
        //-----------------------------------------------------------------
        //harga_penawaran
        $(document).on('keyup', '#harga_penawaran', function() {
            let harga_penawaran = $('#harga_penawaran').val();
            let total_harga_penawaran = '';
            if (harga_penawaran > 0) {
                let hasilPPN = (harga_penawaran*0.11)+parseInt(harga_penawaran);
                total_harga_penawaran += '<hr><div class="alert alert-warning" role="alert">'+
                                '<h4 class="alert-heading">Total Harga Penawaran Dengan PPN</h4>'+
                                '<div class="alert-body">'+
                                    ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_penawaran) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                                '</div>'+
                            '</div>';
                $('.total_harga_penawaran').html(total_harga_penawaran);
            }else{
                total_harga_penawaran += '';
                $('.total_harga_penawaran').html(total_harga_penawaran);
            }
            
        })
        let harga_penawaran = $('#harga_penawaran').val();
        let total_harga_penawaran = '';
        if (harga_penawaran > 0) {
            let hasilPPN = (harga_penawaran*0.11)+parseInt(harga_penawaran);
            total_harga_penawaran += '<hr><div class="alert alert-warning" role="alert">'+
                            '<h4 class="alert-heading">Total Harga Penawaran Dengan PPN</h4>'+
                            '<div class="alert-body">'+
                                ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_penawaran) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                            '</div>'+
                        '</div>';
            $('.total_harga_penawaran').html(total_harga_penawaran);
        }else{
            total_harga_penawaran += '';
            $('.total_harga_penawaran').html(total_harga_penawaran);
        }
        //harga_setelah_negosiasi
        $(document).on('keyup', '#harga_setelah_negosiasi', function() {
            let harga_setelah_negosiasi = $('#harga_setelah_negosiasi').val();
            let total_harga_setelah_negosiasi = '';
            if (harga_setelah_negosiasi > 0) {
                let hasilPPN = (harga_setelah_negosiasi*0.11)+parseInt(harga_setelah_negosiasi);
                total_harga_setelah_negosiasi += '<hr><div class="alert alert-warning" role="alert">'+
                                '<h4 class="alert-heading">Total Harga Setelah Negoisasi Dengan PPN</h4>'+
                                '<div class="alert-body">'+
                                    ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_setelah_negosiasi) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                                '</div>'+
                            '</div>';
                $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
            }else{
                total_harga_setelah_negosiasi += '';
                $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
            }
            
        })
        let harga_setelah_negosiasi = $('#harga_setelah_negosiasi').val();
        let total_harga_setelah_negosiasi = '';
        if (harga_setelah_negosiasi > 0) {
            let hasilPPN = (harga_setelah_negosiasi*0.11)+parseInt(harga_setelah_negosiasi);
            total_harga_setelah_negosiasi += '<hr><div class="alert alert-warning" role="alert">'+
                            '<h4 class="alert-heading">Total Harga Setelah Negoisasi Dengan PPN</h4>'+
                            '<div class="alert-body">'+
                                ''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(harga_setelah_negosiasi) + ' x 11% = '+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(hasilPPN)+ ''
                            '</div>'+
                        '</div>';
            $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
        }else{
            total_harga_setelah_negosiasi += '';
            $('.total_harga_setelah_negosiasi').html(total_harga_setelah_negosiasi);
        }
    })
</script>