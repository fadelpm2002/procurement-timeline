<div class="modal fade" id="evaluasiPekerjaan" tabindex="-1" role="dialog" aria-labelledby="createPengadaanTitle"
    aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header " style="background-color: #27bd2f">
                <h5 class="modal-title text-white" id="createPengadaanTitle"> Evaluasi Tahapan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-5">
                <form class="" action="{{route('pengadaan-evaluasi')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                    @for ($i = 0; $i < count($data['evaluasi']); $i++)
                    <h6 class="text-primary font-weight-bolder"> {{$data['evaluasi'][$i]['tahapan']}} </h6>
                    <div class="row">
                        <input type="hidden" class="form-control" name="id[]" id="id" value="{{$data['evaluasi'][$i]['id']}}"/>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label for="no_pr" class="form-label">Plan (Hari Kerja)</label>
                                <input type="number" class="form-control" id="no_pr" value="{{$data['evaluasi'][$i]['plan_hk']}}"
                                    placeholder="Optional" name="plan_hk[]" />
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label for="no_pr" class="form-label">Aktual (Hari Kerja)</label>
                                <input type="number" class="form-control" id="no_pr" value="{{$data['evaluasi'][$i]['actual_hk'] == null ? $data['evaluasi'][$i]['aktual'] : $data['evaluasi'][$i]['actual_hk'] }}"
                                    placeholder="Optional" name="aktual_hk[]" />
                            </div>
                        </div>
                        <div class="col-md-12 col-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="keterangan">Deskription</label>
                                    <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                        name="keterangan[]">{{$data['evaluasi'][$i]['alasan']}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                        
                    @endfor
                <br><br><br>
            </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-primary" name="tombol" value="0">Simpan</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
</script>