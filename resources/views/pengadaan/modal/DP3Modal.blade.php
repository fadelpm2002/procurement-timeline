<div class="modal fade text-left" id="pengadaaanDP3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27bd2f">
                <h4 class="modal-title text-white modal_title" id="myModalLabel33">DP3 Pengadaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="pengadaanDP3" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @method('PUT')
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="plan_dp_3_belum_lengkap" class="form-label">DP3 Telah Diterima Lengkap Pada tanggal</label>
                                <input type="date" class="form-control" id="plan_dp_3_belum_lengkap" value="{{old('plan_dp_3_belum_lengkap')}}"
                                    placeholder="Optional" name="plan_dp_3_belum_lengkap" />
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Yes</button>
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>