<div class="modal fade text-left" id="rejectPengadaan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27bd2f">
                <h4 class="modal-title text-white" id="myModalLabel33">Tolak Pengadaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('pengadaan-update-status',$data['id'])}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input name="_method" type="hidden" value="PUT">
                <div class="modal-body">
                    <div class="col-md-12 col-12">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="select2 form-control" name="status" id="status"
                                aria-placeholder="silahkan pilih metode pengadaan" required>
                                <option value=""></option>
                                <option value="7">Gagal</option>
                                <option value="8">Batal</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Deskription</label>
                            <textarea class="form-control" id="keterangan" rows="3" placeholder=""
                                name="keterangan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lanjutkan</button>
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>