<div class="modal fade" id="editPengadaan" tabindex="-1" role="dialog" aria-labelledby="editPengadaanTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header " style="background-color: #27bd2f">
                <h5 class="modal-title text-white" id="editPengadaanTitle"> Edit Pengadaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-5">
                <form action="{{ route('pengadaan-edit', $data['id']) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <input type="hidden" value="{{ Auth::user()->id }}" name="id_mengajukan">
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label for="judul_pekerjaan" class="form-label">Judul Pekerjaan</label>
                                <input type="text" class="form-control" id="judul_pekerjaan"
                                    value="{{$data['judul']}}" placeholder="" name="judul_pekerjaan" required />

                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label for="pic" class="form-label">PIC Procurement</label>
                                <select class="select2 form-control" name="id_pic[]" id="pic"
                                    aria-placeholder="silahkan pilih metode pengadaan" multiple required>
                                    {{-- @for ($i = 0; $i < count($data['id_pengadaan_pic']); $i++)
                                        <option value="{{$data['id_pengadaan_pic'][$i]}}">{{$data['pengadaan_pic'][$i]}}</option>
                                    @endfor --}}
                                    @for ($i = 0; $i < count($data['id_pengadaan_pic']); $i++)
                                        <option value="{{$data['id_pengadaan_pic'][$i]}}" {{ ($data['id_pengadaan_pic'][$i] == $data['id_pengadaan_pic'][$i] ? "selected":"") }}>{{$data['pengadaan_pic'][$i]}}</option>
                                    @endfor
                                    @php
                                    $pic = App\User::get();
                                    @endphp
                                    @foreach ($pic as $item)
                                    <option value="{{$item->id}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="kategori" class="form-label">Kategori</label>
                                <select class="testing form-control" name="id_kategori" id="kategori"
                                    aria-placeholder="silahkan pilih kategori" required>
                                    <option value="{{$data['kategori']}}">{{$data['kategori']}}</option>
                                    @php
                                    $kategori = App\Kategori::get();
                                    @endphp
                                    @foreach ($kategori as $item)
                                    <option value="{{$item->nama}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="jenis_anggaran" class="form-label">Jenis Anggaran</label>
                                <select class="select2 form-control" name="jenis_anggaran" id="jenis_anggaran"
                                    aria-placeholder="silahkan pilih kategori" required>
                                    <option value="{{$data['jenis_anggaran']}}">{{$data['jenis_anggaran']}}</option>
                                    <option value="ABO">ABO</option>
                                    <option value="ABI">ABI</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="jenis_pengadaan" class="form-label">Jenis Pengadaan</label>
                                <select class="select2 form-control" name="jenis_pengadaan" id="jenis_pengadaan"
                                    aria-placeholder="silahkan pilih jenis pengadaan" required>
                                    <option value="{{$data['jenis_pengadaan']}}">{{$data['jenis_pengadaan']}}</option>
                                    <option value="barang">Barang</option>
                                    <option value="jasa">Jasa</option>
                                    {{-- <option value="barang & jasa">Barang & Jasa</option> --}}
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label for="tander" class="form-label">Tender Ke</label>
                                <input type="number" class="form-control" id="tander"
                                     placeholder="" name="tander" value="{{$data['tender']}}" required />

                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label for="tahun_anggaran" class="form-label">Tahun Anggaran</label>
                                <select class="select2 form-control" name="tahun_anggaran" id="tahun_anggaran"
                                    aria-placeholder="silahkan pilih Tahun" required>
                                    <option value="{{$data['tahun_anggaran']}}">{{$data['tahun_anggaran']}}</option>
                                    @for ($i=1; $i < 2; $i++)
                                        @php
                                            $addYear = Carbon\Carbon::now()->subYear($i);
                                        @endphp
                                        <option value="{{$addYear->format('Y')}}">{{$addYear->format('Y')}}</option>
                                    @endfor
                                    @for ($i=0; $i < 5; $i++)
                                        @php
                                            $addYear = Carbon\Carbon::now()->addYear($i);
                                        @endphp
                                        <option value="{{$addYear->format('Y')}}">{{$addYear->format('Y')}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-3">
                            <div class="form-group">
                                <label for="fpp" class="form-label">FPP</label>
                                <select class="testing form-control" name="id_fpp" id="fpp"
                                    aria-placeholder="silahkan pilih FPP" required>
                                    <option value="{{$data['fpp']}}">{{$data['fpp']}}</option>
                                    @php
                                    $fpp = App\FungsiPemintaPengadaan::get();
                                    @endphp
                                    @foreach ($fpp as $item)
                                    <option value="{{$item->nama}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-3">
                            <div class="form-group">
                                <label for="no_pr" class="form-label">No PR</label>
                                <input type="text" class="form-control" id="no_pr" value="{{$data['no_pr']}}"
                                    placeholder="Optional" name="no_pr" />
                            </div>
                        </div>
                        <div class="col-md-3 col-3">
                            <div class="form-group">
                                <label for="kategori_csms" class="form-label">Kategori CSMS </label>
                                <select class="select2 form-control" name="kategori_csms" id="kategori_csms"
                                    aria-placeholder="silahkan pilih metode pengadaan" required>
                                    <option value="{{$data['kategori_csms']}}">{{$data['kategori_csms']}}</option>
                                    <option value="non risk">Non Risk</option>
                                    <option value="low risk">Low Risk</option>
                                    <option value="medium risk">Medium Risk</option>
                                    <option value="high risk">High Risk</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-3">
                            <div class="form-group">
                                <label for="metode_pengadaan" class="form-label">Metode Pengadaan</label>
                                <select class="testing form-control" name="id_metode_pengadaan" id="metode_pengadaan"
                                    aria-placeholder="silahkan pilih metode pengadaan" required>
                                    <option value="{{$data['metode_pengadaan']}}">{{$data['metode_pengadaan']}}</option>
                                    @php
                                    $metodePengadaan = App\MetodePengadaan::get();
                                    @endphp
                                    @foreach ($metodePengadaan as $item)
                                    <option value="{{$item->nama}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-12">
                            <div class="form-group">
                                <label for="metode_pengadaan" class="form-label">Sub Bidang</label>
                                <select class="pilihan form-control" name="sub_bidang[]" id="sub_bidang" aria-placeholder="silahkan pilih sub bidang" multiple required>
                                    @for ($i = 0; $i < count($data['sub_bidang']); $i++)
                                        {{-- <option value="{{$data['sub_bidang_id'][$i]}} selected">{{$data['sub_bidang'][$i]}}</option> --}}
                                        <option value="{{$data['sub_bidang_id'][$i]}}" {{ ($data['sub_bidang_id'][$i] == $data['sub_bidang_id'][$i] ? "selected":"") }}>{{$data['sub_bidang'][$i]}}</option>
                                    @endfor
                                    @php
                                        $subBidang = App\SubBidang::get();
                                    @endphp
                                    <option value=""></option>
                                    @foreach ($subBidang as $sub)
                                    <option value="{{$sub->id}}">{{$sub->code}} - {{$sub->description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        {{-- <div class="col-md-4 col-4">
                            <div class="form-group">
                                <label for="mengetahui" class="form-label">Mengetahui</label>
                                <select class="select2 form-control" name="id_mengetahui" id="mengetahui"
                                    aria-placeholder="silahkan pilih metode pengadaan" required>
                                    <option value=""></option>
                                    @php
                                    $mengetahui = App\User::get();
                                    @endphp
                                    <option value=""></option>
                                    @foreach ($mengetahui as $item)
                                    <option value="{{$item->id}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-4 pb-5">
                            <div class="form-group">
                                <label for="menyetujui" class="form-label">Menyetujui</label>
                                <select class="select2 form-control" name="id_menyetujui" id="menyetujui"
                                    aria-placeholder="silahkan pilih metode pengadaan" required>
                                    <option value=""></option>
                                    @php
                                    $menyetujui = App\User::get();
                                    @endphp
                                    <option value=""></option>
                                    @foreach ($menyetujui as $item)
                                    <option value="{{$item->id}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div> --}}
                    </div>
                    <br>
                    <br>
                    <br>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-primary">Simpan</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(function() {
    $(document).on('change', '.jenis_donasi', function() {
        if ($(this).val() == 5) {
            $('.itembarang').show(500);
            $('.nominal_pembayaran').css("display", "none");
            $('.nominal_pembayaran').hide(500);
            $('.jenis_donasi_col').removeClass("col-md-12");
            $('.jenis_donasi_col').addClass("col-md-6");
            $('.tanggal_penerima_col').addClass("col-md-6");
            // $('.nominal_pembayaran').hide(500);
        } else {
            $('.itembarang').hide(500);
            $('.nominal_pembayaran').show(500);
            $('.jenis_donasi_col').removeClass("col-md-12");
            $('.jenis_donasi_col').addClass("col-md-12");
            $('.tanggal_penerima_col').removeClass("col-md-6");
            $('.tanggal_penerima_col').addClass("col-md-4");
        }
    })
})

///
// let bay = 1;
// barangDonasi(bay);

function barangDonasi(bay) {
    // let pilih = 'selectpicker';
    let CRbarangDonasi = '';
    CRbarangDonasi += '<div class ="row" id ="tambahBarangDonasi">' +
        '<div class="col-md-4 col-4">' +
        '<div class="form-group">' +
        '<label for="invoice-subject" class="form-label">Nama Barang</label>' +
        '<input type="text" class="form-control" value="" placeholder="" name="name_barang[]"/>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-4 col-4">' +
        '<div class="form-group">' +
        '<label for="invoice-from" class="form-label">qty</label>' +
        '<input type="number" class="form-control" id="invalid-state" value="{{old('
    tanggal ')}}" placeholder="" name="qty[]" />' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2">' +
        '<button type="button" class="btn_tambah_items btn btn-icon btn-flat-success">+</button>' +
        '<button type="button" class="remove btn btn-icon btn-flat-danger">-</button>' +
        // '<button type="button" class="btn btn-gradient-danger remove" >-</button>'+
        '</div>' +
        '</div>';
    if (bay > 1) {
        $('.new-items').append(CRbarangDonasi);
    }
}
$(document).on('click', '.btn_tambah_items', function() {
    bay++;
    barangDonasi(bay);
});
$(document).on('click', '.remove', function() {
    bay--;
    $(this).closest("#tambahBarangDonasi").remove();
});


///
$(function() {

    $(".pilihan").select2();
    $(".testing").select2({
        tags: true,
        createTag: function(params) {
            return {
                id: params.term,
                text: params.term,
                newOption: true
            }
        },
        templateResult: function(data) {
            var $result = $("<span></span>");

            $result.text(data.text);

            if (data.newOption) {
                $result.append(" <em>(new)</em>");
            }

            return $result;
        }
    });
});
</script>