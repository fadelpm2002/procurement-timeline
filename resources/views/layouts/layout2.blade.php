<!DOCTYPE html>
@php
    $thema = App\Mode::where('user_id', Auth::user()->id)->first();
@endphp
@if ($thema)
    <html class="loading {{$thema->name}}" lang="en" data-textdirection="ltr">
@else
    <html class="loading" lang="en" data-textdirection="ltr">
@endif
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="author" content="PIXINVENT">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="{{asset('templateV2')}}/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('templateV2')}}/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
        rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/vendors/css/vendors.min.css">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/vendors/css/charts/apexcharts.css">
    --}}
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/vendors/css/extensions/toastr.min.css">
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/vendors/css/forms/select/select2.min.css">
    --}}
    <link rel="stylesheet" href="{{asset('argon')}}/vendor/select2/dist/css/select2.min.css">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/vendors/css/forms/wizard/bs-stepper.min.css">
    --}}
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/css/core/menu/menu-types/vertical-menu.css">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/plugins/charts/chart-apex.css">
    --}}
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/css/plugins/extensions/ext-component-toastr.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/pages/app-invoice-list.css">
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/pages/page-knowledge-base.css">
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css"
        href="{{asset('templateV2')}}/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/plugins/forms/form-wizard.css">
    --}}
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/app-assets/css/pages/app-user.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('templateV2')}}/assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static   menu-collapsed" data-open="click"
    data-menu="vertical-menu-modern" data-col="">
    @include('sweetalert::alert')

    <!-- BEGIN: Header-->
    {{-- dektop --}}
    <input type="hidden" value="{{Auth::user()->id}}" class="userKu">
    <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow ">
        <div class="navbar-container d-flex content">
            <div class="bookmark-wrapper d-flex align-items-center">
                <ul class="nav navbar-nav d-xl-none">
                    <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i class="ficon"
                                data-feather="menu"></i></a></li>
                </ul>
                <ul class="nav navbar-nav bookmark-icons">
                    {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-toggle="tooltip" data-placement="top" title="Email"><i class="ficon" data-feather="mail"></i></a></li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-chat.html" data-toggle="tooltip" data-placement="top" title="Chat"><i class="ficon" data-feather="message-square"></i></a></li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calendar.html" data-toggle="tooltip" data-placement="top" title="Calendar"><i class="ficon" data-feather="calendar"></i></a></li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-todo.html" data-toggle="tooltip" data-placement="top" title="Todo"><i class="ficon" data-feather="check-square"></i></a></li> --}}
                </ul>
                <ul class="nav navbar-nav">
                    {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon text-warning" data-feather="star"></i></a>
                        <div class="bookmark-input search-input">
                            <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                            <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                            <ul class="search-list search-list-bookmark"></ul>
                        </div>
                    </li> --}}
                </ul>
            </div>
            <ul class="nav navbar-nav align-items-center ml-auto">
                {{-- <li class="nav-item dropdown dropdown-language"><a class="nav-link dropdown-toggle" id="dropdown-flag"
                        href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                            class="flag-icon flag-icon-id"></i><span class="selected-language">Indonesia</span></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="javascript:void(0);" data-language="en"><i class="flag-icon flag-icon-us"></i> English</a><a class="dropdown-item" href="javascript:void(0);" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a><a class="dropdown-item" href="javascript:void(0);" data-language="de"><i class="flag-icon flag-icon-de"></i> German</a><a class="dropdown-item" href="javascript:void(0);" data-language="pt"><i class="flag-icon flag-icon-pt"></i> Portuguese</a></div> 
                </li> --}}
                <li class="nav-item ">
                    @if ($thema)
                        @if ($thema->name == 'light-layout')
                            <a class="nav-link" data-toggle="modal" data-target="#aturThema" id="dark-layout" onclick="setThema(this.id)">
                                <i class="ficon" data-feather="moon"></i>
                            </a>
                        @elseif($thema->name == 'dark-layout')
                            <a class="nav-link" data-toggle="modal" data-target="#aturThema" id="light-layout" onclick="setThema(this.id)">
                                <i class="ficon" data-feather="sun"></i>
                            </a>
                        @endif
                    @else
                        <a class="nav-link" data-toggle="modal" data-target="#aturThema" id="dark-layout" onclick="setThema(this.id)">
                            <i class="ficon" data-feather="moon"></i>
                        </a>   
                    @endif
                </li>
                {{-- <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon" data-feather="search"></i></a>
                    <div class="search-input">
                        <div class="search-input-icon"><i data-feather="search"></i></div>
                        <input class="form-control input" type="text" placeholder="Explore Vuexy..." tabindex="-1" data-search="search">
                        <div class="search-input-close"><i data-feather="x"></i></div>
                        <ul class="search-list search-list-main"></ul>
                    </div>
                </li> --}}
                {{-- <li class="nav-item dropdown dropdown-notification mr-25">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="dropdown">
                        <i class="ficon" data-feather="bell"></i>
                        <div class="totalNotif"></div>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                        <li class="dropdown-menu-header">
                            <div class="dropdown-header d-flex">
                                <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                            </div>
                        </li>
                        <li class="scrollable-container media-list">
                            <div class="notifikasiKu"></div>
                        </li>
                    </ul>
                </li> --}}
                <li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link"
                        id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <div class="user-nav d-sm-flex d-none">
                            <span class="user-name font-weight-bolder">{{ Auth::user()->nama }}</span>
                            <span class="user-status">{{ Auth::user()->email }}</span>
                        </div>
                        @if (Auth::user()->foto_profil == null)
                        @php
                        $name = Auth::user()->nama;
                        @endphp
                        <div class="avatar bg-light-primary avatar-lg">
                            <span class="avatar-content">{{substr($name,0,1)}}</span>
                        </div>
                        @else
                        <span class="avatar avatar-lg">
                            <img class="round" src="/fotoProfile/{{Auth::user()->foto_profil}}" alt="avatar" height="40"
                                width="40">
                            <span class="avatar-status-online"></span>
                        </span>
                        @endif
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                        {{-- <a class="dropdown-item" href="page-profile.html"><i class="mr-50" data-feather="user"></i>
                            Profile</a> --}}
                        {{-- <a class="dropdown-item" href="app-email.html"><i class="mr-50" data-feather="mail"></i> Inbox</a>
                        <a class="dropdown-item" href="app-todo.html"><i class="mr-50" data-feather="check-square"></i> Task</a>
                        <a class="dropdown-item" href="app-chat.html"><i class="mr-50" data-feather="message-square"></i> Chats</a> --}}
                        <div class="dropdown-divider"></div>
                        {{-- <a class="dropdown-item" href="page-account-settings.html"><i class="mr-50" data-feather="settings"></i> Settings</a>
                        <a class="dropdown-item" href="page-pricing.html"><i class="mr-50" data-feather="credit-card"></i> Pricing</a>
                        <a class="dropdown-item" href="page-faq.html"><i class="mr-50" data-feather="help-circle"></i> FAQ</a> --}}
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="mr-50" data-feather="power"></i>
                            Keluar</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <ul class="main-search-list-defaultlist d-none">
        <li class="d-flex align-items-center"><a href="javascript:void(0);">
                <h6 class="section-label mt-75 mb-0">Files</h6>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                href="app-file-manager.html">
                <div class="d-flex">
                    <div class="mr-75"><img src="{{asset('templateV2')}}/app-assets/images/icons/xls.png" alt="png"
                            height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing
                            Manager</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;17kb</small>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                href="app-file-manager.html">
                <div class="d-flex">
                    <div class="mr-75"><img src="{{asset('templateV2')}}/app-assets/images/icons/jpg.png" alt="png"
                            height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd
                            Developer</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;11kb</small>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                href="app-file-manager.html">
                <div class="d-flex">
                    <div class="mr-75"><img src="{{asset('templateV2')}}/app-assets/images/icons/pdf.png" alt="png"
                            height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital
                            Marketing Manager</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;150kb</small>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100"
                href="app-file-manager.html">
                <div class="d-flex">
                    <div class="mr-75"><img src="{{asset('templateV2')}}/app-assets/images/icons/doc.png" alt="png"
                            height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web
                            Designer</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;256kb</small>
            </a></li>
        <li class="d-flex align-items-center"><a href="javascript:void(0);">
                <h6 class="section-label mt-75 mb-0">Members</h6>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                href="app-user-view.html">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-75"><img
                            src="{{asset('templateV2')}}/app-assets/images/portrait/small/avatar-s-8.jpg" alt="png"
                            height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                href="app-user-view.html">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-75"><img
                            src="{{asset('templateV2')}}/app-assets/images/portrait/small/avatar-s-1.jpg" alt="png"
                            height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd
                            Developer</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                href="app-user-view.html">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-75"><img
                            src="{{asset('templateV2')}}/app-assets/images/portrait/small/avatar-s-14.jpg" alt="png"
                            height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing
                            Manager</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100"
                href="app-user-view.html">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-75"><img
                            src="{{asset('templateV2')}}/app-assets/images/portrait/small/avatar-s-6.jpg" alt="png"
                            height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                    </div>
                </div>
            </a></li>
    </ul>
    <ul class="main-search-list-defaultlist-other-list d-none">
        <li class="auto-suggestion justify-content-between"><a
                class="d-flex align-items-center justify-content-between w-100 py-50">
                <div class="d-flex justify-content-start"><span class="mr-75"
                        data-feather="alert-circle"></span><span>No results found.</span></div>
            </a></li>
    </ul>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    @include('layouts.modal.aturThema')
    @include('layouts.mainMenu')
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="content-wrapper">

            @yield('main_wrapper')

            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                {{-- <section id="dashboard-analytics"> --}}
                <!-- List DataTable -->
                @yield('main_content')

                <!--/ List DataTable -->
                {{-- </section> --}}
                <!-- Dashboard Analytics end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">Klosur Sinergi Raya &copy; 2022<a
                    class="ml-25" href="https://klosur.com/" target="_blank">Klosur</a><span
                    class="d-none d-sm-inline-block">, All rights Reserved</span></span><span
                class="float-md-right d-none d-md-block">Hand-crafted & Made with<i data-feather="heart"></i></span></p>
    </footer>
    <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/charts/apexcharts.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/extensions/toastr.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/extensions/moment.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
    {{-- <script src="{{asset('templateV2')}}/app-assets/vendors/js/forms/select/select2.full.min.js"></script> --}}
    <script src="{{asset('argon')}}/vendor/select2/dist/js/select2.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
    {{-- <script src="{{asset('templateV2')}}/app-assets/vendors/js/forms/wizard/bs-stepper.min.js"></script> --}}
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('templateV2')}}/app-assets/js/core/app-menu.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    {{-- <script src="{{asset('templateV2')}}/app-assets/js/scripts/pages/dashboard-analytics.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/js/scripts/pages/app-invoice-list.js"></script>
    <script src="{{asset('templateV2')}}/app-assets/js/scripts/tables/table-datatables-basic.js"></script> --}}
    <script src="{{asset('templateV2')}}/app-assets/js/scripts/forms/form-select2.js"></script>
    {{-- <script src="{{asset('templateV2')}}/app-assets/js/scripts/pages/app-invoice.js"></script> --}}
    {{-- <script src="{{asset('templateV2')}}/app-assets/js/scripts/components/components-collapse.js"></script> --}}
    {{-- <script src="{{asset('templateV2')}}/app-assets/js/scripts/cards/card-analytics.js"></script> --}}
    {{-- <script src="{{asset('templateV2')}}/app-assets/js/scripts/pages/page-knowledge-base.js"></script> --}}
    <script src="{{asset('templateV2')}}/app-assets/js/scripts/forms/form-input-mask.js"></script>
    {{-- <script src="{{asset('templateV2')}}/app-assets/js/scripts/forms/form-validation.js"></script> --}}
    <script src="{{asset('templateV2')}}/app-assets/js/scripts/forms/pickers/form-pickers.js"></script>
    {{-- <script src="{{asset('templateV2')}}/app-assets/js/scripts/forms/form-wizard.js"></script> --}}
    <script src="{{asset('templateV2')}}/app-assets/js/scripts/pages/app-user-view.js"></script>
    <!-- END: Page JS-->

    <script>
    function setThema(id) {
        $('.codeThema').val(id);
        if (id == 'dark-layout') {
            $('.pesan').html('Apakah Anda Yakin Untuk Aktifkan Thema <span class="text-success">Dark Mode</span> ?');
        } else if (id == 'light-layout') {
            $('.pesan').html('Apakah Anda Yakin Untuk Aktifkan Thema <span class="text-success">Light Mode</span> ?');
        }
    }
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
    });
    // let user = $('.userKu').val();
    // fetch('../notifikasi-pengadaan/' + user + '', {
    //         method: 'get'
    //     })
    //     .then(response => response.json())
    //     .then(data => {
    //         let dataLength = data.pengadaanMengetahui.length + data.pengadaanMenyetujui.length;
    //         // console.log(data.pengadaanMengetahui);
    //         // console.log(data.pengadaanMenyetujui);
    //         // console.log(data.pengadaanMengetahui.length);
    //         // console.log(data.pengadaanMenyetujui.length);
    //         if (dataLength > 0) {
    //             $('.totalNotif').html('<span class="badge badge-pill badge-danger badge-up">' + dataLength +
    //                 '</span>');
    //         } else {

    //         }
    //         let notif = '';
    //         if (data.pengadaanMengetahui.length > 0) {
    //             notif += '<div class="media d-flex align-items-center">' +
    //                 '<h6 class="font-weight-bolder mr-auto mb-0">' +
    //                 'Pengadaan Diajukan' +
    //                 '</h6>' + '</div>';
    //             for (let i = 0; i < data.pengadaanMengetahui.length; i++) {
    //                 notif += '<a class="d-flex" href="../pengadaan-detail/' + data.pengadaanMengetahui[i]['id'] +
    //                     '">' +
    //                     ' <div class="media d-flex align-items-start">' +
    //                     ' <div class="media-left">' +
    //                     '<div class="avatar bg-light-warning">' +
    //                     '<div class="avatar-content"></div>' +
    //                     '</div>' +
    //                     '</div>' +
    //                     '<div class="media-body">' +
    //                     '<p class="media-heading"><span class="font-weight-bolder">' + data.pengadaanMengetahui[i][
    //                         'nama_mengajukan'
    //                     ] +
    //                     '</span></p>' +
    //                     '<small class="notification-text">' + data.pengadaanMengetahui[i]['judul'] + '</small>' +
    //                     '</div>' +
    //                     '</div>' +
    //                     '</a>';
    //             }
    //         }
    //         if (data.pengadaanMenyetujui.length > 0) {
    //             notif += '<div class="media d-flex align-items-center">' +
    //                 '<h6 class="font-weight-bolder mr-auto mb-0">' +
    //                 'Pengadaan Telah Diketahui' +
    //                 '</h6>' + '</div>';
    //             for (let i = 0; i < data.pengadaanMenyetujui.length; i++) {
    //                 notif += '<a class="d-flex" href="../pengadaan-detail/' + data.pengadaanMenyetujui[i]['id'] +
    //                     '">' +
    //                     ' <div class="media d-flex align-items-start">' +
    //                     ' <div class="media-left">' +
    //                     '<div class="avatar bg-light-warning">' +
    //                     '<div class="avatar-content"></div>' +
    //                     '</div>' +
    //                     '</div>' +
    //                     '<div class="media-body">' +
    //                     '<p class="media-heading"><span class="font-weight-bolder">' + data.pengadaanMenyetujui[i][
    //                         'nama_mengetahui'
    //                     ] +
    //                     '</span></p>' +
    //                     '<small class="notification-text">' + data.pengadaanMenyetujui[i]['judul'] + '</small>' +
    //                     '</div>' +
    //                     '</div>' +
    //                     '</a>';
    //             }
    //         }


    //         $('.notifikasiKu').html(notif);
    //     }).catch(err => console.log(err));
    </script>
</body>
<!-- END: Body-->

</html>