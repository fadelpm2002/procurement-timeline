<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('/logout', 'Auth\LogoutController@index')->name('logout');
    Route::get('/pengadaan-index-timeline', 'PengadaanController@pengadaanIndexTimeline')->name('pengadaan-index-timeline');
    Route::get('/pengadaan-index', 'PengadaanController@pengadaanIndex')->name('pengadaan.index');
    Route::post('/pengadaan-create', 'PengadaanController@pengadaanCreate')->name('pengadaan.create');
    Route::post('/pengadaan-evaluasi', 'PengadaanController@pengadaanEvaluasi')->name('pengadaan-evaluasi');
    Route::get('/pengadaan-filter', 'PengadaanController@pengadaanFilter')->name('pengadaan.filter');
    Route::get('/notifikasi-pengadaan/{id}', 'NotifikasiController@notifikasiPengadaan')->name('notifikasi.pengadaan');
    Route::get('/pengadaan-detail/{id}', 'PengadaanController@pengadaanDetail')->name('pengadaan-detail');
    Route::get('/pengadaan-detail-api/{id}', 'PengadaanController@pengadaanDetailApi')->name('pengadaan-detail-api');
    Route::get('/pengadaan-export-evaluasi/{id}', 'PengadaanController@pengadaanExportEvaluasi')->name('pengadaan-export-evaluasi');
    Route::put('/pengadaan-update-status/{id}', 'PengadaanController@pengadaanUpdateStatus')->name('pengadaan.update.status');
    Route::get('/pengadaan-timeline/{id}', 'PengadaanController@pengadaanTimeline')->name('pengadaan.timeline');
    Route::get('/pengadaan-timeline-filter', 'PengadaanController@pengadaanTimelineFilter')->name('pengadaan.timeline.filter');
    Route::put('/pengadaan-mengetahui/{id}', 'PengadaanController@pengadaanMengetahui')->name('pengadaan-mengetahui');
    Route::put('/pengadaan-menyetujui/{id}', 'PengadaanController@pengadaanMenyetujui')->name('pengadaan-menyetujui');
    Route::put('/pengadaan-batal-dokumen/{id}', 'PengadaanController@pengadaanBatalDokumen')->name('pengadaan-batal-dokumen');
    Route::put('/pengadaan-reject-dokumen/{id}', 'PengadaanController@pengadaanRejectDokumen')->name('pengadaan-reject-dokumen');
    Route::put('/pengadaan-ubah-status/{id}', 'PengadaanController@pengadaanUbahStatus')->name('pengadaan-ubah-status');
    Route::put('/pengadaan-update-tahapan/{id}', 'PengadaanController@pengadaanUpdateTahapan')->name('pengadaan-update-tahapan');
    Route::put('/pengadaan-update-tahapan2/{id}', 'PengadaanController@pengadaanUpdateTahapan2')->name('pengadaan-update-tahapan2');
    Route::put('/pengadaan-edit-tahapan/{id}', 'PengadaanController@pengadaanEditTahapan')->name('pengadaan-edit-tahapan');
    Route::put('/pengadaan-edit/{id}', 'PengadaanController@pengadaanEdit')->name('pengadaan-edit');
    Route::get('/pengadaan-export', 'PengadaanController@pengadaanExport')->name('pengadaan-export');
    Route::put('/pengadaan-DP3/{id}', 'PengadaanController@pengadaanDP3')->name('pengadaan-DP3');
    // testing
    Route::get('/dashboard', 'HomeController@homeIndex')->name('dashboard');
    Route::post('/atur-thema', 'HomeController@aturThema')->name('atur-thema');
    Route::get('/', 'HomeController@homeIndex')->name('dashboard');
});

Route::get('/pengadaan-dashboard-guest', 'PengadaanController@pengadaanDashbordGuest')->name('pengadaan-dashboard-guest');
Route::get('/pengadaan-dashboard-filter', 'PengadaanController@pengadaanDashbordFilter')->name('pengadaan-dashboard-filter');